﻿using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client.Models;
using LendFoundry.Loans.Payment.Api.Controllers;
using LendFoundry.Loans.Payment.Api.Models;
using LendFoundry.Loans.Payment.Service.Fake;
using LendFoundry.Loans.Schedule;
using Microsoft.AspNet.Mvc;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Payment.Api.Test
{
    public class PaymentApiTests : InMemoryObjects
    {
        private Mock<IPaymentService> Service { get; } = new Mock<IPaymentService>();

        private Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        private PaymentController Controller => new PaymentController(Service.Object, new UtcTenantTime(), Logger.Object);

        [Fact]
        public void Put_Ach_WhenExcutionOk()
        {
            // arrange
            var list = new[] { InMemoryAchPaymentRequest }.AsEnumerable();
            list = list.Select((s, i) =>
            {
                s.LoanReferenceNumber = $"loan00{i}";
                s.AccountHolder = "AccountHolder";
                s.AccountNumber = "123456";
                s.RoutingNumber = "1234";
                s.AccountType = BankAccountType.Savings;
                return s;
            });
            var achPaymentRequest = list.FirstOrDefault() ?? InMemoryAchPaymentRequest;

            Service.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>())).Returns(InMemoryAchPayment);

            // act
            var actionResult = Controller.Add(achPaymentRequest);

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)actionResult).StatusCode);
            Service.Verify(v => v.AddPayment(It.IsAny<IPaymentRequest>()), Times.Once);
        }

        [Fact]
        public void Put_Ach_When_RequestIsNull()
        {
            // arrange
            // act
            var actionResult = Controller.Add((AchPaymentRequest) null);

            // assert
            Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
        }

        [Fact]
        public void Put_Ach_When_Request_HasNo_LoanReferenceNumber()
        {
            // arrange
            var achPaymentRequest = InMemoryAchPaymentRequest;
            achPaymentRequest.LoanReferenceNumber = string.Empty;

            // act
           var actionResult = Controller.Add(achPaymentRequest);

            // assert
            Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
        }

        [Fact]
        public void Put_Ach_When_Request_Has_Invalid_PaymentMethod()
        {
            // arrange
            var achPaymentRequest = InMemoryAchPaymentRequest;
            achPaymentRequest.LoanReferenceNumber = string.Empty;

            // act
            var actionResult = Controller.Add(achPaymentRequest);

            // assert
            Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
        }




        [Fact]
        public void Put_Check_WhenExcutionOk()
        {
            // arrange
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((s, i) =>
            {
                s.LoanReferenceNumber = $"loan00{i}";
                return s;
            });
            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;
            
            Service.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>())).Returns(InMemoryCheckPayment);
            
            // act
            var actionResult = Controller.Add(checkPaymentRequest);

            // assert
            Assert.Equal(200, ((HttpOkObjectResult)actionResult).StatusCode);
            Service.Verify(v => v.AddPayment(It.IsAny<IPaymentRequest>()), Times.Once);
        }

        [Fact]
        public void Put_Check_When_RequestIsNull()
        {
            // arrange
            // act
            var actionResult = Controller.Add((CheckPaymentRequest) null);

            // assert
            Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
        }

        [Fact]
        public void Put_Check_When_Request_HasNo_LoanReferenceNumber()
        {
            // arrange
            var checkPaymentRequest = InMemoryCheckPaymentRequest;
            checkPaymentRequest.LoanReferenceNumber = string.Empty;

            // act
            var actionResult = Controller.Add(checkPaymentRequest);

            // assert
            Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
        }

        [Fact]
        public void Put_Check_When_Request_Has_Invalid_PaymentMethod()
        {
            // arrange
            var checkPaymentRequest = InMemoryCheckPaymentRequest;
            checkPaymentRequest.LoanReferenceNumber = string.Empty;

            // act
            var actionResult = Controller.Add(checkPaymentRequest);

            // assert
            Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
        }




        [Fact]
        public void Delete_Cancel_When_ExecutionOk()
        {
            // arrange
            var paymentCancelationRequest = new PaymentCancelationRequest
            {
                Id = Guid.NewGuid().ToString(),
                Notes = "Payment Cancelation Request Test"
            };

            Service.Setup(x => x.CancelPayment(It.IsAny<string>(), It.IsAny<string>()));

            // act
            var actionResult = Controller.Cancel(paymentCancelationRequest);

            // assert
            Assert.Equal(204, ((HttpStatusCodeResult)actionResult).StatusCode);
            Service.Verify(x => x.CancelPayment(It.IsAny<string>(), It.IsAny<string>()), Times.Once);
        }

        [Fact]
        public void Delete_Cancel_When_RequestIsNull()
        {
            // arrange
            Service.Setup(x => x.CancelPayment(It.IsAny<string>(), It.IsAny<string>()));

            // act
            var actionResult = Controller.Cancel(null);

            // assert
            Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
            Service.Verify(x => x.CancelPayment(It.IsAny<string>(), It.IsAny<string>()), Times.Never);
        }




        [Fact]
        public void ReconcilePendingPayments_When_ExecutionOk()
        {
            // arrange
            Service.Setup(x => x.ReconcilePendingPayments(It.IsAny<PaymentMethod>(), It.IsAny<DateTime>()));

            // act
            var actionResult = Controller.ReconcilePendingPayments(PaymentMethod.ACH, DateTime.Now);

            // assert
            Assert.Equal(204, ((HttpStatusCodeResult)actionResult).StatusCode);
            Service.Verify(x => x.ReconcilePendingPayments(It.IsAny<PaymentMethod>(), It.IsAny<DateTime>()), Times.Once);
        }

        [Fact]
        public void ReconcilePendingPayments_When_HasPaymentMethodIncorrect()
        {
            // arrange
            Service.Setup(x => x.ReconcilePendingPayments(It.IsAny<PaymentMethod>(), It.IsAny<DateTime>()));

            // act
            var actionResult = Controller.ReconcilePendingPayments(PaymentMethod.Undefined, DateTime.Now);

            // assert
            Assert.Equal(400, ((ObjectResult)actionResult).StatusCode);
            Service.Verify(x => x.ReconcilePendingPayments(It.IsAny<PaymentMethod>(), It.IsAny<DateTime>()), Times.Never);
        }


        [Fact]
        public void Pause_Should_Return_200()
        {

            var paymentController = GetPaymentController();
            const string referenceNumber = "123";
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date,
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(3),
                Notes = "This should not run for next three days"
            };

            var response = paymentController.Pause(referenceNumber, expectedPaymentPause).Result;
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as IPaymentPause;
            Assert.NotNull(result);
            Assert.Equal(referenceNumber, result.LoanReferenceNumber);
            Assert.Equal(expectedPaymentPause.StartDate, result.StartDate.Date);
            Assert.Equal(expectedPaymentPause.EndDate, result.EndDate.Date);
            Assert.Equal(expectedPaymentPause.Notes, result.Notes);
        }

        [Fact]
        public void Pause_Should_Return_200_Without_EndDate()
        {
            var paymentController = GetPaymentController();
            const string referenceNumber = "123";
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date,
                Notes = "This should not run for next three days"
            };

            var response = paymentController.Pause(referenceNumber, expectedPaymentPause).Result;
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as IPaymentPause;
            Assert.NotNull(result);

            Assert.Equal(referenceNumber, result.LoanReferenceNumber);
            Assert.Equal(expectedPaymentPause.StartDate, result.StartDate.Date);
            Assert.Equal(default(DateTimeOffset), result.EndDate);
            Assert.Equal(expectedPaymentPause.Notes, result.Notes);
        }

        [Fact]
        public void Pause_Should_Return_400_Without_LoanReferenceNumber()
        {
            var paymentController = GetPaymentController();
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date.AddDays(1),
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(3),
                Notes = "This should not run for next three days"
            };

            var response = paymentController.Pause(null, expectedPaymentPause).Result;
            Assert.IsType<Foundation.Services.ErrorResult>(response);
            var result = response as Foundation.Services.ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void Pause_Should_Return_400_With_Invalid_StartDate()
        {
            const string referenceNumber = "123";
            var paymentController = GetPaymentController();
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.AddDays(-1).Date,
                EndDate = DateTimeOffset.UtcNow.AddDays(3).Date,
                Notes = "This should not run for next three days"
            };
            var response = paymentController.Pause(referenceNumber, expectedPaymentPause).Result;
            Assert.IsType<Foundation.Services.ErrorResult>(response);
            var result = response as Foundation.Services.ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);
        }

        [Fact]
        public void Pause_Should_Return_400_With_Invalid_EndDate()
        {
            const string referenceNumber = "123";
            var paymentController = GetPaymentController();
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.AddDays(1).Date,
                EndDate = DateTimeOffset.UtcNow.AddDays(-3).Date,
                Notes = "This should not run for next three days"
            };

            var response = paymentController.Pause(referenceNumber, expectedPaymentPause).Result;
            Assert.IsType<Foundation.Services.ErrorResult>(response);
            var result = response as Foundation.Services.ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 400);

        }

        [Fact]
        public void Pause_Should_Return_409_When_Pause_Already_Exist()
        {
            const string referenceNumber = "123";
            var paymentController = GetPaymentController();

            paymentController.Pause(referenceNumber, new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date,
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(3),
                Notes = "This should not run for next three days"
            }).Wait();

            var response = paymentController.Pause(referenceNumber, new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date.AddDays(1),
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(10),
                Notes = "This should not run for next three days"
            }).Result;
            Assert.IsType<Foundation.Services.ErrorResult>(response);
            var result = response as Foundation.Services.ErrorResult;
            Assert.NotNull(result);
            Assert.Equal(result.StatusCode, 409);
        }

        [Fact]
        public void Delete_Pause_Should_Return_204()
        {
            var fakeTenant = new FakeTenantService();
            var paymentController = GetPaymentController(new List<IPaymentPause>()
                {
                    new PaymentPause()
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "123",
                        StartDate = DateTimeOffset.UtcNow.AddDays(-3),
                        EndDate = DateTimeOffset.UtcNow.AddDays(3),
                        Notes = "This should not run for next three days"
                    }
                });

            var response = paymentController.Resume("123").Result;
            Assert.IsType<NoContentResult>(response);
        }

        [Fact]
        public void GetAll_Return_Active_Pause()
        {
            var tenantTime = new UtcTenantTime();
            var fakeTenant = new FakeTenantService();
            var paymentController = GetPaymentController(
                    new List<IPaymentPause>
                {
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "123",
                        StartDate = tenantTime.Today.AddDays(-10),
                        EndDate = tenantTime.Today.AddDays(-7),
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "456",
                        StartDate = tenantTime.Today,
                        EndDate = tenantTime.Today.AddDays(3),
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "654",
                        StartDate = tenantTime.Today,
                        EndDate = tenantTime.Today.AddDays(4),
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "999",
                        StartDate = tenantTime.Today,
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "789",
                        StartDate = tenantTime.Today.AddDays(-4),
                        EndDate = tenantTime.Today.AddDays(-3),
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "987",
                        StartDate = tenantTime.Today.AddDays(10),
                        EndDate = tenantTime.Today.AddDays(13),
                        Notes = "This should not run for next three days"
                    }
                });

            var response = paymentController.GetPauses().Result;
            Assert.IsType<HttpOkObjectResult>(response);
            var activePauses = ((HttpOkObjectResult)response).Value as List<IPaymentPause>;
            Assert.NotNull(activePauses);
            Assert.Equal(3, activePauses.Count());
            Assert.Contains(activePauses, a => a.LoanReferenceNumber == "456");
            Assert.Contains(activePauses, a => a.LoanReferenceNumber == "654");
            Assert.Contains(activePauses, a => a.LoanReferenceNumber == "999");

        }

        [Fact]
        public void GetByReferenceNumber_Return_Active_PauseOnly()
        {
            var fakeTenant = new FakeTenantService();
            var tenantTime = new UtcTenantTime();
            var expectedPaymentPause = new PaymentPause
            {
                TenantId = fakeTenant.Current.Id,
                LoanReferenceNumber = "123",
                StartDate = tenantTime.Today,
                EndDate = tenantTime.Today.AddDays(3),
                Notes = "This should not run for next three days"
            };
            var paymentController = GetPaymentController(new List<IPaymentPause>
            {
                new PaymentPause
                {
                    TenantId = fakeTenant.Current.Id,
                    LoanReferenceNumber = "123",
                    StartDate = tenantTime.Today.AddDays(-10),
                    EndDate = tenantTime.Today.AddDays(-7),
                    Notes = "This should not run for next three days"
                },
                expectedPaymentPause
            });

            var response = paymentController.GetPauseByLoanReferenceNumber("123").Result;
            Assert.IsType<HttpOkObjectResult>(response);
            var result = ((HttpOkObjectResult)response).Value as IPaymentPause;
            Assert.NotNull(result);
            Assert.Equal(expectedPaymentPause.LoanReferenceNumber, result.LoanReferenceNumber);
            Assert.Equal(expectedPaymentPause.StartDate, result.StartDate);
            Assert.Equal(expectedPaymentPause.EndDate, result.EndDate);
            Assert.Equal(result.Notes, result.Notes);
        }

        private PaymentController GetPaymentController(List<IPaymentPause> records = null)
        {
            return new PaymentController(GetPaymentService(records), new UtcTenantTime(), Logger.Object);
        }

        private IPaymentService GetPaymentService(List<IPaymentPause> records = null)
        {
            var utcTenantTime = new UtcTenantTime();
            return new PaymentService
                    (
                        new PaymentConfiguration(),
                        null,
                        null,
                        null,
                        new FakeLoanService(new List<ILoanInfo>() {new InMemoryLoanBackupAdapter(new LoanInfo() { ReferenceNumber = "123"}) }), 
                        utcTenantTime,
                        new FakePaymentPauseRepository(new FakeTenantService(), utcTenantTime,
                        records ?? new List<IPaymentPause>()),
                        new FakeEventHub(),
                        Mock.Of<ILogger>()
                    );
        }
    }
    public class InMemoryLoanBackupAdapter : ILoanInfo
    {
        public InMemoryLoanBackupAdapter(LoanInfo model)
        {
            Loan = new Loan
            {
                ReferenceNumber = model.ReferenceNumber,
            };

            Summary = new PaymentScheduleSummary();
        }

        public ILoan Loan { get; }

        public IPaymentScheduleSummary Summary { get; }
    }
    public abstract class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";


        public CheckPaymentRequest InMemoryCheckPaymentRequest => new CheckPaymentRequest
        {
            LoanReferenceNumber = "",
            Method = PaymentMethod.Check,
            Type = PaymentType.Fee,
            Code = "124",
            DueDate = new DateTimeOffset(DateTime.Now),
            Amount = 100.00,
            RoutingNumber = "32",
            CheckNumber = "38",
            CheckAmount = 100.00,
            BalanceAmount = 100.00,
            Notes = "Payment Service Unit Test"
        };

        public IEnumerable<IPayment> InMemoryCheckPayment
        {
            get
            {
                return new []
                {
                    new CheckPayment(InMemoryCheckPaymentRequest)
                    {
                        Id = Guid.NewGuid().ToString(),
                        TenantId = TenantId
                    }
                };
            }
        }



        public AchPaymentRequest InMemoryAchPaymentRequest => new AchPaymentRequest
        {
            LoanReferenceNumber = "",
            Method = PaymentMethod.ACH,
            Type = PaymentType.Payoff,
            Code = "124",
            DueDate = new DateTimeOffset(DateTime.Now),
            Amount = 100.00,
            RoutingNumber = "32",
            AccountHolder = "AccountHolder",
            AccountNumber = "123456",
            AccountType = BankAccountType.Savings
        };

        public IEnumerable<IPayment> InMemoryAchPayment
        {
            get
            {
                return new[] 
                {
                    new AchPayment(InMemoryAchPaymentRequest)
                    {
                        Id = Guid.NewGuid().ToString(),
                        TenantId = TenantId
                    }
                };
            }
        }
    }
}