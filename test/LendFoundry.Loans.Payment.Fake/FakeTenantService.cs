﻿using System;
using System.Collections.Generic;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Loans.Payment.Service.Fake
{
    public class FakeTenantService : ITenantService
    {
        public List<TenantInfo> GetActiveTenants()
        {
            throw new NotImplementedException();
        }

        public TenantInfo Current { get; }=new TenantInfo("fake");
    }
}
