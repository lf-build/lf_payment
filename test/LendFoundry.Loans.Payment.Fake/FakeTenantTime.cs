﻿using System;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.Payment.Service.Fake
{
    public class FakeTenantTime : ITenantTime
    {
        private DateTimeOffset _dummyDate;
        public FakeTenantTime(DateTimeOffset dummyDate)
        {
            _dummyDate = dummyDate;
        }

        public DateTimeOffset Create(int year, int month, int day)
        {
            throw new NotImplementedException();
        }

        public DateTimeOffset FromDate(DateTime dateTime)
        {
            throw new NotImplementedException();
        }

        public DateTimeOffset Convert(DateTimeOffset dateTime)
        {
            throw new NotImplementedException();
        }

        public DateTimeOffset Now => _dummyDate;
        public DateTimeOffset Today => _dummyDate.DateTime;
        public TimeZoneInfo TimeZone { get; }
    }
}
