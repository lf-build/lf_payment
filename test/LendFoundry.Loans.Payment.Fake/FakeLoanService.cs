using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Status;

namespace LendFoundry.Loans.Payment.Service.Fake
{
    public class FakeLoanService : ILoanService
    {
        private List<ILoanInfo> LoanInfos { get; }

        public FakeLoanService(List<ILoanInfo> loanInfos )
        {
            LoanInfos=loanInfos??new List<ILoanInfo>();
        }

        public ILoanDetails GetLoanDetails(string referenceNumber)
        {
            throw new NotImplementedException();
        }

        public ILoanInfo GetLoanInfo(string referenceNumber)
        {
            return LoanInfos.FirstOrDefault(r => r.Loan?.ReferenceNumber == referenceNumber);
        }

        public IEnumerable<ITransaction> GetTransactions(string loanReferenceNumber)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IInstallment> GetPaymentSchedule(string loanReferenceNumber)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ILoanInfo> GetLoansDueIn(DateTime date)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime date, PaymentMethod paymentMethod)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsDueIn(DateTime date, PaymentMethod paymentMethod, InstallmentType type)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsDueInByAnniversaryDate(DateTime date, PaymentMethod paymentMethod, InstallmentType type)
        {
            throw new NotImplementedException();
        }

        public IPayoffAmount GetPayoffAmount(string loanReferenceNumber, DateTime date)
        {
            throw new NotImplementedException();
        }

        public void AddBank(string referenceNumber, BankAccount bankAccount)
        {
            throw new NotImplementedException();
        }

        public void SetBankAsPrimary(string referenceNumber, string accountNumber)
        {
            throw new NotImplementedException();
        }

        public LoanStatus GetStatus(string referenceNumber)
        {
            throw new NotImplementedException();
        }

        public LoanStatus GetStatusAttributes(string referenceNumber)
        {
            throw new NotImplementedException();
        }

        public void UpdateLoanStatus(string referenceNumber, string nextStatus)
        {
            throw new NotImplementedException();
        }

        public void UpdateLoanStatus(string referenceNumber, string nextStatus, DateTimeOffset? startDate, DateTimeOffset? endDate)
        {
            throw new NotImplementedException();
        }

        public void UpdatePaymentMethod(string referenceNumber, PaymentMethod paymentMethod)
        {
            throw new NotImplementedException();
        }

        public bool IsAllowedAction(string referenceNumber, Actions actionType)
        {
            throw new NotImplementedException();
        }

        public void UpdateInstallmentStatus(string loanReferenceNumber, DateTimeOffset dueDate, InstallmentStatus newStatus)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<IInstallmentInfo> GetInstallmentsWithGracePeriodDate(DateTime date, InstallmentType type, InstallmentStatus status)
        {
            throw new NotImplementedException();
        }

        public void SetInstallmentToUnPaid(string loanReferenceNumber, DateTimeOffset dueDate)
        {
            throw new NotImplementedException();
        }

        public void AddBank(string referenceNumber, Loans.BankAccount bankAccount)
        {
            throw new NotImplementedException();
        }

        public LoanSummary GetLoanSummary(string referenceNumber)
        {
            throw new NotImplementedException();
        }
    }
}