﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Tenant.Client;

namespace LendFoundry.Loans.Payment.Service.Fake
{
    public class FakePaymentPauseRepository : IPaymentPauseRepository
    {

        public FakePaymentPauseRepository(ITenantService tenantService, ITenantTime tenantTime)
        {
            TenantService = tenantService;
            TenantTime = tenantTime;
        }
        public FakePaymentPauseRepository(ITenantService tenantService, ITenantTime tenantTime, List<IPaymentPause> inMemoryCollection) : this(tenantService, tenantTime)
        {
            InMemoryCollection = inMemoryCollection;
        }

        public List<IPaymentPause> InMemoryCollection { get; } = new List<IPaymentPause>();
        private ITenantService TenantService { get; }
        private ITenantTime TenantTime { get; }

        public Task<IPaymentPause> Get(string id)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<IPaymentPause>> All(Expression<Func<IPaymentPause, bool>> query, int? skip = null, int? quantity = null)
        {
            throw new NotImplementedException();
        }

        public void Add(IPaymentPause item)
        {
            throw new NotImplementedException();
        }

        public void Remove(IPaymentPause item)
        {
            throw new NotImplementedException();
        }

        public void Update(IPaymentPause item)
        {
            throw new NotImplementedException();
        }

        public int Count(Expression<Func<IPaymentPause, bool>> query)
        {
            throw new NotImplementedException();
        }

        public Task<IPaymentPause> AddOrReplace(IPaymentPause paymentPause)
        {
            paymentPause.TenantId = TenantService.Current.Id;
            var existingRecord = InMemoryCollection.FirstOrDefault(p => p.TenantId == TenantService.Current.Id &&
                                                                        p.LoanReferenceNumber ==
                                                                        paymentPause.LoanReferenceNumber);
            paymentPause.Id = Guid.NewGuid().ToString("N");
            if (existingRecord != null)
                InMemoryCollection.Remove(existingRecord);
            InMemoryCollection.Add(paymentPause);
            return Task.FromResult(paymentPause);
        }

        public Task Remove(string referenceNumber)
        {
            var existingRecord = InMemoryCollection.FirstOrDefault(p => p.TenantId == TenantService.Current.Id &&
                                                                        p.LoanReferenceNumber ==
                                                                        referenceNumber);

            if (existingRecord != null)
                InMemoryCollection.Remove(existingRecord);

            return Task.FromResult(true);
        }

        public Task<List<IPaymentPause>> Get()
        {
            return Task.FromResult(InMemoryCollection.Where(p =>
                p.TenantId == TenantService.Current.Id &&
                p.StartDate <= TenantTime.Today &&
                (p.EndDate == default(DateTimeOffset) || p.EndDate >= TenantTime.Today)).ToList());
        }

        public Task<IPaymentPause> GetByLoanReferenceNumber(string referenceNumber)
        {
            return Task.FromResult(InMemoryCollection.FirstOrDefault(p => p.TenantId == TenantService.Current.Id &&
                                                                          p.LoanReferenceNumber == referenceNumber &&
                                                                          p.StartDate <= TenantTime.Today &&
                                                                          (p.EndDate == default(DateTimeOffset) ||
                                                                           p.EndDate >= TenantTime.Today)));
        }
    }
}