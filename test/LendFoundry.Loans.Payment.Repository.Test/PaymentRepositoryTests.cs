﻿using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Testing.Mongo;
using LendFoundry.Loans.Payment.Persistence;
using LendFoundry.Tenant.Client;
using Moq;
using System.Linq;
using System;
using Xunit;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.Payment.Repository.Test
{
    public class PaymentRepositoryTests : InMemoryObjects
    {
        private Mock<ITenantService> TenantService { get; set; }

        private IPaymentRepository Repository(IMongoConfiguration config)
        {
            TenantService = new Mock<ITenantService>();
            TenantService.Setup(s => s.Current).Returns(new TenantInfo { Id = TenantId });
            return new MongoPaymentRepository
            (
                configuration: config,
                tenantService: TenantService.Object,
                tenantTime: new UtcTenantTime()
            );
        }


        [MongoFact]
        public void Add_One_CheckPayment()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var list = new[] { InMemoryCheckPayment }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    return s;
                }).ToArray();

                var checkPayment = list.FirstOrDefault() ?? InMemoryCheckPayment;
                var repo = Repository(config);
                
                // act
                repo.Add(checkPayment);

                // assert
                var checkPaymentAdded = repo.Get(checkPayment.Id).Result;
                Assert.NotNull(checkPaymentAdded);
                Assert.Equal(checkPaymentAdded.TenantId, TenantId);
                Assert.False(string.IsNullOrWhiteSpace(checkPaymentAdded.Id));
                Assert.Equal(checkPaymentAdded.LoanReferenceNumber, checkPayment.LoanReferenceNumber);
            });
        }



        [MongoFact]
        public void Update_One_CheckPayment()
        {
            MongoTest.Run(config =>
            {
                // arrange
                var list = new[] { InMemoryCheckPayment }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.TenantId = TenantId;
                    s.Status = PaymentStatus.Pending;
                    return s;
                }).ToArray();

                var checkPayment = list.FirstOrDefault() ?? InMemoryCheckPayment;
                var repo = Repository(config);

                // act
                repo.Add(checkPayment);

                var domainObject = repo.All(p => p.LoanReferenceNumber == checkPayment.LoanReferenceNumber).Result.FirstOrDefault();
                Assert.NotNull(domainObject);

                if (domainObject != null)
                {
                    domainObject.Status = PaymentStatus.Received;

                    repo.Update(domainObject);

                    // assert
                    var checkPaymentUpdated = repo.Get(domainObject.Id).Result;
                    Assert.NotNull(checkPaymentUpdated);
                    Assert.Equal(checkPaymentUpdated.TenantId, TenantId);
                    Assert.False(string.IsNullOrWhiteSpace(checkPaymentUpdated.Id));
                    Assert.Equal(checkPaymentUpdated.LoanReferenceNumber, checkPayment.LoanReferenceNumber);
                    Assert.NotEqual(checkPaymentUpdated.Status, checkPayment.Status);
                }
            });
        }
    }

    public abstract class InMemoryObjects
    {
        public string TenantId { get; } = "my-tenant";

        public ICheckPaymentRequest InMemoryCheckPaymentRequest => new CheckPaymentRequest
        {
            LoanReferenceNumber = "",
            Method = PaymentMethod.Check,
            Type = PaymentType.Payoff,
            Code = "142",
            DueDate = new DateTimeOffset(DateTime.Now),
            Amount = 100.00,
            RoutingNumber = "123456",
            CheckNumber = "1234",
            BalanceAmount = 0.5
        };

        public ICheckPayment InMemoryCheckPayment
        {
            get
            {
                return new CheckPayment(new CheckPaymentRequest
                {
                    LoanReferenceNumber = "",
                    Method = PaymentMethod.Check,
                    Type = PaymentType.Payoff,
                    Code = "142",
                    DueDate = new DateTimeOffset(DateTime.Now),
                    Amount = 100.00,
                    RoutingNumber = "123456"
                });
            }
        }
    }
}