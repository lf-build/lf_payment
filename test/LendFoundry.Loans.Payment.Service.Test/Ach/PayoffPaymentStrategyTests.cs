﻿using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Security.Tokens;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System;
using Xunit;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Loans.Payment.Service.Test.Ach
{
    public class PayoffPaymentStrategyTests : InMemoryObjects
    {
        private new IPaymentStrategyFactory PaymentStrategyFactory => new PaymentStrategyFactory
            (
                Repository.Object,
                AchProxyFactory.Object,
                EventHubFactory.Object,
                TenantTime.Object,
                Calendar.Object,
                ConfigurationFactory.Object,
                TokenReader.Object,
                FeeService.Object,
                LoanService.Object,
                Logger.Object,
                PaymentConfigurationServiceFactory.Object
            );

        private IPaymentService Service
        {
            get
            {
                return new PaymentService
                    (
                        new PaymentConfiguration(),
                        PaymentStrategyFactory,
                        ReconciliationStrategyFactory.Object,
                        Repository.Object,
                        LoanService.Object,
                        TenantTime.Object,
                        PaymentPauseRepository.Object,
                        EventHub.Object,
                        Mock.Of<ILogger>()
                    );
            }
        }



        [Fact]
        public void AddPayment_When_ExecutionOk()
        {
            // arrange
            var payoff = (InMemoryInstallments.Sum(i => i.Expected.Principal) + InMemoryInstallments.First().Expected.Interest);

            var list = new[] { InMemoryAchPaymentRequest }.AsEnumerable();
            list = list.Select((p, i) =>
            {
                i++;
                p.LoanReferenceNumber = $"loan00{i}";
                p.Amount = payoff;
                p.Type = PaymentType.Payoff;
                return p;
            }).ToArray();

            var achPaymentRequest = list.FirstOrDefault() ?? InMemoryAchPaymentRequest;

            AchProxy.Setup(x => x.Queue(It.IsAny<IAchPayment>(), It.IsAny<DateTimeOffset>())).Returns(It.IsAny<string>());
            AchProxyFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(AchProxy.Object);

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(achPaymentRequest.DueDate);
            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

            Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Mock.Of<IDateInfo>());

            var tenantConfiguration = new TenantConfiguration
            {
                PaymentTimeLimit = "15",
                Timezone = null
            };
            ConfigurationFactory.Setup(x => x.Create<TenantConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()).Get())
                .Returns(tenantConfiguration);

            EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

            var payoffAmount = new PayoffAmount
            {
                Amount = payoff,
                ValidUntil = new DateTimeOffset(DateTime.Now)
            };
            LoanService.Setup(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()))
                .Returns(payoffAmount);

            PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>()))
                .Returns(new List<IPayment>());

            // act 
            var achPayment = Service.AddPayment(achPaymentRequest).FirstOrDefault();

            // assert
            if (achPayment == null)
                return;

            Assert.NotNull(achPayment);
            Assert.Equal(achPaymentRequest.LoanReferenceNumber, achPayment.LoanReferenceNumber);
            TenantTime.Verify(x => x.FromDate(It.IsAny<DateTime>()), Times.Once);
            LoanService.Verify(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()), Times.Once);
        }

        [Fact]
        public void AddPayment_When_PayoffAmount_Doesnt_Match()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryAchPaymentRequest }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.Type = PaymentType.Payoff;
                    return s;
                }).ToArray();

                var achPaymentRequest = list.FirstOrDefault() ?? InMemoryAchPaymentRequest;

                AchProxy.Setup(x => x.Queue(It.IsAny<IAchPayment>(), It.IsAny<DateTimeOffset>())).Returns(It.IsAny<string>());
                AchProxyFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(AchProxy.Object);

                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(achPaymentRequest.DueDate);
                TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

                Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Mock.Of<IDateInfo>());

                var tenantConfiguration = new TenantConfiguration
                {
                    PaymentTimeLimit = "15",
                    Timezone = null
                };
                ConfigurationFactory.Setup(x => x.Create<TenantConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()).Get())
                    .Returns(tenantConfiguration);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var payoffAmount = new PayoffAmount
                {
                    Amount = 0.00,
                    ValidUntil = new DateTimeOffset(DateTime.Now.AddDays(10))
                };

                LoanService.Setup(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()))
                    .Returns(payoffAmount);

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>()))
                .Returns(new List<IPayment>());

                // act 
                Service.AddPayment(achPaymentRequest);

                // assert
                LoanService.Verify(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()), Times.Once);
            });
        }

        [Fact]
        public void AddPayment_When_HasIncorrect_Amount()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryAchPaymentRequest }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.Amount = 0.00;
                    s.Type = PaymentType.Payoff;
                    return s;
                }).ToArray();

                var achPaymentRequest = list.FirstOrDefault() ?? InMemoryAchPaymentRequest;

                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(achPaymentRequest.DueDate);
                TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));

                Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Mock.Of<IDateInfo>());

                var tenantConfiguration = new TenantConfiguration
                {
                    PaymentTimeLimit = "15",
                    Timezone = null
                };
                ConfigurationFactory.Setup(x => x.Create<TenantConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()).Get())
                    .Returns(tenantConfiguration);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var payoffAmount = new PayoffAmount
                {
                    Amount = 100.00,
                    ValidUntil = new DateTimeOffset(DateTime.Now.AddDays(10))
                };

                LoanService.Setup(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()))
                    .Returns(payoffAmount);

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                // act 
                Service.AddPayment(achPaymentRequest);

            });
        }
    }
}