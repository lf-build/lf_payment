﻿using LendFoundry.Loans.Payment.Ach;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.MoneyMovement.Ach;
using Moq;
using System;
using Xunit;

namespace LendFoundry.Loans.Payment.Service.Test.Ach
{
    public class AchProxyTests : InMemoryObjects
    {
        private readonly Mock<IAchService> AchService = new Mock<IAchService>();

        private IAchProxy Proxy => new AchProxy(AchService.Object, Mock.Of<IPaymentConfiguration>());
            
        [Fact]
        public void Queue_When_ExecutionOk()
        {
            // arrange
            var executionDate = DateTime.Now.Date;

            var achPaymentRequest = InMemoryAchPaymentRequest;
            achPaymentRequest.Type = PaymentType.Scheduled;

            // Create method and this variable to implement UnitTest related this Object "IInstructionRequest"
            var fakeInstructionRequest = new InstructionRequest
            {
                FundingSourceId = "crb",
                ReferenceNumber = "loan001",
                ExecutionDate = executionDate,
                Receiving = new Receiving
                {
                    AccountNumber = achPaymentRequest.AccountNumber,
                    AccountType = AccountType.Individual,
                    Name = achPaymentRequest.AccountHolder,
                    RoutingNumber = achPaymentRequest.RoutingNumber
                },
                StandardEntryClassCode = "WEB",
                Amount = Convert.ToDecimal(achPaymentRequest.Amount),
                Frequency = achPaymentRequest.Type == PaymentType.Scheduled ? Frequency.Recurring : Frequency.Single,
                Type = achPaymentRequest.AccountType == BankAccountType.Checking ? TransactionType.DebitFromChecking : TransactionType.DebitFromSavings
            };

            // This code is needed to joint the properties from object above
            var fakeInstruction = new Instruction
            {
                Id = Guid.NewGuid().ToString(),
                Amount = fakeInstructionRequest.Amount,
                ExecutionDate = executionDate,
                Frequency = fakeInstructionRequest.Frequency
            };
            AchService.Setup(x => x.Queue(It.IsAny<IInstructionRequest>())).Returns(fakeInstruction);

            var achPayment = new AchPayment(achPaymentRequest)
            {
                Id = Guid.NewGuid().ToString(),
                TenantId = InMemoryTenant.Id
            };

            // act
            var returnQueue = Proxy.Queue(achPayment, executionDate);

            // assert
            Assert.Equal(fakeInstruction.Id, returnQueue);
            AchService.Verify(x => x.Queue(It.IsAny<IInstructionRequest>()), Times.AtLeast(1));
        }

        [Fact]
        public void Queue_When_ExecutionFailed()
        {
            Assert.Throws<NullReferenceException>(() =>
            {
                // arrange
                var executionDate = DateTime.Now.Date;

                var achPaymentRequest = InMemoryAchPaymentRequest;
                achPaymentRequest.Type = PaymentType.Scheduled;

                // Create method and this variable to implement UnitTest related this Object "IInstructionRequest"
                var fakeInstructionRequest = new InstructionRequest
                {
                    FundingSourceId = "crb",
                    ReferenceNumber = "loan001",
                    ExecutionDate = executionDate,
                    Receiving = new Receiving
                    {
                        AccountNumber = achPaymentRequest.AccountNumber,
                        AccountType = AccountType.Individual,
                        Name = achPaymentRequest.AccountHolder,
                        RoutingNumber = achPaymentRequest.RoutingNumber
                    },
                    StandardEntryClassCode = "WEB",
                    Amount = Convert.ToDecimal(achPaymentRequest.Amount),
                    Frequency = achPaymentRequest.Type == PaymentType.Scheduled ? Frequency.Recurring : Frequency.Single,
                    Type = achPaymentRequest.AccountType == BankAccountType.Checking ? TransactionType.DebitFromChecking : TransactionType.DebitFromSavings
                };

                // This code is needed to joint the properties from object above
                var fakeInstruction = new Instruction
                {
                    Id = Guid.NewGuid().ToString(),
                    Amount = fakeInstructionRequest.Amount,
                    ExecutionDate = executionDate,
                    Frequency = fakeInstructionRequest.Frequency
                };
                AchService.Setup(x => x.Queue(fakeInstructionRequest)).Returns(fakeInstruction);

                var achPayment = new AchPayment(achPaymentRequest)
                {
                    Id = Guid.NewGuid().ToString(),
                    TenantId = InMemoryTenant.Id
                };

                // act
                Proxy.Queue(achPayment, executionDate);

                // assert
                AchService.Verify(x => x.Queue(It.IsAny<IInstructionRequest>()), Times.AtLeast(1));
            });
        }


        [Fact]
        public void Queue_When_AddInvestorId()
        {
            // arrange
            var executionDate = DateTime.Now.Date;

            var achPaymentRequest = InMemoryAchPaymentRequest;
            achPaymentRequest.Type = PaymentType.Scheduled;

            // Create method and this variable to implement UnitTest related this Object "IInstructionRequest"
            var fakeInstructionRequest = new InstructionRequest
            {
                FundingSourceId = "crb",
                ReferenceNumber = "loan001",
                ExecutionDate = executionDate,
                Receiving = new Receiving
                {
                    AccountNumber = achPaymentRequest.AccountNumber,
                    AccountType = AccountType.Individual,
                    Name = achPaymentRequest.AccountHolder,
                    RoutingNumber = achPaymentRequest.RoutingNumber
                },
                StandardEntryClassCode = "WEB",
                Amount = Convert.ToDecimal(achPaymentRequest.Amount),
                Frequency = achPaymentRequest.Type == PaymentType.Scheduled ? Frequency.Recurring : Frequency.Single,
                Type = achPaymentRequest.AccountType == BankAccountType.Checking ? TransactionType.DebitFromChecking : TransactionType.DebitFromSavings
            };

            // This code is needed to joint the properties from object above
            var fakeInstruction = new Instruction
            {
                Id = Guid.NewGuid().ToString(),
                Amount = fakeInstructionRequest.Amount,
                ExecutionDate = executionDate,
                Frequency = fakeInstructionRequest.Frequency
            };
            AchService.Setup(x => x.Queue(It.IsAny<IInstructionRequest>())).Returns(fakeInstruction);

            var achPayment = new AchPayment(achPaymentRequest, fakeInstructionRequest.FundingSourceId)
            {
                Id = Guid.NewGuid().ToString(),
                TenantId = InMemoryTenant.Id
            };

            // act
            var returnQueue = Proxy.Queue(achPayment, executionDate);

            // assert
            Assert.Equal(fakeInstruction.Id, returnQueue);
            Assert.Equal(fakeInstructionRequest.FundingSourceId, achPayment.InvestorId);
            AchService.Verify(x => x.Queue(It.IsAny<IInstructionRequest>()), Times.AtLeast(1));
        }
    }
}