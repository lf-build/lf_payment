﻿using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Security.Tokens;
using Moq;
using System.Collections.Generic;
using System.Linq;
using System;
using Xunit;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Loans.Payment.Service.Test.Ach
{
    public class ExtraPaymentStrategyTests : InMemoryObjects
    {
        private IPaymentService PaymentService
        {
            get
            {
                return new PaymentService
                    (
                        new PaymentConfiguration(),
                        PaymentStrategyFactory,
                        ReconciliationStrategyFactory.Object,
                        Repository.Object,
                        LoanService.Object,
                        TenantTime.Object,
                        PaymentPauseRepository.Object,
                        EventHub.Object,
                        Mock.Of<ILogger>()
                    );
            }
        }


        [Fact]
        public void AddPayment_When_ExecutionOk()
        {
            // arrange
            var list = new[] { InMemoryAchPaymentRequest }.AsEnumerable();
            list = list.Select((p, i) =>
            {
                i++;
                p.LoanReferenceNumber = $"loan00{i}";
                p.Amount = 200.00;
                p.Type = PaymentType.Extra;
                p.DueDate = new DateTimeOffset(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0, TimeSpan.Zero);
                return p;
            }).ToArray();

            var achPaymentRequest = list.FirstOrDefault() ?? InMemoryAchPaymentRequest;

            AchProxy.Setup(x => x.Queue(It.IsAny<IAchPayment>(), It.IsAny<DateTimeOffset>())).Returns(It.IsAny<string>());
            AchProxyFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(AchProxy.Object);

            TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(achPaymentRequest.DueDate);

            Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Mock.Of<IDateInfo>());

            var tenantConfiguration = new TenantConfiguration
            {
                PaymentTimeLimit = "15",
                Timezone = null
            };
            ConfigurationFactory.Setup(x => x.Create<TenantConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()).Get())
                .Returns(tenantConfiguration);

            EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            var installments = new List<IInstallment>
            {
                Mock.Of<IInstallment>(i => i.DueDate == new DateTimeOffset(DateTime.Now.AddDays(-1)) && i.EndingBalance == loanAdapter.Loan.Terms.LoanAmount),
                Mock.Of<IInstallment>(i => i.DueDate == new DateTimeOffset(DateTime.Now.AddDays(2)) && i.EndingBalance == loanAdapter.Loan.Terms.LoanAmount)
            };
            LoanService.Setup(x => x.GetPaymentSchedule(It.IsAny<string>())).Returns(installments);

            PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>()))
                .Returns(new List<IPayment>());

            // act 
            var achPayment = PaymentService.AddPayment(achPaymentRequest).FirstOrDefault();

            // assert
            if (achPayment == null)
                return;
            
            Assert.NotNull(achPayment);
            Assert.Equal(achPaymentRequest.LoanReferenceNumber, achPayment.LoanReferenceNumber);
            TenantTime.Verify(x => x.FromDate(It.IsAny<DateTime>()), Times.Once);
            FeeService.Verify(x => x.Dequeue(It.IsAny<string>()), Times.Once);
            LoanService.Verify(x => x.GetLoanInfo(It.IsAny<string>()), Times.AtLeast(1));
            LoanService.Verify(x => x.GetPaymentSchedule(It.IsAny<string>()), Times.AtLeast(1));
        }

        [Fact]
        public void AddPayment_When_HasNo_Request()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                PaymentService.AddPayment(null);
            });
        }

        [Fact]
        public void AddPayment_When_HasIncorrect_Amount()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryAchPaymentRequest }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.Amount = 0.00;
                    s.Type = PaymentType.Extra;
                    return s;
                }).ToArray();

                var achPaymentRequest = list.FirstOrDefault() ?? InMemoryAchPaymentRequest;

                AchProxy.Setup(x => x.Queue(It.IsAny<IAchPayment>(), It.IsAny<DateTimeOffset>())).Returns(It.IsAny<string>());
                AchProxyFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(AchProxy.Object);

                TenantTime.Setup(x => x.Today)
                    .Returns(new DateTimeOffset(DateTime.Now));

                Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Mock.Of<IDateInfo>());

                var tenantConfiguration = new TenantConfiguration
                {
                    PaymentTimeLimit = "15",
                    Timezone = null
                };
                ConfigurationFactory.Setup(x => x.Create<TenantConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()).Get())
                    .Returns(tenantConfiguration);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>()))
                    .Returns(loanAdapter.Loan.Terms.OriginationDate.AddDays(5));

                var installments = new List<IInstallment>
                {
                    Mock.Of<IInstallment>(i => 
                        i.DueDate == new DateTimeOffset(DateTime.Now.AddDays(-1)) && 
                        i.EndingBalance == loanAdapter.Loan.Terms.LoanAmount
                    ),
                    Mock.Of<IInstallment>(i => 
                        i.DueDate == new DateTimeOffset(DateTime.Now.AddDays(2)) && 
                        i.EndingBalance == loanAdapter.Loan.Terms.LoanAmount
                    )
                };
                LoanService.Setup(x => x.GetPaymentSchedule(It.IsAny<string>())).Returns(installments);

                PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>()))
                    .Returns(new List<IPayment>());

                // act 
                PaymentService.AddPayment(achPaymentRequest);

            });
        }

        [Fact]
        public void AddPayment_When_ExtraPayment_IsGreatherThan_PrincipalBalance()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryAchPaymentRequest }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.Amount = 2000.00;
                    s.Type = PaymentType.Extra;
                    return s;
                }).ToArray();

                var achPaymentRequest = list.FirstOrDefault() ?? InMemoryAchPaymentRequest;

                AchProxy.Setup(x => x.Queue(It.IsAny<IAchPayment>(), It.IsAny<DateTimeOffset>())).Returns(It.IsAny<string>());
                AchProxyFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(AchProxy.Object);

                TenantTime.Setup(x => x.Today)
                    .Returns(new DateTimeOffset(DateTime.Now));

                Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Mock.Of<IDateInfo>());

                var tenantConfiguration = new TenantConfiguration
                {
                    PaymentTimeLimit = "15",
                    Timezone = null
                };
                ConfigurationFactory.Setup(x => x.Create<TenantConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()).Get())
                    .Returns(tenantConfiguration);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>()))
                    .Returns(loanAdapter.Loan.Terms.OriginationDate.AddDays(5));

                var installments = new List<IInstallment>
                {
                    Mock.Of<IInstallment>(i => 
                        i.DueDate == new DateTimeOffset(DateTime.Now.AddDays(-1)) && 
                        i.EndingBalance == loanAdapter.Loan.Terms.LoanAmount &&
                        i.Status == InstallmentStatus.Open
                    ),
                    Mock.Of<IInstallment>(i => 
                        i.DueDate == new DateTimeOffset(DateTime.Now.AddDays(2)) && 
                        i.EndingBalance == loanAdapter.Loan.Terms.LoanAmount &&
                        i.Status == InstallmentStatus.Open
                    )
                };
                LoanService.Setup(x => x.GetPaymentSchedule(It.IsAny<string>())).Returns(installments);

                PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>()))
                    .Returns(new List<IPayment>());

                // act 
                PaymentService.AddPayment(achPaymentRequest);

            });
        }

        [Fact]
        public void AddPayment_When_Payment_CannotBeScheduledBefore_OriginationDate()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryAchPaymentRequest }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.Amount = 2000.00;
                    s.Type = PaymentType.Extra;
                    return s;
                }).ToArray();

                var achPaymentRequest = list.FirstOrDefault() ?? InMemoryAchPaymentRequest;

                AchProxy.Setup(x => x.Queue(It.IsAny<IAchPayment>(), It.IsAny<DateTimeOffset>())).Returns(It.IsAny<string>());
                AchProxyFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())).Returns(AchProxy.Object);

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                TenantTime.Setup(x => x.Today)
                    .Returns(new DateTimeOffset(DateTime.Now));

                Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Mock.Of<IDateInfo>());

                var tenantConfiguration = new TenantConfiguration
                {
                    PaymentTimeLimit = "15",
                    Timezone = null
                };
                ConfigurationFactory.Setup(x => x.Create<TenantConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()).Get())
                    .Returns(tenantConfiguration);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>())
                    .Publish(It.IsAny<PaymentAdded>()));
                
                var installments = new List<IInstallment>
                {
                    Mock.Of<IInstallment>(i => 
                        i.DueDate == new DateTimeOffset(DateTime.Now.AddDays(-1)) && 
                        i.EndingBalance == loanAdapter.Loan.Terms.LoanAmount &&
                        i.Status == InstallmentStatus.Open
                    ),
                    Mock.Of<IInstallment>(i => 
                        i.DueDate == new DateTimeOffset(DateTime.Now.AddDays(2)) && 
                        i.EndingBalance == loanAdapter.Loan.Terms.LoanAmount &&
                        i.Status == InstallmentStatus.Open
                    )
                };
                LoanService.Setup(x => x.GetPaymentSchedule(It.IsAny<string>())).Returns(installments);

                PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>()))
                    .Returns(new List<IPayment>());

                // act 
                PaymentService.AddPayment(achPaymentRequest);

            });
        }
    }
}