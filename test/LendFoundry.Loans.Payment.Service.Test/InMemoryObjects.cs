using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Client.Models;
using LendFoundry.Loans.Fees;
using LendFoundry.Loans.Payment.Ach;
using LendFoundry.Loans.Schedule;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Moq;

namespace LendFoundry.Loans.Payment.Service.Test
{
    public abstract class InMemoryObjects
    {
        protected Mock<IPaymentRepository> Repository { get; } = new Mock<IPaymentRepository>();
        protected Mock<IPaymentPauseRepository> PaymentPauseRepository { get; } = new Mock<IPaymentPauseRepository>();
        protected Mock<IEventHubClient> EventHub { get; } = new Mock<IEventHubClient>();

        protected Mock<IAchProxy> AchProxy { get; } = new Mock<IAchProxy>();

        protected Mock<IAchProxyFactory> AchProxyFactory { get; } = new Mock<IAchProxyFactory>();

        protected Mock<IEventHubClientFactory> EventHubFactory { get; } = new Mock<IEventHubClientFactory>();

        protected Mock<ITenantTime> TenantTime { get; } = new Mock<ITenantTime>();

        protected Mock<ICalendarService> Calendar { get; } = new Mock<ICalendarService>();

        protected Mock<IConfigurationServiceFactory> ConfigurationFactory { get; } = new Mock<IConfigurationServiceFactory>();

        protected Mock<IConfigurationServiceFactory<PaymentConfiguration>> PaymentConfigurationServiceFactory { get; } = new Mock<IConfigurationServiceFactory<PaymentConfiguration>>();

        protected Mock<ITokenReader> TokenReader { get; } = new Mock<ITokenReader>();

        protected Mock<ILoanFeeService> FeeService { get; } = new Mock<ILoanFeeService>();

        protected Mock<ILogger> Logger { get; } = new Mock<ILogger>();

        protected IPaymentStrategyFactory PaymentStrategyFactory => new PaymentStrategyFactory
	        (
	        Repository.Object,
	        AchProxyFactory.Object,
	        EventHubFactory.Object,
	        TenantTime.Object,
	        Calendar.Object,
	        ConfigurationFactory.Object,
	        TokenReader.Object,
	        FeeService.Object,
	        LoanService.Object,
	        Logger.Object,
	        PaymentConfigurationServiceFactory.Object
	        );

	    protected Mock<IPaymentStrategy> PaymentStrategy { get; } = new Mock<IPaymentStrategy>();

        protected Mock<IReconciliationStrategyFactory> ReconciliationStrategyFactory { get; } = new Mock<IReconciliationStrategyFactory>();

        protected Mock<IReconciliationStrategy> ReconciliationStrategy { get; } = new Mock<IReconciliationStrategy>();

        protected Mock<ILoanService> LoanService { get; } = new Mock<ILoanService>();

        public TenantInfo InMemoryTenant => new TenantInfo { Id = "my-tenant" };

	    public ICheckPaymentRequest InMemoryCheckPaymentRequest => new CheckPaymentRequest
        {
            LoanReferenceNumber = "",
            Method = PaymentMethod.Check,
            Type = PaymentType.Fee,
            Code = "142",
            DueDate = new DateTimeOffset(DateTime.Now),
            RoutingNumber = "001",
            CheckNumber = "219",
            CheckAmount = 337.92,
            Notes = "This PaymentRequest is a real payment based in the LoanInfo"
        };

        public IPayment InMemoryCheckPayment => new CheckPayment(InMemoryCheckPaymentRequest)
        {
            Id = Guid.NewGuid().ToString(),
            TenantId = InMemoryTenant.Id
        };


        public IAchPaymentRequest InMemoryAchPaymentRequest => new AchPaymentRequest
        {
            LoanReferenceNumber = "",
            Method = PaymentMethod.ACH,
            Type = PaymentType.Payoff,
            Code = "142",
            DueDate = new DateTimeOffset(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, 0, TimeSpan.Zero),
            Amount = 337.92,
            RoutingNumber = "001",
            AccountHolder = "Bank of America",
            AccountNumber = "219",
            AccountType = BankAccountType.Checking
        };

        public IPayment InMemoryAchPayment => new AchPayment(InMemoryAchPaymentRequest)
        {
            Id = Guid.NewGuid().ToString(),
            TenantId = InMemoryTenant.Id
        };

        public IEnumerable<IInstallment> InMemoryInstallments
        {
            get
            {
                return new[]
                {
                    Mock.Of<IInstallment>(i =>
                        i.PaymentMethod == PaymentMethod.ACH &&
                        i.AnniversaryDate == new DateTimeOffset(new DateTime(2016, 02, 15)) &&
                        i.DueDate == new DateTimeOffset(new DateTime(2016, 02, 15)) &&
                        i.OpeningBalance == 1000 &&
                        i.Expected == Mock.Of<IPaydown>(p => p.Amount == 337.92 && p.Principal == 331.05 && p.Interest == 6.87) &&
                        i.EndingBalance == 668.95 &&
                        i.Status == InstallmentStatus.Open &&
                        i.Type == InstallmentType.Installment
                    ),
                    Mock.Of<IInstallment>(i =>
                        i.PaymentMethod == PaymentMethod.ACH &&
                        i.AnniversaryDate == new DateTimeOffset(new DateTime(2016, 03, 15)) &&
                        i.DueDate == new DateTimeOffset(new DateTime(2016, 03, 15)) &&
                        i.OpeningBalance == 668.95 &&
                        i.Expected == Mock.Of<IPaydown>(p => p.Amount == 337.92 && p.Principal == 333.33 && p.Interest == 4.59) &&
                        i.EndingBalance == 335.62 &&
                        i.Status == InstallmentStatus.Open &&
                        i.Type == InstallmentType.Installment
                    ),
                    Mock.Of<IInstallment>(i =>
                        i.PaymentMethod == PaymentMethod.ACH &&
                        i.AnniversaryDate == new DateTimeOffset(new DateTime(2016, 04, 15)) &&
                        i.DueDate == new DateTimeOffset(new DateTime(2016, 04, 15)) &&
                        i.OpeningBalance == 335.62 &&
                        i.Expected == Mock.Of<IPaydown>(p => p.Amount == 337.92 && p.Principal == 335.62 && p.Interest == 2.3) &&
                        i.EndingBalance == 0 &&
                        i.Status == InstallmentStatus.Open &&
                        i.Type == InstallmentType.Installment
                    )
                };
            }
        } 

        public static LoanInfo GetLoanInfo(string loanReferenceNumber = null)
        {
            var loanInfo = new LoanInfo
            {
                ReferenceNumber = loanReferenceNumber,
                FundingSource = "crb",
                Purpose = "Home improvement",
                Status = new Loans.Client.Models.LoanStatus(),
                Investor = new LoanInvestor
                {
                    Id = "1001",
                    LoanPurchaseDate = new DateTimeOffset(new DateTime(2016, 01, 01))
                },
                CurrentDue = 679.72,
                DaysPastDue = 36,
                LastPaymentAmount = 0,
                LastPaymentDate = new DateTimeOffset(DateTime.Now),
                NextDueDate = new DateTimeOffset(DateTime.Now),
                NextInstallmentAmount = 337.92,
                RemainingBalance = 1000,
                Scores = new[]
                {
                    new Score
                    {
                        Name = "ficoRiskScore09",
                        InitialDate = new DateTimeOffset(new DateTime(2016, 02, 13)),
                        InitialScore = "500",
                        UpdatedDate = new DateTimeOffset(new DateTime(2016, 02, 14)),
                        UpdatedScore = "601"
                    },
                    new Score
                    {
                        Name = "Acme",
                        InitialDate = new DateTimeOffset(new DateTime(2016, 02, 11)),
                        InitialScore = "ABC"
                    }
                },
                Grade = "B",
                PreCloseDti = 5.7,
                PostCloseDti = 6.4,
                MonthlyIncome = 7000,
                HomeOwnership = "Owner",
                CampaignCode = "190C-2",
                Borrowers = new[]
                {
                    new Loans.Client.Models.Borrower
                    {
                        ReferenceNumber = "0001",
                        IsPrimary = true,
                        FirstName = "John",
                        MiddleInitial = "F",
                        LastName = "Doe",
                        DateOfBirth = new DateTimeOffset(new DateTime(1982, 5, 11)),
                        SSN = "103-58-4851",
                        Email = "johndoe@lendfoundry.com",
                        Addresses = new[]
                        {
                            new Loans.Client.Models.Address
                            {
                                IsPrimary = true,
                                Line1 = "145 5th Ave",
                                City = "Irvine",
                                State = "CA",
                                ZipCode = "12952"
                            }
                        },
                        Phones = new[]
                        {
                            new Loans.Client.Models.Phone
                            {
                                Type = PhoneType.Home,
                                IsPrimary = true,
                                Number = "23938439"
                            }
                        }
                    }
                },
                Terms = new Loans.Client.Models.LoanTerms
                {
                    ApplicationDate = new DateTimeOffset(new DateTime(2016, 01, 09)),
                    APR = 164.85,
                    FundedDate = new DateTimeOffset(new DateTime(2016, 01, 17)),
                    LoanAmount = 1000,
                    OriginationDate = new DateTimeOffset(new DateTime(2016, 01, 15)),
                    Term = 3,
                    Rate = 8.24,
                    RateType = RateType.Percent,
                    Fees = new[]
                    {
                        new Loans.Client.Models.Fee
                        {
                            Amount = 212,
                            Code = "142",
                            Type = FeeType.Fixed
                        }
                    }
                },
                PaymentMethod = PaymentMethod.ACH,
            };
            return loanInfo;
        }

        public class InMemoryLoanBackupAdapter : ILoanInfo
        {
            public InMemoryLoanBackupAdapter(LoanInfo model)
            {
                Loan = new Loan
                {
                    ReferenceNumber = model.ReferenceNumber,
                    FundingSource = model.FundingSource,
                    Purpose = model.Purpose,
                    Investor = new LoanInvestor
                    {
                        Id = model.Investor.Id,
                        LoanPurchaseDate = model.Investor.LoanPurchaseDate
                    },
                    Scores = new[]
                    {
                        new Score
                        {
                            Name = "ficoRiskScore09",
                            InitialDate = new DateTimeOffset(new DateTime(2016, 02, 13)),
                            InitialScore = "500",
                            UpdatedDate = new DateTimeOffset(new DateTime(2016, 02, 14)),
                            UpdatedScore = "601"
                        },
                        new Score
                        {
                            Name = "Acme",
                            InitialDate = new DateTimeOffset(new DateTime(2016, 02, 11)),
                            InitialScore = "ABC"
                        }
                    },
                    Grade = model.Grade,
                    PreCloseDti = model.PreCloseDti,
                    PostCloseDti = model.PostCloseDti,
                    MonthlyIncome = model.MonthlyIncome,
                    HomeOwnership = model.HomeOwnership,
                    CampaignCode = model.CampaignCode,
                    Borrowers = new[]
                    {
                        new Borrower
                        {
                            ReferenceNumber = "0001",
                            IsPrimary = true,
                            FirstName = "John",
                            MiddleInitial = "F",
                            LastName = "Doe",
                            DateOfBirth = new DateTimeOffset(new DateTime(1982, 5, 11)),
                            SSN = "103-58-4851",
                            Email = "johndoe@lendfoundry.com",
                            Addresses = new[]
                            {
                                new Address
                                {
                                    IsPrimary = true,
                                    Line1 = "145 5th Ave",
                                    City = "Irvine",
                                    State = "CA",
                                    ZipCode = "12952"
                                }
                            },
                            Phones = new[]
                            {
                                new Phone
                                {
                                    Type = PhoneType.Home,
                                    IsPrimary = true,
                                    Number = "23938439"
                                }
                            }
                        }
                    },
                    Terms = new LoanTerms
                    {
                        ApplicationDate = model.Terms.ApplicationDate,
                        APR = model.Terms.APR,
                        FundedDate = model.Terms.FundedDate,
                        LoanAmount = model.Terms.LoanAmount,
                        OriginationDate = model.Terms.OriginationDate,
                        Term = model.Terms.Term,
                        Rate = model.Terms.Rate,
                        RateType = model.Terms.RateType,
                        Fees = new[]
                        {
                            new Fee
                            {
                                Amount = model.Terms.Fees[0].Amount,
                                Code = model.Terms.Fees[0].Code,
                                Type = model.Terms.Fees[0].Type
                            }
                        }
                    },
                    PaymentMethod = model.PaymentMethod,
                };

                Summary = new PaymentScheduleSummary
                {
                    CurrentDue = model.CurrentDue,
                    DaysPastDue = model.DaysPastDue,
                    LastPaymentAmount = model.LastPaymentAmount,
                    LastPaymentDate = model.LastPaymentDate,
                    NextDueDate = model.NextDueDate,
                    NextInstallmentAmount = model.NextInstallmentAmount,
                    RemainingBalance = model.RemainingBalance
                };
            }

            public ILoan Loan { get; }

            public IPaymentScheduleSummary Summary { get; }
        }
    }
}