﻿using LendFoundry.Foundation.Services;
using Moq;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;
using System;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Loans.Payment.Service.Fake;
using Xunit;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Loans.Payment.Service.Test
{
    public class PaymentServiceTests : InMemoryObjects
    {
        private new Mock<IPaymentStrategyFactory> PaymentStrategyFactory { get; } = new Mock<IPaymentStrategyFactory>();

        private IPaymentService Service
        {
            get
            {
                return new PaymentService
                    (
                        new PaymentConfiguration(),
                        PaymentStrategyFactory.Object,
                        ReconciliationStrategyFactory.Object,
                        Repository.Object,
                        LoanService.Object,
                        TenantTime.Object,
                        PaymentPauseRepository.Object,
                        EventHub.Object,
                        Mock.Of<ILogger>()
                    );
            }
        }

        [Fact]
        public void AddPayment_WhenExecutionOk()
        {
            // arrange
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((s, i) =>
            {
                i++;
                s.LoanReferenceNumber = $"loan00{i}";
                s.CheckNumber = "2000";
                //s.TenantId = TenantId;
                return s;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

            TenantTime.Setup(t => t.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

            Repository.Setup(r => r.All(It.IsAny<Expression<Func<Payment, bool>>>())).Returns(new List<Payment>());
            
            PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>())).Returns(new List<IPayment> { new CheckPayment(checkPaymentRequest) });

            PaymentStrategyFactory.Setup(x => x.Create(It.IsAny<PaymentMethod>(), It.IsAny<PaymentType>())).Returns(PaymentStrategy.Object);

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            // act
            var payment = Service.AddPayment(checkPaymentRequest).FirstOrDefault();

            // assert
            if (payment == null)
                return;

            Assert.NotNull(payment);
            Assert.Equal(checkPaymentRequest.LoanReferenceNumber, payment.LoanReferenceNumber);
            
            TenantTime.Verify(x => x.FromDate(It.IsAny<DateTime>()), Times.Once);
            PaymentStrategy.Verify(x => x.AddPayment(It.IsAny<IPaymentRequest>()), Times.Once);
            PaymentStrategyFactory.Verify(x => x.Create(It.IsAny<PaymentMethod>(), It.IsAny<PaymentType>()), Times.Once);
        }

        [Fact]
        public void AddPayment_When_HasIncorrect_Amount()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((p, i) =>
                {
                    i++;
                    p.LoanReferenceNumber = $"loan00{i}";
                    p.CheckNumber = "2000";
                    p.Amount = 50.00;
                    p.Type = PaymentType.Extra;
                    return p;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                TenantTime.Setup(t => t.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

                var checkPayment = new CheckPayment(checkPaymentRequest)
                {
                    Id = Guid.NewGuid().ToString(),
                    TenantId = InMemoryTenant.Id,
                    Status = PaymentStatus.Cancelled
                };

                Repository.Setup(r => r.All(It.IsAny<Expression<Func<Payment, bool>>>())).Returns(new Payment[] { checkPayment });

                PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>())).Returns(new List<IPayment> { new CheckPayment(checkPaymentRequest) });

                PaymentStrategyFactory.Setup(x => x.Create(It.IsAny<PaymentMethod>(), It.IsAny<PaymentType>())).Returns(PaymentStrategy.Object);

                // act
                Service.AddPayment(checkPaymentRequest);
            });
        }

        [Fact]
        public void AddPayment_When_ThrowException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // act
                Service.AddPayment(null);
            });
        }

        [Fact]
        public void AddPayment_When_LoanNotFound()
        {
            Assert.Throws<LoanNotFoundException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((p, i) =>
                {
                    i++;
                    p.LoanReferenceNumber = $"loan00{i}";
                    p.CheckNumber = "2000";
                    return p;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                TenantTime.Setup(t => t.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

                Repository.Setup(r => r.All(It.IsAny<Expression<Func<Payment, bool>>>())).Returns(new List<Payment>());

                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>()));

                // act
                Service.AddPayment(checkPaymentRequest);

                // assert
                TenantTime.Verify(x => x.FromDate(It.IsAny<DateTime>()), Times.Once);

                LoanService.Verify(x => x.GetLoanInfo(It.IsAny<string>()), Times.Once);
            });
        }

        [Fact]
        public void AddPayment_When_OriginationDate_IsGreaterThan_PaymentDueDate()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                var loanInfo = GetLoanInfo("loan001");
                var checkPaymentRequest = InMemoryCheckPaymentRequest;

                checkPaymentRequest.LoanReferenceNumber = "loan001";
                checkPaymentRequest.CheckNumber = "2000";
                checkPaymentRequest.DueDate = loanInfo.Terms.OriginationDate.AddDays(-1);

                TenantTime.Setup(t => t.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);
                Repository.Setup(r => r.All(It.IsAny<Expression<Func<Payment, bool>>>())).Returns(new List<Payment>());
                PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>())).Returns(new List<IPayment> { new CheckPayment(checkPaymentRequest) });
                PaymentStrategyFactory.Setup(x => x.Create(It.IsAny<PaymentMethod>(), It.IsAny<PaymentType>())).Returns(PaymentStrategy.Object);

                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                Service.AddPayment(checkPaymentRequest);

                TenantTime.Verify(x => x.FromDate(It.IsAny<DateTime>()), Times.Once);
                LoanService.Verify(x => x.GetLoanInfo(It.IsAny<string>()), Times.Once);
            });
        }


    
        [Fact]
        public void CancelPayment_WhenExecutionOK()
        {
            // arrange
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((s, i) =>
            {
                i++;
                s.LoanReferenceNumber = $"loan00{i}";
                s.CheckNumber = "2000";
                //s.TenantId = TenantId;
                return s;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;
            var checkPayment = new CheckPayment(checkPaymentRequest)
            {
                Id = Guid.NewGuid().ToString(),
                TenantId = InMemoryTenant.Id,
                Status = PaymentStatus.Cancelled
            };

            Repository.Setup(r => r.All(It.IsAny<Expression<Func<Payment, bool>>>())).Returns(new List<Payment>());

            PaymentStrategy.Setup(x => x.CancelPayment(checkPayment, It.IsAny<string>()));

            PaymentStrategyFactory.Setup(x => x.Create(It.IsAny<PaymentMethod>(), It.IsAny<PaymentType>())).Returns(PaymentStrategy.Object);

            var responseTask = Task.FromResult(checkPayment as IPayment);
            Repository.Setup(r => r.Get(It.IsAny<string>())).Returns(responseTask);

            // act
            Service.CancelPayment(checkPayment.Id, "PaymentServiceTest");

            // assert
            Assert.Equal(checkPaymentRequest.LoanReferenceNumber, checkPayment.LoanReferenceNumber);
            Assert.Equal(PaymentStatus.Cancelled, checkPayment.Status);
            
            PaymentStrategy.Verify(x => x.CancelPayment(checkPayment, It.IsAny<string>()), Times.Once);
            PaymentStrategyFactory.Verify(x => x.Create(It.IsAny<PaymentMethod>(), It.IsAny<PaymentType>()), Times.Once);
        }

        [Fact]
        public void CancelPayment_WhenExecutionFailed()
        {
            Assert.Throws<NotFoundException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.CheckNumber = "2000";
                    //s.TenantId = TenantId;
                    return s;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;
                var checkPayment = new CheckPayment(checkPaymentRequest)
                {
                    Id = Guid.NewGuid().ToString(),
                    TenantId = InMemoryTenant.Id,
                    Status = PaymentStatus.Cancelled
                };

                Repository.Setup(r => r.All(It.IsAny<Expression<Func<Payment, bool>>>())).Returns(new List<Payment>());

                PaymentStrategy.Setup(x => x.CancelPayment(checkPayment, It.IsAny<string>()));

                PaymentStrategyFactory.Setup(x => x.Create(It.IsAny<PaymentMethod>(), It.IsAny<PaymentType>())).Returns(PaymentStrategy.Object);

                var responseTask = Task.FromResult(checkPayment as IPayment);
                Repository.Setup(r => r.Get("WrongId")).Returns(responseTask);

                // act
                Service.CancelPayment(checkPayment.Id, "PaymentServiceTest");
            });
        }

        [Fact]
        public void CancelPayment_When_ThrowException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // act
                Service.CancelPayment(new CheckPayment(InMemoryCheckPaymentRequest).Id, "PaymentServiceTest");
            });
        }

	    [Fact]
        public void ReconcilePendingPayments_WhenExecutionOk()
        {
            // arrange
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((s, i) =>
            {
                i++;
                s.LoanReferenceNumber = $"loan00{i}";
                s.CheckNumber = "2000";
                //s.TenantId = TenantId;
                return s;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;
            TenantTime.Setup(t => t.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

            ReconciliationStrategy.Setup(x => x.ReconcilePendingPayments(It.IsAny<DateTime>()));

            ReconciliationStrategyFactory.Setup(x => x.Create(It.IsAny<PaymentMethod>())).Returns(ReconciliationStrategy.Object);
            
            // act
            Service.ReconcilePendingPayments(PaymentMethod.Check, DateTime.Now);

            // assert
            ReconciliationStrategyFactory.Verify(x => x.Create(It.IsAny<PaymentMethod>()), Times.Once);
        }

        [Fact]
        public void ReconcilePendingPayments_When_ThrowException()
        {
            Assert.Throws<ArgumentNullException>(() =>
            {
                // arrange
                var paymentMethod = PaymentMethod.Undefined;

                // act
                Service.ReconcilePendingPayments(paymentMethod, DateTime.Now);
            });
        }

        [Fact]
        public void ShouldEnterEntryInPaymentPause()
        {
            var eventHub = new FakeEventHub();
            var paymentService = GetPaymentService(eventHub);
            const string referenceNumber = "123";
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date,
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(3),
                Notes = "This should not run for next three days"
            };

            var task = paymentService.Pause(referenceNumber, expectedPaymentPause);
            task.Wait();

            var result = paymentService.GetPauseByLoanReferenceNumber(referenceNumber).Result;
            Assert.Equal(referenceNumber, result.LoanReferenceNumber);
            Assert.Equal(expectedPaymentPause.StartDate, result.StartDate.Date);
            Assert.Equal(expectedPaymentPause.EndDate, result.EndDate.Date);
            Assert.Equal(expectedPaymentPause.Notes, result.Notes);
            Assert.NotNull(eventHub.PublishedEvents.FirstOrDefault(p => p.EventName == typeof(PaymentPaused).Name));
        }

        [Fact]
        public void ShouldEnterWithNoEndDateEntryInPaymentPause()
        {
            var eventHub = new FakeEventHub();
            var paymentService = GetPaymentService(eventHub);
            const string referenceNumber = "123";
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date,
                EndDate = null,
                Notes = "This should not run for next three days"
            };

            var task = paymentService.Pause(referenceNumber, expectedPaymentPause);
            task.Wait();

            var result = paymentService.GetPauseByLoanReferenceNumber(referenceNumber).Result;
            Assert.Equal(referenceNumber, result.LoanReferenceNumber);
            Assert.Equal(expectedPaymentPause.StartDate, result.StartDate.Date);
            Assert.Equal(default(DateTimeOffset), result.EndDate);
            Assert.Equal(expectedPaymentPause.Notes, result.Notes);
            Assert.NotNull(eventHub.PublishedEvents.FirstOrDefault(p => p.EventName == typeof(PaymentPaused).Name));
        }

        [Fact]
        public void ShouldThrowExceptionOnMissingReferenceNumber()
        {
            var paymentService = GetPaymentService();
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date.AddDays(1),
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(3),
                Notes = "This should not run for next three days"
            };

            var exception =
                Assert.ThrowsAsync<ArgumentException>(async () => await paymentService.Pause(null, expectedPaymentPause))
                    .Result;

            Assert.Contains("Loan Reference Number is required", exception.Message);
        }

        [Fact]
        public void ShouldThrowExceptionOnInvalidStartDate()
        {
            const string referenceNumber = "123";
            var paymentService = GetPaymentService();
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.AddDays(-1).Date,
                EndDate = DateTimeOffset.UtcNow.AddDays(3).Date,
                Notes = "This should not run for next three days"
            };

            var exception =
                Assert.ThrowsAsync<ArgumentException>(
                        async () => await paymentService.Pause(referenceNumber, expectedPaymentPause))
                    .Result;

            Assert.Equal("StartDate should be greater than todays date", exception.Message);

        }

        [Fact]
        public void ShouldThrowExceptionOnInvalidEndDate()
        {
            const string referenceNumber = "123";
            var paymentService = GetPaymentService();
            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.AddDays(1).Date,
                EndDate = DateTimeOffset.UtcNow.AddDays(-3).Date,
                Notes = "This should not run for next three days"
            };

            var exception =
                Assert.ThrowsAsync<ArgumentException>(async () => await paymentService.Pause(referenceNumber, expectedPaymentPause))
                    .Result;

            Assert.Equal("EndDate should be greater than start date", exception.Message);

        }

        [Fact]
        public void ShouldThrowExceptionOnExistingActivePause()
        {
            const string referenceNumber = "123";
            var paymentService = GetPaymentService();

            paymentService.Pause(referenceNumber, new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date,
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(3),
                Notes = "This should not run for next three days"
            }).Wait();

            Assert.ThrowsAsync<ActivePauseAlreadyExistException>(async () => await paymentService.Pause(referenceNumber, new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date.AddDays(1),
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(10),
                Notes = "This should not run for next three days"
            })).Wait();

        }

        [Fact]
        public void ShouldReplaceExistingInActivePause()
        {
            const string referenceNumber = "123";

            var fakeTenant = new FakeTenantService();
            var repository = new FakePaymentPauseRepository(fakeTenant, new UtcTenantTime(),
                new List<IPaymentPause>()
                {
                    new PaymentPause()
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "123",
                        StartDate = DateTimeOffset.UtcNow.Date.AddDays(-10),
                        EndDate = DateTimeOffset.UtcNow.Date.AddDays(-7),
                        Notes = "This should not run for next three days"
                    }
                });

            var paymentService = GetPaymentService(paymentRepository: repository);

            var expectedPaymentPause = new PaymentPauseRequest()
            {
                StartDate = DateTimeOffset.UtcNow.Date,
                EndDate = DateTimeOffset.UtcNow.Date.AddDays(3),
                Notes = "This should not run for next three days"
            };

            paymentService.Pause(referenceNumber, expectedPaymentPause).Wait();

            var result = paymentService.GetPauseByLoanReferenceNumber(referenceNumber).Result;
            Assert.Equal(referenceNumber, result.LoanReferenceNumber);
            Assert.Equal(expectedPaymentPause.StartDate, result.StartDate.Date);
            Assert.Equal(expectedPaymentPause.EndDate.Value, result.EndDate.Date);
            Assert.Equal(expectedPaymentPause.Notes, result.Notes);
            Assert.Equal(1, repository.InMemoryCollection.Count);

        }

        [Fact]
        public void ShouldDeleteExistingActivePause()
        {
            var eventHub = new FakeEventHub();
            var fakeTenant = new FakeTenantService();
            var repository = new FakePaymentPauseRepository(fakeTenant, new UtcTenantTime(),
                new List<IPaymentPause>()
                {
                    new PaymentPause()
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "123",
                        StartDate = DateTimeOffset.UtcNow.AddDays(-3),
                        EndDate = DateTimeOffset.UtcNow.AddDays(3),
                        Notes = "This should not run for next three days"
                    }
                });

            var paymentService = GetPaymentService(paymentRepository: repository, eventHubClient: eventHub);
            paymentService.Resume("123").Wait();
            Assert.Equal(0, repository.InMemoryCollection.Count);
            Assert.NotNull(eventHub.PublishedEvents.FirstOrDefault(p => p.EventName == typeof(PaymentResumed).Name));

        }

        [Fact]
        public void DeleteShouldWorkIfActivePauseNotExist()
        {
            var fakeTenant = new FakeTenantService();
            var repository = new FakePaymentPauseRepository(fakeTenant, new UtcTenantTime(),
                new List<IPaymentPause>()
                {
                    new PaymentPause()
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "123",
                        StartDate = DateTimeOffset.UtcNow.AddDays(-10),
                        EndDate = DateTimeOffset.UtcNow.AddDays(-7),
                        Notes = "This should not run for next three days"
                    }
                });

            var paymentService = GetPaymentService(paymentRepository: repository);
            paymentService.Resume("123").Wait();
            Assert.Equal(0, repository.InMemoryCollection.Count);

        }

        [Fact]
        public void DeleteShouldWorkIfNoPauseExist()
        {
            var fakeTenant = new FakeTenantService();
            var repository = new FakePaymentPauseRepository(fakeTenant, new UtcTenantTime(), new List<IPaymentPause>());

            var paymentService = GetPaymentService(paymentRepository: repository);
            paymentService.Resume("123456").Wait();
            Assert.Equal(0, repository.InMemoryCollection.Count);

        }

        [Fact]
        public void GetAllShouldReturnActivePauseOnly()
        {
            var fakeTenant = new FakeTenantService();
            var tenantTime = new UtcTenantTime();
            var repository = new FakePaymentPauseRepository(fakeTenant, tenantTime,
                new List<IPaymentPause>
                {
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "123",
                        StartDate = tenantTime.Today.AddDays(-10),
                        EndDate = tenantTime.Today.AddDays(-7),
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "456",
                        StartDate = tenantTime.Today,
                        EndDate = tenantTime.Today.AddDays(3),
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "654",
                        StartDate = tenantTime.Today,
                        EndDate = tenantTime.Today.AddDays(4),
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "999",
                        StartDate = tenantTime.Today,
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "789",
                        StartDate = tenantTime.Today.AddDays(-4),
                        EndDate = tenantTime.Today.AddDays(-3),
                        Notes = "This should not run for next three days"
                    },
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "987",
                        StartDate = tenantTime.Today.AddDays(10),
                        EndDate = tenantTime.Today.AddDays(13),
                        Notes = "This should not run for next three days"
                    }
                });

            var paymentService = GetPaymentService(paymentRepository: repository);
            var activePauses = paymentService.GetPauses().Result.ToList();
            Assert.Equal(3, activePauses.Count());
            Assert.Contains(activePauses, a => a.LoanReferenceNumber == "456");
            Assert.Contains(activePauses, a => a.LoanReferenceNumber == "654");
            Assert.Contains(activePauses, a => a.LoanReferenceNumber == "999");

        }

        [Fact]
        public void GetByReferenceNumberShouldReturnActivePauseOnly()
        {

            var fakeTenant = new FakeTenantService();
            var tenantTime = new UtcTenantTime();

            var expectedPaymentPause = new PaymentPause
            {
                TenantId = fakeTenant.Current.Id,
                LoanReferenceNumber = "123",
                StartDate = tenantTime.Today,
                EndDate = tenantTime.Today.AddDays(3),
                Notes = "This should not run for next three days"
            };

            var repository = new FakePaymentPauseRepository(fakeTenant, tenantTime,
                new List<IPaymentPause>
                {
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "123",
                        StartDate = tenantTime.Today.AddDays(-10),
                        EndDate = tenantTime.Today.AddDays(-7),
                        Notes = "This should not run for next three days"
                    },
                    expectedPaymentPause
                });

            var paymentService = GetPaymentService(paymentRepository: repository);
            var result = paymentService.GetPauseByLoanReferenceNumber("123").Result;
            Assert.Equal(expectedPaymentPause.LoanReferenceNumber, result.LoanReferenceNumber);
            Assert.Equal(expectedPaymentPause.StartDate, result.StartDate);
            Assert.Equal(expectedPaymentPause.EndDate, result.EndDate);
            Assert.Equal(result.Notes, result.Notes);

        }

        [Fact]
        public void ChequePaymentShouldBeAddedEventIfPaymentPaused()
        {            // arrange
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((s, i) =>
            {
                i++;
                s.LoanReferenceNumber = $"loan00{i}";
                s.CheckNumber = "2000";
                //s.TenantId = TenantId;
                return s;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

            TenantTime.Setup(t => t.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

            Repository.Setup(r => r.All(It.IsAny<Expression<Func<Payment, bool>>>())).Returns(new List<Payment>());

            PaymentStrategy.Setup(x => x.AddPayment(It.IsAny<IPaymentRequest>())).Returns(new List<IPayment> { new CheckPayment(checkPaymentRequest) });

            PaymentStrategyFactory.Setup(x => x.Create(It.IsAny<PaymentMethod>(), It.IsAny<PaymentType>())).Returns(PaymentStrategy.Object);

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            var fakeTenant = new FakeTenantService();
            var tenantTime = new UtcTenantTime();

            var repository = new FakePaymentPauseRepository(fakeTenant, tenantTime,
                new List<IPaymentPause>
                {
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber =checkPaymentRequest.LoanReferenceNumber,
                        StartDate = tenantTime.Today,
                        EndDate = tenantTime.Today.AddDays(3),
                        Notes = "This should not run for next three days"
                    }
                });

            var paymentService = GetPaymentService(paymentRepository: repository);
            var result=paymentService.AddPayment(new CheckPayment(checkPaymentRequest));
            Assert.NotNull(result);
            Assert.Equal(1,result.Count());

        }

        [Fact]
        public void AchPaymentShouldNotBeAddedIfPaymentPaused()
        {
            var fakeTenant = new FakeTenantService();
            var tenantTime = new UtcTenantTime();

            var repository = new FakePaymentPauseRepository(fakeTenant, tenantTime,
                new List<IPaymentPause>
                {
                    new PaymentPause
                    {
                        TenantId = fakeTenant.Current.Id,
                        LoanReferenceNumber = "123",
                        StartDate = tenantTime.Today,
                        EndDate = tenantTime.Today.AddDays(3),
                        Notes = "This should not run for next three days"
                    }
                });

            var paymentService = GetPaymentService(paymentRepository: repository);
            var exception = Assert.Throws<ArgumentException>(() => paymentService.AddPayment(new AchPaymentRequest()
            {
                AccountHolder = "account-holder",
                AccountNumber = "123",
                Method = PaymentMethod.ACH,
                LoanReferenceNumber = "123",
                AccountType = BankAccountType.Savings,
                Amount = 200.52,
                DueDate = tenantTime.Now,
                Code = "123",
                RoutingNumber = "123",
                Type = PaymentType.Scheduled
            }));

            Assert.Equal(exception.Message, "Payment cannot be scheduled as payment is paused.");


        }

        private IPaymentService GetPaymentService(IEventHubClient eventHubClient = null, List<IPaymentPause> records = null, IPaymentPauseRepository paymentRepository = null)
        {
            var loanInfo = GetLoanInfo("123");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);
            var utcTenantTime = new UtcTenantTime();
            return new PaymentService
                    (
                        new PaymentConfiguration(),
                        PaymentStrategyFactory.Object,
                        ReconciliationStrategyFactory.Object,
                        Repository.Object,
                        LoanService.Object,
                        utcTenantTime,
                        paymentRepository ?? new FakePaymentPauseRepository(new FakeTenantService(), utcTenantTime,
                        records ?? new List<IPaymentPause>()),
                        eventHubClient ?? new FakeEventHub(),
                        Mock.Of<ILogger>()
                    );
        }

    }
}