﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Fees;
using LendFoundry.Loans.Payment.Check;
using LendFoundry.Security.Tokens;
using Moq;
using Xunit;

namespace LendFoundry.Loans.Payment.Service.Test
{
	public class InstallmentAndExtraPaymentStrategyTests : InMemoryObjects
	{
		private Mock<IPaymentRepository> repository { get; } = new Mock<IPaymentRepository>();
		private Mock<IEventHubClientFactory> eventHubFactory { get; } = new Mock<IEventHubClientFactory>();
		private Mock<ITenantTime> tenantTime { get; } = new Mock<ITenantTime>();
		private Mock<ICalendarService> calendar { get; }  = new Mock<ICalendarService>();
		private Mock<IConfigurationServiceFactory<PaymentConfiguration>> configurationFactory { get; }  = new Mock<IConfigurationServiceFactory<PaymentConfiguration>>();
		private Mock<ITokenReader> tokenReader { get; }  = new Mock<ITokenReader>();
		private Mock<ILoanFeeService> feeService { get; } = new Mock<ILoanFeeService>();
		private Mock<ILogger> logger { get; } = new Mock<ILogger>();
		private Mock<ILoanService> loanService { get; }  = new Mock<ILoanService>();

		[Fact(Skip = "it was disabled")]
		public void Multiple_payment_test()
		{
			IPaymentStrategy payment = new InstallmentAndExtraPaymentStrategy(
				repository.Object, 
				eventHubFactory.Object, 
				tenantTime.Object, 
				calendar.Object, 
				configurationFactory.Object, 
				tokenReader.Object, 
				feeService.Object,
				logger.Object, 
				loanService.Object);

			IPaymentRequest request = new CheckPaymentRequest
			{
				LoanReferenceNumber = "001",
				RoutingNumber = "002",
				Type = PaymentType.Scheduled,
				CheckAmount = 1
			};

			var loanInfo = GetLoanInfo("loan001");
			var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);

			loanService.Setup(a => a.GetLoanInfo(request.LoanReferenceNumber)).Returns(loanAdapter);
			var eventHubClient = new Mock<IEventHubClient>();
			eventHubFactory.Setup(a => a.Create(It.IsAny<ITokenReader>())).Returns(eventHubClient.Object);

			var paymentConfiguration = new PaymentConfiguration
			{
				CheckProcessingFeeCode = "103"
			};
			configurationFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get()).Returns(paymentConfiguration);
			
			payment.AddPayment(request);
		}
	}
}