﻿using LendFoundry.Calendar.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Security.Tokens;
using Moq;
using System.Linq;
using System;
using Xunit;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Loans.Payment.Service.Test.Check
{
    public class FeePaymentStrategyTests : InMemoryObjects
    {
        private IPaymentService Service => new PaymentService
            (
                new PaymentConfiguration(),
                PaymentStrategyFactory,
                ReconciliationStrategyFactory.Object,
                Repository.Object,
                LoanService.Object,
                TenantTime.Object,
                PaymentPauseRepository.Object,
                EventHub.Object,
                Mock.Of<ILogger>()
            );

        [Fact]
        public void AddPayment_When_ExecutionOk()
        {
            // arrange
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((p, i) =>
            {
                i++;
                p.LoanReferenceNumber = $"loan00{i}";
                p.RoutingNumber = "001";
                p.CheckNumber = "1234";
                p.CheckAmount = 15.00;  // TODO: Question to MMatthew
                p.FeeAmount = 5.00; // Fee 
                p.DueDate = new DateTimeOffset(new DateTime(2016, 02, 15));
                p.Type = PaymentType.Fee;
                return p;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
            TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

            Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Mock.Of<IDateInfo>());

            var tenantConfiguration = new TenantConfiguration
            {
                PaymentTimeLimit = "15",
                Timezone = null
            };

            ConfigurationFactory.Setup(
                x => x.Create<TenantConfiguration>(It.IsAny<string>(), It.IsAny<ITokenReader>()).Get())
                .Returns(tenantConfiguration);

            EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            // act 
            var checkPayment = Service.AddPayment(checkPaymentRequest).FirstOrDefault();

            // assert
            if (checkPayment == null)
                return;

            Assert.NotNull(checkPayment);
            Assert.Equal(checkPaymentRequest.LoanReferenceNumber, checkPayment.LoanReferenceNumber);

            Assert.False(string.IsNullOrWhiteSpace(checkPayment.Code));
            Assert.False(checkPayment.Type != PaymentType.Fee);

            Assert.False(string.IsNullOrWhiteSpace(checkPaymentRequest.RoutingNumber));
        }

        [Fact]
        public void AddPayment_When_HasNo_FeeCode()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((p, i) =>
                {
                    i++;
                    p.LoanReferenceNumber = $"loan00{i}";
                    p.RoutingNumber = "001";
                    p.CheckNumber = "1234";
                    p.FeeAmount = 6.87;
                    p.DueDate = new DateTimeOffset(new DateTime(2016, 02, 15));
                    p.Type = PaymentType.Fee;
                    // Remove the Fee Code
                    p.Code = string.Empty;
                    return p;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                // act 
                Service.AddPayment(checkPaymentRequest);
            });
        }

        [Fact]
        public void AddPayment_When_FeeAmount_IsGreatherThan_CheckAmount()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((p, i) =>
                {
                    i++;
                    p.LoanReferenceNumber = $"loan00{i}";
                    p.RoutingNumber = "001";
                    p.CheckNumber = "1234";
                    p.CheckAmount = 0.00;
                    p.FeeAmount = 6.87;
                    p.DueDate = new DateTimeOffset(new DateTime(2016, 02, 15));
                    p.Type = PaymentType.Fee;
                    return p;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                // act 
                Service.AddPayment(checkPaymentRequest);
            });
        }
    }
}