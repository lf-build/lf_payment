﻿using LendFoundry.Loans.Payment.Events;
using LendFoundry.Security.Tokens;
using Moq;
using System.Linq;
using System;
using Xunit;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Loans.Payment.Service.Test.Check
{
    public class InstallmentAndExtraPaymentStrategyTests : InMemoryObjects
    {
        private IPaymentService Service
        {
            get
            {
                return new PaymentService
                    (
                        new PaymentConfiguration(),
                        PaymentStrategyFactory,
                        ReconciliationStrategyFactory.Object,
                        Repository.Object,
                        LoanService.Object,
                        TenantTime.Object,
                        PaymentPauseRepository.Object,
                        EventHub.Object,
                        Mock.Of<ILogger>()
                    );
            }
        }

        [Fact]
        public void AddPayment_When_ExecutionOk()
        {
            // arrange
            var fee = 5.00;
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((p, i) =>
            {
                i++;
                p.LoanReferenceNumber = $"loan00{i}";
                p.RoutingNumber = "001";
                p.CheckNumber = "1234";
                p.CheckAmount = 679.72 + fee; // 679.72 + 5.00 // (CurrentDue + Fee)
                p.FeeAmount = fee;
                p.DueDate = new DateTimeOffset(new DateTime(2016, 02, 15));
                p.Type = PaymentType.Scheduled;
                return p;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
            TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

            Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            LoanService.Setup(x => x.GetPaymentSchedule(It.IsAny<string>())).Returns(InMemoryInstallments);

            EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

            var paymentConfiguration = new PaymentConfiguration
            {
                CheckProcessingFeeCode = "103"
            };
            PaymentConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get()).Returns(paymentConfiguration);

            // act 
            var payments = Service.AddPayment(checkPaymentRequest).ToList();
            var checkPayment = payments.FirstOrDefault();

            // assert
            Assert.NotNull(checkPayment);
            if (checkPayment == null)
                return;

            Assert.Equal(checkPaymentRequest.LoanReferenceNumber, checkPayment.LoanReferenceNumber);

            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);

            LoanService.Verify(x => x.GetLoanInfo(It.IsAny<string>()), Times.AtLeast(1));
            LoanService.Verify(x => x.GetPaymentSchedule(It.IsAny<string>()), Times.AtLeast(1));

            Assert.True(payments.Any(p => p.Type == PaymentType.Fee));
            Assert.True(payments.Any(p => p.Type == PaymentType.Scheduled));
            //Assert.True(payments.Any(p => p.Type == PaymentType.Extra));
        }

        [Fact]
        public void AddPayment_When_FeeAmount_IsGreather_CheckAmount()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((p, i) =>
                {
                    i++;
                    p.LoanReferenceNumber = $"loan00{i}";
                    p.CheckNumber = "2000";
                    p.FeeAmount = 2000;
                    p.CheckAmount = 1000;
                    p.Type = PaymentType.Scheduled;
                    return p;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                // act 
                Service.AddPayment(checkPaymentRequest);

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
            });
        }

        [Fact]
        public void AddPayment_When_NotFound_Loan()
        {
            Assert.Throws<LoanNotFoundException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((p, i) =>
                {
                    i++;
                    p.LoanReferenceNumber = $"loan00{i}";
                    p.CheckNumber = "2000";
                    p.CheckAmount = 2.00;
                    p.FeeAmount = 1.00;
                    p.Type = PaymentType.Extra;
                    return p;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                var paymentConfiguration = new PaymentConfiguration
                {
                    CheckProcessingFeeCode = "103"
                };
                PaymentConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get()).Returns(paymentConfiguration);

                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>()));

                // act 
                Service.AddPayment(checkPaymentRequest);

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Once);
            });
        }

        [Fact]
        public void AddPayment_When_ExtraPaymentAmount_IsGreatherThan_PrincipalBalance()
        {
            Assert.Throws<InvalidArgumentException>(() =>
            {
                // arrange
                var list = new[] {InMemoryCheckPaymentRequest}.AsEnumerable();
                list = list.Select((p, i) =>
                {
                    i++;
                    p.LoanReferenceNumber = $"loan00{i}";
                    p.RoutingNumber = "001";
                    p.CheckNumber = "1234";
                    p.CheckAmount = 2000.00; //679.72; // CurrentDue
                    p.FeeAmount = 6.87;
                    p.DueDate = new DateTimeOffset(new DateTime(2016, 02, 15));
                    p.Type = PaymentType.Scheduled;
                    return p;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

                Logger.Setup(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()));

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);
                LoanService.Setup(x => x.GetPaymentSchedule(It.IsAny<string>())).Returns(InMemoryInstallments);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var paymentConfiguration = new PaymentConfiguration
                {
                    CheckProcessingFeeCode = "103"
                };
                PaymentConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get())
                    .Returns(paymentConfiguration);

                // act 
                Service.AddPayment(checkPaymentRequest);
                

                // assert
                Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.AtLeast(1));

                LoanService.Verify(x => x.GetLoanInfo(It.IsAny<string>()), Times.AtLeast(1));
                LoanService.Verify(x => x.GetPaymentSchedule(It.IsAny<string>()), Times.AtLeast(1));
            });
        }
    }
}