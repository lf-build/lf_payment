﻿using LendFoundry.Calendar.Client;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Security.Tokens;
using Moq;
using System.Linq;
using System;
using LendFoundry.EventHub.Client;
using Xunit;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Loans.Payment.Service.Test.Check
{
    public class PayoffPaymentStrategyTests : InMemoryObjects
    {
        private IPaymentService Service => new PaymentService
            (
                new PaymentConfiguration(),
                PaymentStrategyFactory,
                ReconciliationStrategyFactory.Object,
                Repository.Object,
                LoanService.Object,
                TenantTime.Object,
                PaymentPauseRepository.Object,
                EventHub.Object,
                Mock.Of<ILogger>()
            );

        [Fact]
        public void AddPayment_When_ExecutionOk()
        {
            // arrange
            var fee = 5.00;
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((p, i) =>
            {
                i++;
                p.LoanReferenceNumber = $"loan00{i}";
                p.RoutingNumber = "001";
                p.CheckNumber = "1234";
                p.CheckAmount = 1006.87 + fee; // PayoffAmount ( SUM(InstallmentsAmount) + FeeAmount ) + FeeCheckAmount
                p.FeeAmount = fee;
                p.DueDate = new DateTimeOffset(new DateTime(2016, 02, 15));
                p.Type = PaymentType.Payoff;
                return p;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
            TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

            Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Mock.Of<IDateInfo>());

            var paymentConfiguration = new PaymentConfiguration
            {
                CheckProcessingFeeCode = "103"
            };
            PaymentConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get()).Returns(paymentConfiguration);

            EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

            var payoffAmount = new PayoffAmount
            {
                Amount = 1006.87,
                ValidUntil = new DateTimeOffset(DateTime.Now.AddDays(10))
            };
            LoanService.Setup(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()))
                .Returns(payoffAmount);

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            // act 
            var checkPayment = Service.AddPayment(checkPaymentRequest).FirstOrDefault();

            // assert
            if (checkPayment == null)
                return;

            Assert.NotNull(checkPayment);
            Assert.Equal(checkPaymentRequest.LoanReferenceNumber, checkPayment.LoanReferenceNumber);

            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            LoanService.Verify(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()), Times.Once);
        }

        [Fact]
        public void AddPayment_When_FeeAmount_IsGreather_CheckAmount()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((p, i) =>
                {
                    i++;
                    p.LoanReferenceNumber = $"loan00{i}";
                    p.CheckNumber = "2000";
                    p.FeeAmount = 2000;
                    p.CheckAmount = 1000;
                    p.Type = PaymentType.Payoff;
                    return p;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

                Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Mock.Of<IDateInfo>());

                var paymentConfiguration = new PaymentConfiguration
                {
                    CheckProcessingFeeCode = "103"
                };
                PaymentConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get()).Returns(paymentConfiguration);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var payoffAmount = new PayoffAmount
                {
                    Amount = 100.00,
                    ValidUntil = new DateTimeOffset(DateTime.Now.AddDays(10))
                };

                LoanService.Setup(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()))
                    .Returns(payoffAmount);

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                // act 
                Service.AddPayment(checkPaymentRequest);

            });
        }

        [Fact]
        public void AddPayment_When_HasIncorrect_PayoffAmount()
        {
            Assert.Throws<ArgumentException>(() =>
            {
                // arrange
                var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
                list = list.Select((s, i) =>
                {
                    i++;
                    s.LoanReferenceNumber = $"loan00{i}";
                    s.CheckNumber = "2000";
                    s.FeeAmount = 300.00;
                    s.CheckAmount = 200.00;
                    s.Type = PaymentType.Payoff;
                    return s;
                }).ToArray();

                var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

                TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
                TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

                Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                    .Returns(Mock.Of<IDateInfo>());

                var paymentConfiguration = new PaymentConfiguration
                {
                    CheckProcessingFeeCode = "103"
                };
                PaymentConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get()).Returns(paymentConfiguration);

                EventHubFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Publish(It.IsAny<PaymentAdded>()));

                var payoffAmount = new PayoffAmount
                {
                    Amount = 0,
                    ValidUntil = new DateTimeOffset(DateTime.Now.AddDays(10))
                };

                LoanService.Setup(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()))
                    .Returns(payoffAmount);

                var loanInfo = GetLoanInfo("loan001");
                var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
                LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

                // act 
                Service.AddPayment(checkPaymentRequest);

            });
        }


        [Fact]
        public void AddPayment_When_RefundRequired()
        {
            // arrange
            var fee = 5.00;
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((p, i) =>
            {
                i++;
                p.LoanReferenceNumber = $"loan00{i}";
                p.RoutingNumber = "001";
                p.CheckNumber = "1234";
                p.CheckAmount = 1506.37 + fee; // actual 1006.87 // PayoffAmount ( SUM(InstallmentsAmount) + FeeAmount ) + FeeCheckAmount
                p.FeeAmount = fee;
                p.DueDate = new DateTimeOffset(new DateTime(2016, 02, 15));
                p.Type = PaymentType.Payoff;
                return p;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
            TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

            Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Mock.Of<IDateInfo>());

            var paymentConfiguration = new PaymentConfiguration
            {
                CheckProcessingFeeCode = "103"
            };
            PaymentConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get()).Returns(paymentConfiguration);

            var eventHub = new Mock<IEventHubClient>();
            EventHubFactory.Setup(a => a.Create(It.IsAny<ITokenReader>())).Returns(eventHub.Object);

            var payoffAmount = new PayoffAmount
            {
                Amount = 1006.87,
                ValidUntil = new DateTimeOffset(DateTime.Now.AddDays(10))
            };
            LoanService.Setup(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()))
                .Returns(payoffAmount);

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            // act 
            var checkPayment = Service.AddPayment(checkPaymentRequest).FirstOrDefault();

            // assert
            if (checkPayment == null)
                return;

            Assert.NotNull(checkPayment);
            Assert.Equal(checkPaymentRequest.LoanReferenceNumber, checkPayment.LoanReferenceNumber);

            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            LoanService.Verify(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()), Times.Once);
            eventHub.Verify(e=>e.Publish(It.IsAny<PaymentToBeRefunded>()));
        }

        [Fact]
        public void AddPayment_When_RefundRequired_Not_Required()
        {
            // arrange
            var fee = 5.00;
            var list = new[] { InMemoryCheckPaymentRequest }.AsEnumerable();
            list = list.Select((p, i) =>
            {
                i++;
                p.LoanReferenceNumber = $"loan00{i}";
                p.RoutingNumber = "001";
                p.CheckNumber = "1234";
                p.CheckAmount = 1006.87 + fee; // actual 1006.87 // PayoffAmount ( SUM(InstallmentsAmount) + FeeAmount ) + FeeCheckAmount
                p.FeeAmount = fee;
                p.DueDate = new DateTimeOffset(new DateTime(2016, 02, 15));
                p.Type = PaymentType.Payoff;
                return p;
            }).ToArray();

            var checkPaymentRequest = list.FirstOrDefault() ?? InMemoryCheckPaymentRequest;

            TenantTime.Setup(x => x.Today).Returns(new DateTimeOffset(DateTime.Now));
            TenantTime.Setup(x => x.FromDate(It.IsAny<DateTime>())).Returns(checkPaymentRequest.DueDate);

            Calendar.Setup(x => x.GetDate(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<int>()))
                .Returns(Mock.Of<IDateInfo>());

            var paymentConfiguration = new PaymentConfiguration
            {
                CheckProcessingFeeCode = "103"
            };
            PaymentConfigurationServiceFactory.Setup(x => x.Create(It.IsAny<ITokenReader>()).Get()).Returns(paymentConfiguration);

            var eventHub = new Mock<IEventHubClient>();
            EventHubFactory.Setup(a => a.Create(It.IsAny<ITokenReader>())).Returns(eventHub.Object);

            var payoffAmount = new PayoffAmount
            {
                Amount = 1006.87,
                ValidUntil = new DateTimeOffset(DateTime.Now.AddDays(10))
            };
            LoanService.Setup(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()))
                .Returns(payoffAmount);

            var loanInfo = GetLoanInfo("loan001");
            var loanAdapter = new InMemoryLoanBackupAdapter(loanInfo);
            LoanService.Setup(x => x.GetLoanInfo(It.IsAny<string>())).Returns(loanAdapter);

            // act 
            var checkPayment = Service.AddPayment(checkPaymentRequest).FirstOrDefault();

            // assert
            if (checkPayment == null)
                return;

            Assert.NotNull(checkPayment);
            Assert.Equal(checkPaymentRequest.LoanReferenceNumber, checkPayment.LoanReferenceNumber);

            Logger.Verify(x => x.Error(It.IsAny<string>(), It.IsAny<Exception>()), Times.Never);
            LoanService.Verify(x => x.GetPayoffAmount(It.IsAny<string>(), It.IsAny<DateTime>()), Times.Once);
            eventHub.Verify(e => e.Publish(It.IsAny<PaymentToBeRefunded>()), Times.Never);
        }
    }
}