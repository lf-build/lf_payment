﻿using System;
using System.Collections.Generic;
using LendFoundry.Foundation.Services;
using Moq;
using RestSharp;
using Xunit;

namespace LendFoundry.Loans.Payment.Client.Test
{
    public class PaymentServiceClientTest
    {

        [Fact]
        public void Client_Pause()
        {
            IRestRequest request = null;
            var mockServiceClient = new Mock<IServiceClient>();
            mockServiceClient.Setup(s => s.ExecuteAsync<PaymentPause>(It.IsAny<IRestRequest>()))
                 .Callback<IRestRequest>(r => request = r)
                 .ReturnsAsync(new PaymentPause());

            var paymentServiceClient = new PaymentServiceClient(mockServiceClient.Object);
            
            var result = paymentServiceClient.Pause("123", new PaymentPauseRequest()
            {
                StartDate = DateTime.Now,
                EndDate = DateTime.Now.AddDays(10)
            });

            Assert.NotNull(result);
            Assert.NotNull(request);
            Assert.Equal("ach/{loanReferenceNumber}/pause", request.Resource);
            Assert.Equal(Method.POST, request.Method);
        }

        [Fact]
        public void Client_Resume()
        {
            IRestRequest request = null;
            var mockServiceClient = new Mock<IServiceClient>();
            mockServiceClient.Setup(s => s.ExecuteAsync(It.IsAny<IRestRequest>()))
                 .Callback<IRestRequest>(r => request = r);

            var paymentServiceClient = new PaymentServiceClient(mockServiceClient.Object);

            var result = paymentServiceClient.Resume("123");

            Assert.NotNull(result);
            Assert.NotNull(request);
            Assert.Equal("ach/{loanReferenceNumber}/pause", request.Resource);
            Assert.Equal(Method.DELETE, request.Method);
        }

        [Fact]
        public void Client_GetPauses()
        {
            IRestRequest request = null;
            var mockServiceClient = new Mock<IServiceClient>();
            mockServiceClient.Setup(s => s.ExecuteAsync<List<PaymentPause>>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r).ReturnsAsync(new List<PaymentPause>());

            var paymentServiceClient = new PaymentServiceClient(mockServiceClient.Object);

            var result = paymentServiceClient.GetPauses();

            Assert.NotNull(result);
            Assert.NotNull(request);
            Assert.Equal("ach/pauses", request.Resource);
            Assert.Equal(Method.GET, request.Method);
        }

        [Fact]
        public void Client_GetPause_By_LoanReferenceNumber()
        {
            IRestRequest request = null;
            var mockServiceClient = new Mock<IServiceClient>();
            mockServiceClient.Setup(s => s.ExecuteAsync<PaymentPause>(It.IsAny<IRestRequest>()))
                .Callback<IRestRequest>(r => request = r).ReturnsAsync(new PaymentPause());

            var paymentServiceClient = new PaymentServiceClient(mockServiceClient.Object);

            var result = paymentServiceClient.GetPauseByLoanReferenceNumber("123");

            Assert.NotNull(result);
            Assert.NotNull(request);
            Assert.Equal("ach/{loanReferenceNumber}/pause", request.Resource);
            Assert.Equal(Method.GET, request.Method);
        }
    }
}
