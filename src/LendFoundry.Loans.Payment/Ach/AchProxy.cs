using LendFoundry.MoneyMovement.Ach;
using LendFoundry.MoneyMovement.Ach.Client;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Payment.Ach
{
    public class AchProxy : IAchProxy
    {
        public AchProxy(IAchService achService, IPaymentConfiguration configuration)
        {
            AchService = achService;
            Configuration = configuration;
            EntryDescriptions = new PredicatedValues<IAchPayment, string>(
                new PredicatedValue<IAchPayment, string>(Configuration.RetryEntryDescription, pmt => pmt.Attempts > 1),
                new PredicatedValue<IAchPayment, string>(Configuration.LateFeeEntryDescription, pmt => pmt.Type == PaymentType.Fee && pmt.Code == Configuration.LateFeeCode),
                new PredicatedValue<IAchPayment, string>(Configuration.ReturnFeeEntryDescription, pmt => pmt.Type == PaymentType.Fee && pmt.Code == Configuration.ReturnFeeCode)
            );
            InvestorIds = new PredicatedValues<IAchPayment, string>(
                new PredicatedValue<IAchPayment, string>(Configuration.LateFeeInvestorId, pmt => pmt.Type == PaymentType.Fee && pmt.Code == Configuration.LateFeeCode),
                new PredicatedValue<IAchPayment, string>(Configuration.ReturnFeeInvestorId, pmt => pmt.Type == PaymentType.Fee && pmt.Code == Configuration.ReturnFeeCode),
                PredicatedValue<IAchPayment, string>.Default<IAchPayment, string>(pmt => pmt.InvestorId)
            );
        }

        private IAchService AchService { get; }
        private IPaymentConfiguration Configuration { get; }
        private PredicatedValues<IAchPayment, string> EntryDescriptions { get; }
        private PredicatedValues<IAchPayment, string> InvestorIds { get; }

        public string Queue(IAchPayment payment)
        {
            return Queue(payment, payment.Amount, payment.DueDate.Date);
        }

        public string Queue(IAchPayment payment, DateTimeOffset executionDate)
        {
            return Queue(payment, payment.Amount, executionDate.Date);
        }

        public string Queue(List<IAchPayment> payments)
        {
            if (payments == null || !payments.Any())
                throw new ArgumentNullException(nameof(payments));

            return Queue(payments, payments.First().DueDate);
        }

        public string Queue(List<IAchPayment> payments, DateTimeOffset executionDate)
        {
            if (payments == null || !payments.Any())
                throw new ArgumentNullException(nameof(payments));

            return Queue(payments.First(), payments.Sum(p => p.Amount), executionDate.Date);
        }

        public void Dequeue(string instructionId)
        {
            AchService.Dequeue(instructionId);
        }

        private string Queue(IAchPayment payment, double amount, DateTime executionDate)
        {
            var instruction = AchService.Queue(CreateInstruction(payment, amount, executionDate));

            return instruction.Id;
        }

        private IInstructionRequest CreateInstruction(IAchPayment payment, double amount, DateTime executionDate)
        {
            var instruction = new InstructionRequest
            {
                FundingSourceId = InvestorIds.ValueFor(payment),
                ReferenceNumber = payment.LoanReferenceNumber,
                ExecutionDate = executionDate,
                EntryDescription = EntryDescriptions.ValueFor(payment),
                Receiving = new Receiving
                {
                    AccountNumber = payment.AccountNumber,
                    AccountType = AccountType.Individual,
                    Name = payment.AccountHolder,
                    RoutingNumber = payment.RoutingNumber
                },
                Amount = Convert.ToDecimal(amount),
                StandardEntryClassCode = "WEB",
                Frequency = payment.Type == PaymentType.Scheduled ? Frequency.Recurring : Frequency.Single,
                Type = payment.AccountType == BankAccountType.Checking ? TransactionType.DebitFromChecking : TransactionType.DebitFromSavings
            };
            return instruction;
        }
    }
}