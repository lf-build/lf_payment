using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Payment.Ach
{
    public interface IAchProxy
    {
        string Queue(IAchPayment payment);

        string Queue(IAchPayment payment, DateTimeOffset executionDate);

        string Queue(List<IAchPayment> payments);

        string Queue(List<IAchPayment> payments, DateTimeOffset executionDate);

        void Dequeue(string instructionId);
    }
}