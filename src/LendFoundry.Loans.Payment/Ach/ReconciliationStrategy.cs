using System;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Foundation.Logging;
using System.Linq;

namespace LendFoundry.Loans.Payment.Ach
{
    internal class ReconciliationStrategy : IReconciliationStrategy
    {
        public ReconciliationStrategy(IPaymentRepository repository, IEventHubClient eventHub, ILogger logger)
        {
            Repository = repository;
            EventHub = eventHub;
            Logger = logger;
        }

        private IPaymentRepository Repository { get; }
        private IEventHubClient EventHub { get; }
        private ILogger Logger { get; }

        public void ReconcilePendingPayments(DateTimeOffset dueDate)
        {
            Logger.Info($"Reconciling ACH payments due on {dueDate.ToString("yyyy-MM-dd")}");

            Expression<Func<IPayment, bool>> query = p => 
                p.Method == PaymentMethod.ACH && 
                p.Status == PaymentStatus.Pending && 
                p.DueDate == dueDate;

            var payments = WaitFor(Repository.MarkAsReceived(query));

            Logger.Info($"{payments.Count()} ACH payment(s) due on {dueDate.ToString("yyyy-MM-dd")} have been reconciled");

            foreach (var payment in payments)
                PaymentReceived(payment);
        }

        protected void PaymentReceived(IPayment payment)
        {
            var @event = new PaymentReceived(payment);
            WaitFor(EventHub.Publish(@event));
            Logger.Info($"Event `{@event.GetType().Name}` published: LoanReferenceNumber={{LoanReferenceNumber}} PaymentId={{Id}}", new
            {
                payment.Id,
                payment.LoanReferenceNumber,
                payment.DueDate,
                payment.Amount,
                payment.Type,
                payment.Method,
                payment.Code
            });
        }

        protected static T WaitFor<T>(Task<T> task)
        {
            task.Wait();

            if (task.IsFaulted)
                throw new Exception("Error during async task execution", task.Exception);

            return task.Result;
        }
    }
}