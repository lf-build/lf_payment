using LendFoundry.Configuration.Client;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Payment.Ach
{
    public class AchProxyFactory : IAchProxyFactory
    {
        public AchProxyFactory(IAchServiceFactory achServiceFactory, IConfigurationServiceFactory<PaymentConfiguration> configurationServiceFactory)
        {
            AchServiceFactory = achServiceFactory;
            ConfigurationServiceFactory = configurationServiceFactory;
        }

        private IAchServiceFactory AchServiceFactory { get; }
        private IConfigurationServiceFactory<PaymentConfiguration> ConfigurationServiceFactory { get; }

        public IAchProxy Create(ITokenReader reader)
        {
            var achService = AchServiceFactory.Create(reader);
            var configurationService = ConfigurationServiceFactory.Create(reader);
            return new AchProxy(achService, configurationService.Get());
        }
    }
}