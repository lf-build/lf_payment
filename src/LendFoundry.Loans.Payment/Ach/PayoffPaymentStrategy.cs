﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Payment.Ach
{
    internal class PayoffPaymentStrategy : AchPaymentStrategyBase, IPaymentStrategy
    {
        public PayoffPaymentStrategy(
            IPaymentRepository repository,
            IAchProxyFactory achProxyFactory,
            IEventHubClientFactory eventHubFactory,
            ITenantTime tenantTime,
            ICalendarService calendar,
            IConfigurationServiceFactory configurationFactory,
            ITokenReader tokenReader,
            ILoanService loanService,
            ILogger logger
        ) : base(repository, achProxyFactory, eventHubFactory, tenantTime, calendar, configurationFactory, tokenReader, logger)
        {
            LoanService = loanService;
        }

        private ILoanService LoanService { get; }

        public IEnumerable<IPayment> AddPayment(IPaymentRequest request)
        {
            try
            {
                ValidateRequest(request);

                if (request.Type != PaymentType.Payoff)
                    throw new ArgumentException("The payment type should be 'Payoff'.");

                var expectedPayoffAmount = LoanService.GetPayoffAmount(request.LoanReferenceNumber, request.DueDate.Date);

                if (!DoesPayoffAmountMatch(request, expectedPayoffAmount))
                    throw new InvalidArgumentException($"Payoff amount doesn't match the expected amount of {expectedPayoffAmount.Amount} for date {request.DueDate}");

                var loan = LoanService.GetLoanInfo(request.LoanReferenceNumber);
                if (loan == null)
                    throw new LoanNotFoundException(request.LoanReferenceNumber);

                return new[] { QueueAndSave(CreatePayment(request, loan.Loan.Investor)) };
            }
            catch (Exception ex)
            {
                Logger.Error($"Unhandled exception while add payment using PaymentMethod:#{request.Method}:\n", ex);
                throw;
            }
        }

        private static bool DoesPayoffAmountMatch(IPaymentRequest request, IPayoffAmount payoff)
        {
            return Math.Abs(payoff.Amount - request.Amount) < double.Epsilon;
        }

        public IEnumerable<IPayment> RetryPayment(IPayment payment, DateTimeOffset dueDate, BankAccount bankAccount)
        {
            throw new ArgumentException("Payoff payments cannot be retried");
        }
    }
}
