using LendFoundry.MoneyMovement.Ach;

namespace LendFoundry.Loans.Payment.Ach.Events
{
    public class AchInstructionRejected
    {
        public string ReferenceNumber { get; set; }

        public string Reason { get; set; }

        public string Description { get; set; }

        public Instruction Instruction { get; set; }
    }
}