using LendFoundry.MoneyMovement.Ach;

namespace LendFoundry.Loans.Payment.Ach.Events
{
    public class AchPaymentRejection : PaymentRejection
    {
        public AchPaymentRejection(Instruction instruction)
        {
            Instruction = instruction;
        }

        public Instruction Instruction { get; set; }
    }
}