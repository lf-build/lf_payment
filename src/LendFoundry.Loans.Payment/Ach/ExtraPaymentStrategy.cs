using System;
using System.Collections.Generic;
using System.Linq;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Fees;
using LendFoundry.Security.Tokens;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;

namespace LendFoundry.Loans.Payment.Ach
{
    internal class ExtraPaymentStrategy : AchPaymentStrategyBase, IPaymentStrategy
    {
        public ExtraPaymentStrategy(
            IPaymentRepository repository,
            IAchProxyFactory achProxyFactory,
            IEventHubClientFactory eventHubFactory,
            ITenantTime tenantTime,
            ICalendarService calendar,
            IConfigurationServiceFactory configurationFactory,
            ITokenReader tokenReader,
            ILoanFeeService feeService,
            ILoanService loanService,
            ILogger logger
        ) : base(repository, achProxyFactory, eventHubFactory, tenantTime, calendar, configurationFactory, tokenReader, logger)
        {
            FeeService = feeService;
            LoanService = loanService;
        }

        private ILoanFeeService FeeService { get; }

        private ILoanService LoanService { get; }

        public IEnumerable<IPayment> AddPayment(IPaymentRequest request)
        {
            try
            {
                ValidateRequest(request);

                var loanInfo = LoanService.GetLoanInfo(request.LoanReferenceNumber);
                if (loanInfo == null)
                    throw new LoanNotFoundException(request.LoanReferenceNumber);

                AssertPaymentAmountIsLessThanPrincipalBalance(request, loanInfo);

                var schedule = LoanService.GetPaymentSchedule(request.LoanReferenceNumber);
                if (schedule.Any(i => i.DueDate == request.DueDate))
                    throw new PaymentAlreadyExistException($"Installment already scheduled on {request.DueDate.Date.ToString("MMM dd, yyyy")}.");

                var feePayments = new List<IAchPayment>();
                var remainingFees = new List<Fees.IFee>();

                Fees.IFee fee;
                while ((fee = FeeService.Dequeue(request.LoanReferenceNumber)) != null)
                {
                    var totalAmount = feePayments.Sum(p => p.Amount) + fee.Amount;
                    if (totalAmount >= request.Amount)
                    {
                        var difference = totalAmount - request.Amount;
                        if (difference > 0)
                        {
                            remainingFees.Add(new Fees.Fee
                            {
                                LoanReferenceNumber = fee.LoanReferenceNumber,
                                Code = fee.Code,
                                Name = fee.Name,
                                Amount = difference,
                                Type = FeeType.Fixed
                            });
                        }
                        feePayments.Add(CreateFeePayment(request, fee.Amount - difference, fee.Code, loanInfo.Loan.Investor));
                        break;
                    }
                    feePayments.Add(CreateFeePayment(request, fee.Amount, fee.Code, loanInfo.Loan.Investor));
                }

                //TODO enable transaction control, if achProxyFactory queue fails, fees need to be queued/restored
                if (feePayments.Any()) feePayments.ForEach(pmt => QueueAndSave(pmt));
                if (remainingFees.Any()) FeeService.Add(remainingFees);

                var balance = request.Amount - feePayments.Sum(p => p.Amount);
                return balance > 0 ? new[] { QueueAndSave(CreatePayment(request, balance, loanInfo.Loan.Investor)) } : null;
            }
            catch (Exception ex)
            {
                Logger.Error($"Unhandled exception while add payment using PaymentMethod:#{request.Method}:\n", ex);
                throw;
            }
        }

        private void AssertPaymentAmountIsLessThanPrincipalBalance(IPaymentRequest request, ILoanInfo loanInfo)
        {
            if (request.Type == PaymentType.Extra)
            {
                var installments = LoanService.GetPaymentSchedule(request.LoanReferenceNumber);
                if (installments == null)
                    throw new PaymentScheduleNotFoundException(request.LoanReferenceNumber);

                var precedingInstallment = installments.LastOrDefault(i => (i.PaymentDate ?? i.DueDate) <= request.DueDate);
                var principalBalance = precedingInstallment?.EndingBalance ?? loanInfo.Loan.Terms.LoanAmount;

                if (request.Amount >= principalBalance)
                    throw new InvalidArgumentException("Payment amount is greater than the principal balance at the given date");
            }
        }

        protected new void AssertPaymentCanBeCancelled(IAchPayment payment)
        {
            base.AssertPaymentCanBeCancelled(payment);

            var payments = Repository.All<Payment>(p => p.LoanReferenceNumber == payment.LoanReferenceNumber && p.Type == PaymentType.Payoff && (p.Status == PaymentStatus.Pending || p.Status == PaymentStatus.Received));

            if (payments.ToList().Any())
                throw new InvalidArgumentException("Payoff is scheduled for this loan. Please cancel payoff and attempt again.");
        }

        public IEnumerable<IPayment> RetryPayment(IPayment payment, DateTimeOffset dueDate, BankAccount bankAccount)
        {
            throw new ArgumentException("Extra payments cannot be retried");
        }
    }
}