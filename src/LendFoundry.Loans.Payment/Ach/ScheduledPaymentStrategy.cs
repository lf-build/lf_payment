using System;
using System.Collections.Generic;
using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Loans.Fees;
using LendFoundry.Security.Tokens;
using System.Linq;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;

namespace LendFoundry.Loans.Payment.Ach
{
    internal class ScheduledPaymentStrategy : AchPaymentStrategyBase, IPaymentStrategy
    {
        public ScheduledPaymentStrategy
        (
            IPaymentRepository repository,
            IAchProxyFactory achProxyFactory,
            IEventHubClientFactory eventHubFactory,
            ITenantTime tenantTime,
            ICalendarService calendar,
            IConfigurationServiceFactory configurationFactory,
            ITokenReader tokenReader,
            ILoanFeeService feeService,
            ILoanService loanService,
            ILogger logger
        ) : base(repository, achProxyFactory, eventHubFactory, tenantTime, calendar, configurationFactory, tokenReader, logger)
        {
            FeeService = feeService;
            LoanService = loanService;
        }

        private ILoanFeeService FeeService { get; }

        private ILoanService LoanService { get; }

        public IEnumerable<IPayment> AddPayment(IPaymentRequest request)
        {
            return AddAchPayment(CreatePayment(request));
        }

        private IEnumerable<IPayment> AddAchPayment(IAchPayment payment)
        {
            var fees = new List<Fees.IFee>();
            try
            {
                ValidateRequest(payment);

                var loanInfo = LoanService.GetLoanInfo(payment.LoanReferenceNumber);
                if (loanInfo == null)
                    throw new LoanNotFoundException(payment.LoanReferenceNumber);

                
                Fees.IFee fee;
                var feePayments = new List<IAchPayment>();
                while ((fee = FeeService.Dequeue(payment.LoanReferenceNumber)) != null)
                {
                    fees.Add(fee);
                    feePayments.Add(CreateFeePayment(payment, fee.Amount, fee.Code, loanInfo.Loan.Investor));
                }

                //TODO enable transaction control, if achProxyFactory queue fails, fees need to be queued/restored  (this implementation is not safe)
            
                if(feePayments.Any()) feePayments.ForEach(pmt => QueueAndSave(pmt));
                return new [] { QueueAndSave(CreatePayment(payment, loanInfo.Loan.Investor)) };
            }
            catch (Exception ex)
            {
                if(fees.Any())
                    FeeService.Add(fees);

                Logger.Error($"Unhandled exception while add payment using PaymentMethod: {payment.Method}:\n", ex);
                throw;
            }
        }

        public IEnumerable<IPayment> RetryPayment(IPayment payment, DateTimeOffset dueDate, BankAccount bankAccount)
        {
            if (payment.Method != PaymentMethod.ACH)
                throw new ArgumentException($"Expected payment method {PaymentMethod.ACH}, got {payment.Method} instead");

            if (payment.Status != PaymentStatus.Failed)
                throw new ArgumentException($"Only failed payments can be retried. This payment's status is {payment.Status}.");

            var retry = CreatePayment(payment);
            retry.Id = null;
            retry.DueDate = dueDate;
            retry.Attempts = payment.Attempts + 1;

            if (bankAccount != null)
            {
                retry.AccountNumber = bankAccount.AccountNumber;
                retry.AccountType = bankAccount.AccountType;
                retry.RoutingNumber = bankAccount.RoutingNumber;
            }

            return AddAchPayment(retry);
        }
    }
}