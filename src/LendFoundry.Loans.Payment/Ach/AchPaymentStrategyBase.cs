using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Security.Tokens;
using System.Collections.Generic;
using System.Linq;
using System;

namespace LendFoundry.Loans.Payment.Ach
{
    internal class AchPaymentStrategyBase : PaymentStrategyBase
    {
        protected AchPaymentStrategyBase
        (
            IPaymentRepository repository,
            IAchProxyFactory achProxyFactory,
            IEventHubClientFactory eventHubFactory,
            ITenantTime tenantTime,
            ICalendarService calendar,
            IConfigurationServiceFactory configurationFactory,
            ITokenReader tokenReader,
            ILogger logger
        ) : base(eventHubFactory, tenantTime)
        {
            Repository = repository;
            AchProxy = achProxyFactory.Create(tokenReader);
            Calendar = calendar;
            ConfigurationFactory = configurationFactory;
            TokenReader = tokenReader;
            Logger = logger;
        }

        protected IPaymentRepository Repository { get; }

        protected IAchProxy AchProxy { get; }

        private ICalendarService Calendar { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        protected ITokenReader TokenReader { get; }

        protected ILogger Logger { get; }

        public void CancelPayment(IPayment payment, string notes)
        {
            try
            {
                Logger.Info($"\n\n=================== [ Start Cancel Payment {payment.Id} ] ===================");

                var achPayment = payment as IAchPayment;

                if (achPayment == null)
                    throw new ArgumentNullException(nameof(payment));

                AssertPaymentCanBeCancelled(achPayment);

                AchProxy.Dequeue(achPayment.InstructionId);
                Logger.Info($"The 'Dequeue' method was executed with payment instructionId:{achPayment.InstructionId}.");

                ChangeStatus(payment, PaymentStatus.Cancelled);
                Repository.Update(payment);
                Logger.Info($"The payment with Status:'{payment.Status}', PaymentId:{payment.Id} was updated.");

                var staticReader = new StaticTokenReader(TokenReader.Read());
                var eventHub = EventHubFactory.Create(staticReader);
                eventHub.Publish(new PaymentCancelled(payment, notes));
                Logger.Info($"The event 'PaymentCancelled' with loan #{payment.LoanReferenceNumber}, Amount:${payment.Amount}, Type:{payment.Type}, DueDate:{payment.DueDate}, PaymentId:{payment.Id}, was published.");
            }
            catch (Exception ex)
            {
                Logger.Error("Unhandled exception while cancel payment: ", ex);
                throw;
            }
            finally
            {
                Logger.Info($"\n=================== [ Finish Cancel Payment {payment.Id} ] ===================\n");
            }
        }

        public void FailPayment(IPayment payment, string failureCode, string failureDescription)
        {
            if (payment == null)
                throw new ArgumentNullException(nameof(payment));

            var newStatus = PaymentStatus.Failed;

            if (payment.Status == newStatus)
                throw new ArgumentException($"Payment {payment.Id} from loan {payment.LoanReferenceNumber} is already marked as failed", nameof(payment));

            var oldStatus = payment.Status;

            Logger.Info("Loan {LoanReferenceNumber}: marking payment {Id} as {NewStatus} (current status is {OldStatus})", new { payment.Id, payment.LoanReferenceNumber, payment.Type, OldStatus = oldStatus, NewStatus = newStatus });

            payment.FailureCode = failureCode;
            payment.FailureDescription = failureDescription;
            ChangeStatus(payment, newStatus);

            Repository.Update(payment);

            Logger.Info("Loan {LoanReferenceNumber}: payment {Id} has been marked as {NewStatus} (previous status was {OldStatus})", new { payment.Id, payment.LoanReferenceNumber, payment.Type, OldStatus = oldStatus, NewStatus = newStatus });

            var staticReader = new StaticTokenReader(TokenReader.Read());
            var eventHub = EventHubFactory.Create(staticReader);
            var @event = new PaymentFailed(payment);
            eventHub.Publish(@event);

            Logger.Info($"Loan {{LoanReferenceNumber}}: event {@event.GetType().Name} has been published for payment {{Id}}", new { payment.Id, payment.LoanReferenceNumber, payment.Type, OldStatus = oldStatus, NewStatus = newStatus });
        }

        protected IAchPayment QueueAndSave(IAchPayment payment)
        {
            try
            {
                Logger.Info($"\n\n=================== [ Start QueueAndSave Payment {payment.Id} ] ===================");

                //TODO enable transaction control, repository fails, achProxyFactory instructions should be dequeued
                payment.InstructionId = AchProxy.Queue(payment, GetPreviousBusinessDay(payment));
                Logger.Info($"The 'Queue' method was executed and returned the instructionId:{payment.InstructionId}.");

                Repository.Add(new[] { payment });
                Logger.Info($"The payment with Status:'{payment.Status}', PaymentId:{payment.Id} was added.");

                var staticReader = new StaticTokenReader(TokenReader.Read());
                var eventHub = EventHubFactory.Create(staticReader);
                eventHub.Publish(new PaymentAdded(payment));
                Logger.Info($"The event 'PaymentAdded' with loan #{payment.LoanReferenceNumber}, Amount:${payment.Amount}, Type:{payment.Type}, DueDate:{payment.DueDate}, PaymentId:{payment.Id}, was published.");

                Logger.Info($"\n=================== [ Finish QueueAndSave Payment {payment.Id} ] ===================\n");

                return payment;
            }
            catch (Exception ex)
            {
                Logger.Error("Unhandled exception while queue and save payment: ", ex);
                throw;
            }
        }

        protected void QueueAndSave(List<IAchPayment> payments)
        {
            try
            {
                if (payments.Any())
                {
                    Logger.Info("\n=================== [ Start QueueAndSave ] ===================");

                    //TODO enable transaction control, repository fails, achProxyFactory instructions should be dequeued
                    var instructionId = AchProxy.Queue(payments, GetPreviousBusinessDay(payments.First()));
                    Logger.Info($"The 'Queue' method was executed and returned the instructionId:{instructionId}.");

                    payments.ForEach(payment =>
                    {
                        payment.InstructionId = instructionId;
                    });
                    Repository.Add(payments);

                    var hub = EventHubFactory.Create(TokenReader);
                    payments.ForEach(payment =>
                    {
                        hub.Publish(new PaymentAdded(payment));
                        Logger.Info($"The event 'PaymentAdded' with loan #{payment.LoanReferenceNumber}, Amount:${payment.Amount}, Type:{payment.Type}, DueDate:{payment.DueDate}, PaymentId:{payment.Id}, was published.");
                    });

                    Logger.Info("\n=================== [ Finish QueueAndSave ] ===================\n");
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Unhandled exception while queue and save payment: ", ex);
                throw;
            }
        }

        protected void ValidateRequest(IPaymentRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            if (request.Amount < 0.01)
                throw new InvalidArgumentException("Payment amount should be a minimum of 0.01");

            var today = TenantTime.Today;
            var todayInfo = Calendar.GetDate(today.Year, today.Month, today.Day);

            if (request.DueDate < todayInfo.NextBusinessDay)
            {
                Logger.Info($"The due date [{request.DueDate.ToString("yyyy-MM-dd")}] is less than the next business day.", new
                {
                    request.LoanReferenceNumber,
                    DueDate = request.DueDate.ToString("yyyy-MM-dd"),
                    request.Amount,
                    request.Type,
                    request.Method,
                    NextBusinessDay = todayInfo.NextBusinessDay.ToString("yyyy-MM-dd")
                });
                throw new ArgumentException("The due date is less than the next business day.");
            }
                
            var dueDateInfo = Calendar.GetDate(request.DueDate.Year, request.DueDate.Month, request.DueDate.Day);
            if(dueDateInfo.Type != DateType.Business)
                throw new ArgumentException($"The payment cannot be made on #{dueDateInfo.Type}s");

            var tenantConfiguration = ConfigurationFactory.Create<TenantConfiguration>("tenant", TokenReader).Get();

            //TODO parse time expression such as 14:55
            var timeLimit = today.AddHours(int.Parse(tenantConfiguration.PaymentTimeLimit));

            if (request.DueDate == todayInfo.NextBusinessDay && TenantTime.Now >= timeLimit)
            {
                Logger.Info($"Payment window for {request.DueDate.ToString("yyyy-MM-dd")} has closed.", new
                {
                    request.LoanReferenceNumber,
                    request.DueDate,
                    request.Amount,
                    request.Type,
                    request.Method,
                    TimeLimit = timeLimit,
                    TenantTime.Now
                });
                throw new ArgumentException($"Payment window for {request.DueDate.ToString("yyyy-MM-dd")} has closed.");
            }

            if (Repository.All(p => p.LoanReferenceNumber == request.LoanReferenceNumber 
									&& p.DueDate == request.DueDate
									&& (p.Status == PaymentStatus.Pending && p.Status == PaymentStatus.Received)).Result.Any())
                throw new PaymentAlreadyExistException("Payment for the same day already scheduled. Please cancel the other payment before proceeding.");
        }

        private DateTimeOffset GetPreviousBusinessDay(IPaymentRequest payment)
        {
            return Calendar.GetDate(payment.DueDate.Year, payment.DueDate.Month, payment.DueDate.Day).PreviousBusinessDay;
        }

        internal void AssertPaymentCanBeCancelled(IAchPayment payment)
        {
            try
            {
                if (payment.Type == PaymentType.Scheduled)
                    throw new ArgumentException("It is not possible to cancel a Scheduled payment.");

                if (Repository.All<AchPayment>(p => p.InstructionId == payment.InstructionId && p.Id != payment.Id).Any())
                    throw new ArgumentException("Multiple payments are associated to the same ACH instruction.");

                AssertPaymentExecutionDateIsInsideLimitTenant(payment);
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private void AssertPaymentExecutionDateIsInsideLimitTenant(IAchPayment payment)
        {
            try
            {
                var today = TenantTime.Today;

				var tenantConfiguration = ConfigurationFactory.Create<TenantConfiguration>("tenant", TokenReader).Get();
				var todayInfo = Calendar.GetDate(today.Year, today.Month, today.Day);
				var timeLimit = today.AddHours(int.Parse(tenantConfiguration.PaymentTimeLimit));

				if (payment.DueDate == todayInfo.NextBusinessDay && TenantTime.Now >= timeLimit)
                    throw new ArgumentException($"Payment cancellation window for {payment.DueDate.ToString("yyyy-MM-dd")} has closed.");
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
        }

        protected AchPayment CreatePayment(IPaymentRequest request)
        {
            return InitializeStatus(new AchPayment(request));
        }

        protected AchPayment CreatePayment(IAchPayment payment, ILoanInvestor investor)
        {
            return InitializeStatus(new AchPayment(payment, investor.Id));
        }

        protected AchPayment CreatePayment(IPaymentRequest request, ILoanInvestor investor)
        {
            return InitializeStatus(new AchPayment(request, investor.Id));
        }

        protected AchPayment CreatePayment(IPaymentRequest request, double amount, ILoanInvestor investor)
        {
            return InitializeStatus(new AchPayment(request, amount, investor.Id));
        }

        protected AchPayment CreateFeePayment(IPaymentRequest request, double amount, string code, ILoanInvestor investor)
        {
            return InitializeStatus(new AchPayment(request, amount, PaymentType.Fee, code, investor.Id));
        }
    }
}