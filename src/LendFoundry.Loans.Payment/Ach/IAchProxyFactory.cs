using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Payment.Ach
{
    public interface IAchProxyFactory
    {
        IAchProxy Create(ITokenReader reader);
    }
}