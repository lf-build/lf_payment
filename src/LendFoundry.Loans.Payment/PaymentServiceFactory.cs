﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Fees.Client;
using LendFoundry.Loans.Payment.Ach;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Payment
{
    public class PaymentServiceFactory : IPaymentServiceFactory
    {
        public PaymentServiceFactory(
            IConfigurationServiceFactory configurationServiceFactory,
            IConfigurationServiceFactory<PaymentConfiguration> paymentConfigurationServiceFactory,
            IAchProxyFactory achProxyFactory,
            ICalendarServiceFactory calendarServiceFactory,
            IPaymentRepositoryFactory repositoryFactory,
            ILoanServiceFactory loanServiceFactory,
            ITenantTimeFactory tenantTimeFactory,
            IPaymentPauseRepositoryFactory paymentPauseRepositoryFactory,
            ILoanFeeServiceFactory loanFeeServiceFactory,
            IEventHubClientFactory eventHubFactory,
            ILoggerFactory loggerFactory
        )
        {
            ConfigurationServiceFactory = configurationServiceFactory;
            PaymentConfigurationServiceFactory = paymentConfigurationServiceFactory;
            AchProxyFactory = achProxyFactory;
            CalendarServiceFactory = calendarServiceFactory;
            RepositoryFactory = repositoryFactory;
            LoanServiceFactory = loanServiceFactory;
            TenantTimeFactory = tenantTimeFactory;
            PaymentPauseRepositoryFactory = paymentPauseRepositoryFactory;
            LoanFeeServiceFactory = loanFeeServiceFactory;
            EventHubFactory = eventHubFactory;
            LoggerFactory = loggerFactory;
        }

        private IConfigurationServiceFactory ConfigurationServiceFactory { get; }
        private IConfigurationServiceFactory<PaymentConfiguration> PaymentConfigurationServiceFactory { get; }
        private IAchProxyFactory AchProxyFactory { get; }
        private ICalendarServiceFactory CalendarServiceFactory { get; }
        private IPaymentRepositoryFactory RepositoryFactory { get; }
        private ILoanServiceFactory LoanServiceFactory { get; }
        private ITenantTimeFactory TenantTimeFactory { get; }
        private IPaymentPauseRepositoryFactory PaymentPauseRepositoryFactory { get; }
        private ILoanFeeServiceFactory LoanFeeServiceFactory { get; }
        private IEventHubClientFactory EventHubFactory { get; }
        private ILoggerFactory LoggerFactory { get; }

        public IPaymentService Create(ITokenReader reader)
        {
            var configuration = PaymentConfigurationServiceFactory.Create(reader).Get();
            var repository = RepositoryFactory.Create(reader);
            var tenantTime = TenantTimeFactory.Create(ConfigurationServiceFactory, reader);
            var loanService = LoanServiceFactory.Create(reader);
            var logger = LoggerFactory.CreateLogger();

            var paymentStrategyFactory = new PaymentStrategyFactory(
                repository,
                AchProxyFactory,
                EventHubFactory, 
                tenantTime,
                CalendarServiceFactory.Create(reader),
                ConfigurationServiceFactory, 
                reader,
                LoanFeeServiceFactory.Create(reader),
                loanService,
                logger,
                PaymentConfigurationServiceFactory
            );

            return new PaymentService(
                configuration,
                paymentStrategyFactory,
                new ReconciliationStrategyFactory(repository, EventHubFactory.Create(reader), logger),
                repository,
                loanService,
                tenantTime,
                PaymentPauseRepositoryFactory.Create(reader),
                EventHubFactory.Create(reader),
                logger
            );
        }
    }
}
