using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Fees;
using LendFoundry.Security.Tokens;
using System.Collections.Generic;
using System.Linq;
using System;

namespace LendFoundry.Loans.Payment.Check
{
    public class InstallmentAndExtraPaymentStrategy : CheckPaymentStrategyBase, IPaymentStrategy
    {
        public InstallmentAndExtraPaymentStrategy
            (
                IPaymentRepository repository,
                IEventHubClientFactory eventHubFactory,
                ITenantTime tenantTime,
                ICalendarService calendar,
                IConfigurationServiceFactory<PaymentConfiguration> configurationFactory,
                ITokenReader tokenReader,
                ILoanFeeService feeService,
                ILogger logger,
                ILoanService loanService
            )
            : base
            (
                  repository, 
                  eventHubFactory, 
                  tenantTime, 
                  calendar, 
                  configurationFactory, 
                  tokenReader, 
                  feeService, 
                  logger
            )
        {
            LoanService = loanService;
        }

        private ILoanService LoanService { get; }

        public IEnumerable<IPayment> AddPayment(IPaymentRequest request)
        {
            try
            {
                var checkPaymentRequest = ValidateRequest(request);

                if (!new[] { PaymentType.Scheduled, PaymentType.Extra }.Any( t => t == checkPaymentRequest.Type))
                    throw new ArgumentException("The payment type should be 'Scheduled' or 'Extra'.");

                if (checkPaymentRequest.FeeAmount >= checkPaymentRequest.CheckAmount)
                    throw new ArgumentException("The fee amount should be less than the total check amount.");

                var loanInfo = LoanService.GetLoanInfo(checkPaymentRequest.LoanReferenceNumber);
                if (loanInfo == null)
                    throw new LoanNotFoundException(checkPaymentRequest.LoanReferenceNumber);

                var checkBalance = checkPaymentRequest.CheckAmount - checkPaymentRequest.FeeAmount;

                var installmentAmount = loanInfo.Summary.DaysPastDue > 0
                    ? loanInfo.Summary.CurrentDue
                    : loanInfo.Summary.NextInstallmentAmount;

                if (!installmentAmount.HasValue)
                    throw new ArgumentException("No amount due or installment amount available.");

                var paymentConfiguration = ConfigurationFactory.Create(TokenReader).Get();
                var feeCode = paymentConfiguration.CheckProcessingFeeCode;

                if (checkBalance < installmentAmount.Value)
                {
                    var extraPaymentWithFee = new List<ICheckPayment>
                    {
                        CreateFeePayment(checkPaymentRequest, checkPaymentRequest.FeeAmount, feeCode),
                        CreatePayment(checkPaymentRequest, checkBalance, PaymentType.Extra)
                    };
                    
                    return AddPaymentsAndReconcile(extraPaymentWithFee.ToArray());
                }

                var payments = new List<ICheckPayment>
                {
                    CreateFeePayment(checkPaymentRequest, checkPaymentRequest.FeeAmount, feeCode),
                    CreatePayment(checkPaymentRequest, installmentAmount.Value, PaymentType.Scheduled)
                };
                
                var extraAmount = checkBalance - installmentAmount.Value;
                AssertPaymentAmountIsLessThanPrincipalBalance(loanInfo, checkPaymentRequest, extraAmount);

                if (extraAmount > 0)
                    payments.Add(CreatePayment(checkPaymentRequest, extraAmount, PaymentType.Extra));

                return AddPaymentsAndReconcile(payments.ToArray());
            }
            catch (ArgumentException ex)
            {
                Logger.Error($"Unhandled exception while added the {request.Type} payment request: ", ex);
                throw;
            }
        }

        public IEnumerable<IPayment> RetryPayment(IPayment payment, DateTimeOffset dueDate, BankAccount bankAccount)
        {
            throw new ArgumentException($"Check payments cannot be retried");
        }

        private void AssertPaymentAmountIsLessThanPrincipalBalance(ILoanInfo loanInfo,  ICheckPaymentRequest request, double extraAmount)
        {
            if (request.Type == PaymentType.Scheduled || request.Type == PaymentType.Extra)
            {
                var installments = LoanService.GetPaymentSchedule(request.LoanReferenceNumber);
                var precedingInstallment = installments.LastOrDefault(i => (i.PaymentDate ?? i.DueDate) <= request.DueDate);
                var principalBalance = precedingInstallment?.EndingBalance ?? loanInfo.Loan.Terms.LoanAmount;

                if (extraAmount >= principalBalance)
                    throw new InvalidArgumentException("Payment amount is greater than the principal balance at the given date");
            }
        }
    }
}