using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Fees;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using LendFoundry.Loans.Payment.Events;

namespace LendFoundry.Loans.Payment.Check
{
    internal class PayoffPaymentStrategy : CheckPaymentStrategyBase, IPaymentStrategy
    {
        public PayoffPaymentStrategy
            (
                IPaymentRepository repository,
                IEventHubClientFactory eventHubFactory,
                ITenantTime tenantTime,
                ICalendarService calendar,
                IConfigurationServiceFactory<PaymentConfiguration> configurationFactory,
                ITokenReader tokenReader,
                ILoanFeeService feeService,
                ILogger logger,
                ILoanService loanService
            )
            : base
            (
                  repository, 
                  eventHubFactory, 
                  tenantTime, 
                  calendar, 
                  configurationFactory, 
                  tokenReader, 
                  feeService, 
                  logger
            )
        {
            LoanService = loanService;
        }

        private ILoanService LoanService { get; }

        public IEnumerable<IPayment> AddPayment(IPaymentRequest request)
        {
            try
            {
                var checkPaymentRequest = ValidateRequest(request);

                if (checkPaymentRequest.Type != PaymentType.Payoff)
                    throw new ArgumentException("The payment type should be 'Payoff'.");

                if (checkPaymentRequest.FeeAmount >= checkPaymentRequest.CheckAmount)
                    throw new ArgumentException("The fee amount should be less than the total check amount.");

                var payoff = LoanService.GetPayoffAmount(checkPaymentRequest.LoanReferenceNumber, checkPaymentRequest.DueDate.Date);
                if(payoff == null)
                    throw new ArgumentException("Could not calculate the payoff amount.");

                if(payoff.Amount <= 0)
                    throw new ArgumentException("The payoff amount should be greather zero.");

                if ((checkPaymentRequest.CheckAmount - checkPaymentRequest.FeeAmount) < payoff.Amount)
                    throw new ArgumentException("The payoff amount is greather than check amount minus fee.");

                var paymentConfiguration = ConfigurationFactory.Create(TokenReader).Get();
                var feeCode = paymentConfiguration.CheckProcessingFeeCode;

                var paymentBalance = checkPaymentRequest.CheckAmount - checkPaymentRequest.FeeAmount;
                var overpayment = Math.Round(paymentBalance - payoff.Amount, 2);

                ICheckPayment[] payments = {
                    CreateFeePayment(checkPaymentRequest, checkPaymentRequest.FeeAmount, feeCode),
                    CreatePayment(checkPaymentRequest, payoff.Amount, PaymentType.Payoff)
                };

                var result= AddPaymentsAndReconcile(payments);

                if (overpayment > 0)
                {
                    var eventHubClient = EventHubFactory.Create(TokenReader);
                    var paymentToBeRefunded = new PaymentToBeRefunded
                    {
                        CheckAmount = checkPaymentRequest.CheckAmount,
                        Method = checkPaymentRequest.Method,
                        PayoffAmount = payoff.Amount,
                        RefundAmount = overpayment,
                        Type = checkPaymentRequest.Type,
                        PaymentDate = checkPaymentRequest.DueDate,
                        LoanReferenceNumber = checkPaymentRequest.LoanReferenceNumber
                    };

                    var task= eventHubClient.Publish(paymentToBeRefunded);
                    task.Wait();

                    Logger.Info($"Send {nameof(PaymentToBeRefunded)} Events to EventHub.");

                }

                return result;
            }
            catch (ArgumentException ex)
            {
                Logger.Error("Unhandled exception while added the payment request: ", ex);
                throw;
            }
        }

        public IEnumerable<IPayment> RetryPayment(IPayment payment, DateTimeOffset dueDate, BankAccount bankAccount)
        {
            throw new ArgumentException("Payoff payments cannot be retried");
        }
    }
}