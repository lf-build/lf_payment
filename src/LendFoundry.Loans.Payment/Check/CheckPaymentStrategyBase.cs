using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Fees;
using LendFoundry.Loans.Payment.Events;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LendFoundry.Loans.Payment.Check
{
    public class CheckPaymentStrategyBase : PaymentStrategyBase
    {
        protected CheckPaymentStrategyBase
            (
                IPaymentRepository repository,
                IEventHubClientFactory eventHubFactory,
                ITenantTime tenantTime,
                ICalendarService calendar,
                IConfigurationServiceFactory<PaymentConfiguration> configurationFactory,
                ITokenReader tokenReader,
                ILoanFeeService feeService,
                ILogger logger
            ) : base(eventHubFactory, tenantTime)
        {
            Repository = repository;
            Calendar = calendar;
            ConfigurationFactory = configurationFactory;
            TokenReader = tokenReader;
            FeeService = feeService;
            Logger = logger;
        }

        protected ICalendarService Calendar { get; }

        protected IConfigurationServiceFactory<PaymentConfiguration> ConfigurationFactory { get; }

        protected ILoanFeeService FeeService { get; }

        protected ILogger Logger { get; }

        protected IPaymentRepository Repository { get; }

        protected ITokenReader TokenReader { get; }

		protected ICheckPayment CreateEvents(ICheckPaymentRequest request, List<object> events)
		{
			try
			{
				if (request == null)
					throw new ArgumentNullException(nameof(request));

                var payment = ChangeStatus(new CheckPayment(request), PaymentStatus.Received);

				Repository.Add(new[] { payment });

				Logger.Info($"=================== [ Start Payment {payment.Id} ] ===================");
				Logger.Info($"The event 'PaymentAdded' with loan #{payment.LoanReferenceNumber}, CheckAmount:${payment.CheckAmount}, Amount:${payment.Amount}, Type:{payment.Type}, CheckDate:{payment.DueDate}, CheckNumber:{payment.CheckNumber},  PaymentId:{payment.Id}, was published.");

				events.Add(new PaymentAdded(payment));
				events.Add(new PaymentReceived(payment));
				
				return payment;
			}
			catch (Exception ex)
			{
				Logger.Error("Unhandled exception while add payment and reconcile: ", ex);
				throw;
			}
		}

		protected ICheckPaymentRequest ValidateRequest(IPaymentRequest request)
        {
            try
            {
                var checkPaymentRequest = request as ICheckPaymentRequest;

                if (checkPaymentRequest == null)
                    throw new ArgumentNullException(nameof(request));

                if (checkPaymentRequest.FeeAmount < 0)
                    throw new ArgumentException("The 'Fee Amount' should be provided.");

                if (checkPaymentRequest.CheckAmount < 0)
                    throw new ArgumentException("The 'Check Amount' should be provided.");

                if (string.IsNullOrWhiteSpace(checkPaymentRequest.RoutingNumber))
                    throw new InvalidArgumentException("Payment 'Routing Number' should be provided.");

                return checkPaymentRequest;
            }
            catch (ArgumentException ex)
            {
                Logger.Error("Unhandled exception while validate the payment request: ", ex);
                throw;
            }
        }

        public void CancelPayment(IPayment payment, string notes)
        {
            try
            {
                if (payment == null)
                    throw new ArgumentNullException(nameof(payment));

                ChangeStatus(payment, PaymentStatus.Cancelled);
                Repository.Update(payment);
                EventHubFactory.Create(TokenReader).Publish(new PaymentCancelled(payment, notes));
            }
            catch (Exception ex)
            {
                Logger.Error("Unhandled exception while cancel payment: ", ex);
                throw;
            }
        }
        
        public void FailPayment(IPayment payment, string failureCode, string failureDescription)
        {
            if (payment == null)
                throw new ArgumentNullException(nameof(payment));

            if (payment.Status == PaymentStatus.Failed)
                throw new ArgumentException($"Payment {payment.Id} from loan {payment.LoanReferenceNumber} is already marked as failed", nameof(payment));

            Logger.Info("Loan {LoanReferenceNumber}: marking payment {Id} as "+PaymentStatus.Failed, payment);

            payment.FailureCode = failureCode;
            payment.FailureDescription = failureDescription;
            ChangeStatus(payment, PaymentStatus.Failed);
            Repository.Update(payment);

            Logger.Info("Loan {LoanReferenceNumber}: payment {Id} as been marked as "+PaymentStatus.Failed, payment);

            var staticReader = new StaticTokenReader(TokenReader.Read());
            var eventHub = EventHubFactory.Create(staticReader);
            var @event = new PaymentFailed(payment);
            eventHub.Publish(@event);

            Logger.Info($"Loan {{LoanReferenceNumber}}: event {@event.GetType().Name} has been published for payment {{Id}}", payment);
        }

        protected IEnumerable<ICheckPayment> AddPaymentsAndReconcile(params ICheckPayment[] requests)
        {
            if (requests == null) throw new ArgumentNullException(nameof(requests));
            if (!requests.Any()) throw new ArgumentException(nameof(requests), $"{nameof(requests)} is empty");

            var payments = MarkAsReceived(requests).ToList();

            foreach (var payment in payments)
                Logger.Info("Adding {Type} payment of ${Amount} due on {DueDate} as {Method}", payment);

            Repository.Add(payments);

            PublishEventsForAddedPayments(payments);

            return payments;
        }

        private void PublishEventsForAddedPayments(IEnumerable<ICheckPayment> payments)
        {
            var events = payments.SelectMany(p => new[] {
                new PaymentAdded(p),
                new PaymentReceived(p)
            }).ToList();

            var eventHub = EventHubFactory.Create(new StaticTokenReader(TokenReader.Read()));

            foreach (var @event in events)
                Logger.Info($"Publishing event {@event.GetType().Name} for payment {{PaymentId}}", @event);

            eventHub.PublishBatchWithInterval(events, 1000);
        }

        private IEnumerable<ICheckPayment> MarkAsReceived(IEnumerable<ICheckPaymentRequest> payments)
        {
            return payments.Select(p => new CheckPayment(p)
            {
                Status = PaymentStatus.Received
            });
        }

        protected CheckPayment CreatePayment(ICheckPaymentRequest request, double amount, PaymentType type)
        {
            return InitializeStatus(new CheckPayment(request, amount, type));
        }

        protected CheckPayment CreateFeePayment(ICheckPaymentRequest request, double amount)
        {
            return InitializeStatus(new CheckPayment(request, amount, PaymentType.Fee));
        }

        protected CheckPayment CreateFeePayment(ICheckPaymentRequest request, double amount, string feeCode)
        {
            return InitializeStatus(new CheckPayment(request, amount, PaymentType.Fee, feeCode));
        }
    }
}
