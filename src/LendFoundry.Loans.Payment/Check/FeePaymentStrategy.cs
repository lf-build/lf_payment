using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Fees;
using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Payment.Check
{
    internal class FeePaymentStrategy : CheckPaymentStrategyBase, IPaymentStrategy
    {
        public FeePaymentStrategy
            (
                IPaymentRepository repository,
                IEventHubClientFactory eventHubFactory, 
                ITenantTime tenantTime, 
                ICalendarService calendar, 
                IConfigurationServiceFactory<PaymentConfiguration> configurationFactory, 
                ITokenReader tokenReader, 
                ILoanFeeService feeService,
                ILogger logger
            )
            : base
            (
                  repository, 
                  eventHubFactory, 
                  tenantTime, 
                  calendar, 
                  configurationFactory, 
                  tokenReader, 
                  feeService, 
                  logger
            )
        {
        }
        
        public IEnumerable<IPayment> AddPayment(IPaymentRequest request)
        {
            try
            {
                var checkPaymentRequest = ValidateRequest(request);

                if (checkPaymentRequest.Type != PaymentType.Fee)
                    throw new ArgumentException("The payment type should be 'Fee'.");

                if (string.IsNullOrWhiteSpace(checkPaymentRequest.Code))
                    throw new ArgumentException("The fee code should be provided.");

                if (checkPaymentRequest.FeeAmount >= checkPaymentRequest.CheckAmount)
                    throw new ArgumentException("The fee amount should be less than the total check amount.");

                var feePayment = CreateFeePayment(checkPaymentRequest, checkPaymentRequest.FeeAmount);

                return AddPaymentsAndReconcile(feePayment);
            }
            catch (ArgumentException ex)
            {
                Logger.Error("Unhandled exception while added the payment request: ", ex);
                throw;
            }
        }

        public IEnumerable<IPayment> RetryPayment(IPayment payment, DateTimeOffset dueDate, BankAccount bankAccount)
        {
            throw new ArgumentException("Fee payments cannot be retried");
        }
    }
}