using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.Payment
{
    public abstract class PaymentStrategyBase
    {
        protected PaymentStrategyBase(IEventHubClientFactory eventHubFactory, ITenantTime tenantTime)
        {
            EventHubFactory = eventHubFactory;
            TenantTime = tenantTime;
        }

        protected IEventHubClientFactory EventHubFactory { get; }

        protected ITenantTime TenantTime { get; }

        protected T InitializeStatus<T>(T payment) where T : IPayment
        {
            return ChangeStatus(payment, PaymentStatus.Pending);
        }

        protected T ChangeStatus<T>(T payment, PaymentStatus newStatus) where T : IPayment
        {
            payment.Status = newStatus;
            payment.StatusUpdated = new TimeBucket(TenantTime.Now);
            return payment;
        }
    }
}
