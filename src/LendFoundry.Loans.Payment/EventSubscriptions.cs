using LendFoundry.EventHub;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Payment.Ach.Events;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Payment
{
    public class EventSubscriptions : IEventSubscription<AchInstructionRejected>
    {
        public EventSubscriptions(IPaymentServiceFactory serviceFactory, ITokenReaderFactory tokenReaderFactory, ILoggerFactory loggerFactory)
        {
            ServiceFactory = serviceFactory;
            TokenHandlerFactory = tokenReaderFactory;
            LoggerFactory = loggerFactory;
        }

        private IPaymentServiceFactory ServiceFactory { get; }
        private ITokenReaderFactory TokenHandlerFactory { get; }
        private ILoggerFactory LoggerFactory { get; }

        public void Handle(IEventInfo @event, AchInstructionRejected data)
        {
            var logger = LoggerFactory.CreateLogger();

            logger.Info("Event received: {EventName}(ReferenceNumber={ReferenceNumber} InstructionId={InstructionId} ReturnCode={ReturnCode} Reason={Reason})", new
            {
                EventName = data.GetType().Name,
                InstructionId = data.Instruction?.Id,
                data.Instruction?.Type,
                data.Instruction?.Status,
                data.Instruction?.ReturnCode,
                data.Instruction?.ReferenceNumber,
                data.Instruction?.InternalReferenceNumber,
                data.Instruction?.Frequency,
                data.Instruction?.ExecutionDate,
                data.Instruction?.Amount,
                data.Instruction?.EntryDescription,
                data.Reason,
                data.Description
            });

            var reader = TokenHandlerFactory.Create(@event.TenantId, string.Empty);
            var service = ServiceFactory.Create(reader);
            service.HandleAchRejection(data.Instruction.Id, data.Reason, data.Description);
        }
    }
}