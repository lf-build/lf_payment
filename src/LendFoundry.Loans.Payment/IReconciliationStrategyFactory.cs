﻿namespace LendFoundry.Loans.Payment
{
    public interface IReconciliationStrategyFactory
    {
        IReconciliationStrategy Create(PaymentMethod method);
    }
}