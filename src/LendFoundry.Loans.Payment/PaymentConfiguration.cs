namespace LendFoundry.Loans.Payment
{
    public class PaymentConfiguration : IPaymentConfiguration
    {
        public string DefaultFundingSource { get; set; }

        public int MaxNumberOfAttempts { get; set; }

        public int AttemptIntervalInDays { get; set; }

        public string CheckProcessingFeeCode { get; set; }

        public double CheckProcessingFeeAmount { get; set; }

        public string RetryEntryDescription { get; set; }

        public string LateFeeCode { get; set; }

        public string ReturnFeeCode { get; set; }

        public string LateFeeEntryDescription { get; set; }

        public string ReturnFeeEntryDescription { get; set; }

        public string LateFeeInvestorId { get; set; }

        public string ReturnFeeInvestorId { get; set; }

        public string AcceptedAchRejectionCodeForPaymentFailures { get; set; }
    }
}