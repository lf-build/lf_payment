﻿using System;

namespace LendFoundry.Loans.Payment
{
    public interface IReconciliationStrategy
    {
        void ReconcilePendingPayments(DateTimeOffset dueDate);
    }
}