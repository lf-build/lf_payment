﻿using System;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Logging;

namespace LendFoundry.Loans.Payment
{
    public class ReconciliationStrategyFactory : IReconciliationStrategyFactory
    {
        public ReconciliationStrategyFactory(IPaymentRepository repository, IEventHubClient eventHub, ILogger logger)
        {
            Repository = repository;
            EventHub = eventHub;
            Logger = logger;
        }

        private IPaymentRepository Repository { get; }

        private IEventHubClient EventHub { get; }

        private ILogger Logger { get; }

        public IReconciliationStrategy Create(PaymentMethod method)
        {
            switch (method)
            {
                case PaymentMethod.ACH:
                    return new Ach.ReconciliationStrategy(Repository, EventHub, Logger);
                case PaymentMethod.Check:
                    throw new NotImplementedException();
                default:
                    throw new ArgumentException($"Invalid payment method '{method}'");
            } 
        }
    }
}