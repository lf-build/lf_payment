﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client;
using System.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.EventHub.Client;
using LendFoundry.Loans.Payment.Events;
using System.Linq.Expressions;
using LendFoundry.Foundation.Logging;
using System.Text.RegularExpressions;

namespace LendFoundry.Loans.Payment
{
    public class PaymentService : IPaymentService
    {
        public PaymentService
        (
            IPaymentConfiguration configuration,
            IPaymentStrategyFactory paymentStrategyFactory,
            IReconciliationStrategyFactory reconciliationStrategyFactory,
            IPaymentRepository repository,
            ILoanService loanService,
            ITenantTime tenantTime,
            IPaymentPauseRepository paymentPauseRepository,
            IEventHubClient eventHub,
            ILogger logger
        )
        {
            if (configuration == null)
                throw new ArgumentNullException(nameof(configuration));

            Configuration = configuration;
            PaymentStrategyFactory = paymentStrategyFactory;
            ReconciliationStrategyFactory = reconciliationStrategyFactory;
            Repository = repository;
            LoanService = loanService;
            TenantTime = tenantTime;
            PaymentPauseRepository = paymentPauseRepository;
            EventHub = eventHub;
            Logger = logger;
        }

        private IPaymentConfiguration Configuration { get; }
        private IPaymentStrategyFactory PaymentStrategyFactory { get; }
        private IReconciliationStrategyFactory ReconciliationStrategyFactory { get; }
        private IPaymentRepository Repository { get; }
        private ILoanService LoanService { get; }
        private ITenantTime TenantTime { get; }
        private IPaymentPauseRepository PaymentPauseRepository { get;  }
        private IEventHubClient EventHub { get;  }
        private ILogger Logger { get; }
        
        public Task<IEnumerable<IPayment>> GetAllPayments(string loanReferenceNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException($"{loanReferenceNumber} is null or empty");

            return Repository.All(p => p.LoanReferenceNumber == loanReferenceNumber);
        }
        
        public Task<IEnumerable<IPayment>> GetPaymentsWithStatus(PaymentStatus status, DateTimeOffset statusUpdated)
        {
            if (statusUpdated == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(statusUpdated)}` is either empty or invalid");

            var date = new TimeBucket(statusUpdated);

            return Repository.All(p => 
                p.Status == status &&
                p.StatusUpdated != null && 
                p.StatusUpdated.Day == date.Day);
        }
        
        public Task<IEnumerable<IPayment>> GetPaymentsWithStatus(PaymentStatus status, DateTimeOffset statusUpdated, PaymentMethod method)
        {
            if (statusUpdated == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(statusUpdated)}` is either empty or invalid");

            var date = new TimeBucket(statusUpdated);

            return Repository.All(p => 
                p.Status == status &&
                p.StatusUpdated != null && 
                p.StatusUpdated.Day == date.Day &&
                p.Method == method);
        }

        public Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date)
        {
            if (date == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(date)}` is either empty or invalid");

            return Repository.All(p => p.DueDate == date);
        }

        public Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentMethod method)
        {
            if (date == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(date)}` is either empty or invalid");

            if (method == PaymentMethod.Undefined)
                throw new ArgumentException($"Argument for parameter `{nameof(method)}` cannot be {method}");

            return Repository.All(p => p.DueDate == date && p.Method == method);
        }

        public Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentStatus status)
        {
            if (date == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(date)}` is either empty or invalid");

            return Repository.All(p => p.DueDate == date && p.Status == status);
        }

        public Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentMethod method, PaymentStatus status)
        {
            if (date == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(date)}` is either empty or invalid");

            if (method == PaymentMethod.Undefined)
                throw new ArgumentException($"Argument for parameter `{nameof(method)}` cannot be {method}");

            return Repository.All(p => p.DueDate == date && p.Method == method && p.Status == status);
        }

        public IEnumerable<IPayment> AddPayment(IPaymentRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            request.DueDate = TenantTime.FromDate(request.DueDate.Date);

            AssertPaymentCanBeAdded(request);

            return PaymentStrategyFactory
                .Create(request.Method, request.Type)
                .AddPayment(request);
        }

        public void CancelPayment(string id, string notes)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException(nameof(id));

            var payment = Repository.Get(id).Result;
            if (payment == null)
                throw new NotFoundException($"Payment {id} not found.");

            PaymentStrategyFactory
                .Create(payment.Method, payment.Type)
                .CancelPayment(payment, notes);
        }

        public void FailPayment(string id, string failureCode, string failureDescription)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException(nameof(id));

            var payment = Repository.Get(id).Result;
            if (payment == null)
                throw new NotFoundException($"Payment {id} not found.");

            PaymentStrategyFactory
                .Create(payment.Method, payment.Type)
                .FailPayment(payment, failureCode, failureDescription);
        }

        public void ReconcilePendingPayments(PaymentMethod method, DateTime dueDate)
        {
            if (method == PaymentMethod.Undefined)
                throw new ArgumentNullException(nameof(method));

            ReconciliationStrategyFactory
                .Create(method)
                .ReconcilePendingPayments(TenantTime.FromDate(dueDate));
        }

        private void AssertPaymentCanBeAdded(IPaymentRequest request)
        {
            if (request.Method == PaymentMethod.ACH)
            {
                var paymentPause = PaymentPauseRepository.GetByLoanReferenceNumber(request.LoanReferenceNumber).Result;
                if (paymentPause != null)
                    throw new ArgumentException("Payment cannot be scheduled as payment is paused.");
            }

            if (request.Type == PaymentType.Extra || request.Type == PaymentType.Payoff)
                AssertNoPayoffHasBeenAdded(request.LoanReferenceNumber);

            AssertNotBeforeOriginationDate(request);
        }

        private void AssertNoPayoffHasBeenAdded(string loanReferenceNumber)
        {
            var payments = Repository.All<Payment>(p => p.LoanReferenceNumber == loanReferenceNumber && p.Type == PaymentType.Payoff && (p.Status == PaymentStatus.Pending || p.Status == PaymentStatus.Received));

            if (payments.ToList().Any())
                throw new InvalidArgumentException("Payoff is scheduled for this loan. Please cancel payoff and attempt again.");
        }

        private void AssertNotBeforeOriginationDate(IPaymentRequest paymentRequest)
        {
            var loanInfo = LoanService.GetLoanInfo(paymentRequest.LoanReferenceNumber);
            if (loanInfo == null)
                throw new LoanNotFoundException(paymentRequest.LoanReferenceNumber);

            if (paymentRequest.DueDate <= loanInfo.Loan.Terms.OriginationDate)
                throw new ArgumentException("Payment cannot be scheduled before or on origination date.");
        }

        public async Task<IPaymentPause> Pause(string loanReferenceNumber, IPaymentPauseRequest paymentPauseRequest)
        {
            if (paymentPauseRequest == null)
                throw new ArgumentNullException(nameof(paymentPauseRequest));

            var paymentPause = new PaymentPause
            {
                Notes = paymentPauseRequest.Notes,
                LoanReferenceNumber = loanReferenceNumber
            };

            if (string.IsNullOrWhiteSpace(paymentPause.LoanReferenceNumber))
                throw new ArgumentException("Loan Reference Number is required", nameof(paymentPause.LoanReferenceNumber));

            var loanInfo = LoanService.GetLoanInfo(loanReferenceNumber);
            if (loanInfo == null)
                throw new LoanNotFoundException(loanReferenceNumber);

            var paymentPauseDate = TenantTime.FromDate(paymentPauseRequest.StartDate.Date);
            if (paymentPauseDate < TenantTime.Today)
                throw new ArgumentException("StartDate should be greater than todays date");

            paymentPause.StartDate = paymentPauseDate;

            if (paymentPauseRequest.EndDate != null)
            {
                var paymentResumeDate = TenantTime.FromDate(paymentPauseRequest.EndDate.Value.Date);

                if (paymentResumeDate < paymentPauseDate)
                    throw new ArgumentException("EndDate should be greater than start date");

                paymentPause.EndDate = paymentResumeDate;
            }

            var activePause = await PaymentPauseRepository.GetByLoanReferenceNumber(paymentPause.LoanReferenceNumber);
            if (activePause != null)
                throw new ActivePauseAlreadyExistException($"Active Pause for {paymentPause.LoanReferenceNumber} is already exist");

            var result = await PaymentPauseRepository.AddOrReplace(paymentPause);

            await EventHub.Publish(new PaymentPaused(paymentPause));

            return result;
        }

        public async Task Resume(string loanReferenceNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(loanReferenceNumber));

            var loanInfo = LoanService.GetLoanInfo(loanReferenceNumber);
            if (loanInfo == null)
                throw new LoanNotFoundException(loanReferenceNumber);

            await PaymentPauseRepository.Remove(loanReferenceNumber);

            await EventHub.Publish(new PaymentResumed(loanReferenceNumber));
        }

        public async Task<IEnumerable<IPaymentPause>> GetPauses()
        {
            return await PaymentPauseRepository.Get();
        }

        public async Task<IPaymentPause> GetPauseByLoanReferenceNumber(string loanReferenceNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException("Value cannot be null or whitespace.", nameof(loanReferenceNumber));

            var loanInfo = LoanService.GetLoanInfo(loanReferenceNumber);
            if (loanInfo == null)
                throw new LoanNotFoundException(loanReferenceNumber);

            var paymentPause = await PaymentPauseRepository.GetByLoanReferenceNumber(loanReferenceNumber);
            if (paymentPause == null)
                throw new NotFoundException($"Active pause not found for {loanReferenceNumber}");
            return paymentPause;
        }

        public async Task<IEnumerable<IPayment>> RetryPayment(string paymentId, DateTimeOffset dueDate, BankAccount bankAccount)
        {
            if (string.IsNullOrWhiteSpace(paymentId))
                throw new ArgumentException($"{nameof(paymentId)} is null or blank");

            if (dueDate == default(DateTimeOffset))
                throw new ArgumentException($"{nameof(dueDate)} is empty or invalid");

            if (bankAccount != null)
            {
                if (string.IsNullOrWhiteSpace(bankAccount.AccountNumber))
                    throw new ArgumentException($"{nameof(bankAccount.AccountNumber)} is null or blank");

                if (string.IsNullOrWhiteSpace(bankAccount.RoutingNumber))
                    throw new ArgumentException($"{nameof(bankAccount.RoutingNumber)} is null or blank");

                if (bankAccount.AccountType == BankAccountType.Undefined)
                    throw new ArgumentException($"{nameof(bankAccount.AccountType)} is not defined");
            }

            var payment = await Repository.Get(paymentId);

            if (payment == null)
                throw new NotFoundException($"Payment not found");

            AssertPaymentCanBeAdded(payment);

            return PaymentStrategyFactory
                .Create(payment.Method, payment.Type)
                .RetryPayment(payment, dueDate, bankAccount);
        }

        public void HandleAchRejection(string instructionId, string rejectionCode, string description)
        {
            Logger.Info("Handling ACH rejection: instructionId={InstructionId} rejectionCode={RejectionCode} description={Description}", new { InstructionId = instructionId, RejectionCode = rejectionCode, Description = description });

            var acceptedRejectionCode = Configuration.AcceptedAchRejectionCodeForPaymentFailures;

            if (string.IsNullOrWhiteSpace(acceptedRejectionCode))
            {
                Logger.Error($"Configuration not set: `{nameof(Configuration.AcceptedAchRejectionCodeForPaymentFailures)}`. No ACH rejections will be handled.", new { InstructionId = instructionId, RejectionCode = rejectionCode });
                return;
            }

            if (!Regex.IsMatch(rejectionCode, acceptedRejectionCode))
            {
                Logger.Info("ACH rejection for instruction `{InstructionId}` will not be handled because the reason `{RejectionCode}` does not match the expected reason `{AcceptedAchRejectionCodeForPaymentFailures}`", new { InstructionId = instructionId, RejectionCode = rejectionCode, Configuration.AcceptedAchRejectionCodeForPaymentFailures });
                return;
            }
            
            Expression<Func<AchPayment, bool>> query = p => p.InstructionId == instructionId;
            
            var payments = Repository
                .All(query)
                .OfType<IAchPayment>()
                .ToList();

            Logger.Info("Found {PaymentsFound} payment(s) with rejected instruction {InstructionId}", new { PaymentsFound = payments.Count, InstructionId = instructionId});

            var affectedPayments = payments.Where(p =>
                p.Status != PaymentStatus.Failed &&
                p.Status != PaymentStatus.Cancelled);

            foreach(var payment in payments.Except(affectedPayments))
            {
                Logger.Warn("Loan {LoanReferenceNumber}: payment {Id} cannot be marked as failed because its current status is {Status}", new { payment.Id, payment.LoanReferenceNumber, payment.Status, payment.Type, payment.InstructionId });
            }

            foreach(var payment in affectedPayments)
            {
                try
                {
                    PaymentStrategyFactory
                        .Create(payment.Method, payment.Type)
                        .FailPayment(payment, rejectionCode, description);
                }
                catch(Exception e)
                {
                    Logger.Error("Loan {LoanReferenceNumber}: error while marking payment {Id} as {NewStatus}", e, new { payment.Id, payment.LoanReferenceNumber, payment.Status, payment.Type, payment.InstructionId, NewStatus = PaymentStatus.Failed });
                }
            }
        }

        public PaymentStatus GetPaymentStatus(string loanReferenceNumber)
        {
            return Repository.GetStatus(loanReferenceNumber);
        }


    }
}