﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Fees;
using LendFoundry.Loans.Payment.Ach;
using LendFoundry.Loans.Payment.Check;
using LendFoundry.Security.Tokens;
using PayoffPaymentStrategy = LendFoundry.Loans.Payment.Ach.PayoffPaymentStrategy;
using System;

namespace LendFoundry.Loans.Payment
{
    public class PaymentStrategyFactory : IPaymentStrategyFactory
    {
        public PaymentStrategyFactory
        (
            IPaymentRepository repository,
            IAchProxyFactory achProxyFactory,
            IEventHubClientFactory eventHubClientFactory,
            ITenantTime tenantTime,
            ICalendarService calendar,
            IConfigurationServiceFactory configurationFactory,
            ITokenReader tokenReader,
            ILoanFeeService feeService,
            ILoanService loanService,
            ILogger logger,
            IConfigurationServiceFactory<PaymentConfiguration> paymentConfigurationServiceFactory
        )
        {
            Repository = repository;
            AchProxyFactory = achProxyFactory;
            EventHubClientFactory = eventHubClientFactory;
            FeeService = feeService;
            TenantTime = tenantTime;
            Calendar = calendar;
            ConfigurationFactory = configurationFactory;
            TokenReader = tokenReader;
            LoanService = loanService;
            Logger = logger;
            PaymentConfigurationServiceFactory = paymentConfigurationServiceFactory;
        }

        private IPaymentRepository Repository { get; }

        private IAchProxyFactory AchProxyFactory { get; }

        private IEventHubClientFactory EventHubClientFactory { get; }

        private ILoanFeeService FeeService { get; }

        private ITenantTime TenantTime { get; }

        private ICalendarService Calendar { get; }

        private ILoanService LoanService { get; }

        private IConfigurationServiceFactory ConfigurationFactory { get; }

        private ITokenReader TokenReader { get; }

        private ILogger Logger { get; }

        private IConfigurationServiceFactory<PaymentConfiguration> PaymentConfigurationServiceFactory { get; }

        public IPaymentStrategy Create(PaymentMethod method, PaymentType type)
        {
            try
            {
                switch (method)
                {
                    case PaymentMethod.ACH:
                        return CreateAchStrategy(type);
                    case PaymentMethod.Check:
                        return CreateCheckStrategy(type);
                    default:
                        throw new ArgumentException($"Invalid payment method '{method}'");
                }
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private IPaymentStrategy CreateCheckStrategy(PaymentType type)
        {
            try
            {
                switch (type)
                {
                    case PaymentType.Fee:
                        return new FeePaymentStrategy(
                            Repository, EventHubClientFactory, TenantTime,
                            Calendar, PaymentConfigurationServiceFactory, TokenReader,
                            FeeService, Logger);
                    case PaymentType.Scheduled:
                    case PaymentType.Extra:
                        return new InstallmentAndExtraPaymentStrategy(
                            Repository, EventHubClientFactory, TenantTime,
                            Calendar, PaymentConfigurationServiceFactory, TokenReader,
                            FeeService, Logger, LoanService);
                    case PaymentType.Payoff:
                        return new Check.PayoffPaymentStrategy(
                            Repository, EventHubClientFactory, TenantTime,
                            Calendar, PaymentConfigurationServiceFactory, TokenReader,
                            FeeService, Logger, LoanService);
                    default:
                        throw new ArgumentException($"Invalid payment type '{type}'");
                }
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
        }

        private IPaymentStrategy CreateAchStrategy(PaymentType type)
        {
            try
            {
                switch (type)
                {
                    case PaymentType.Scheduled:
                        return new ScheduledPaymentStrategy(
                            Repository, AchProxyFactory, EventHubClientFactory,
                            TenantTime, Calendar, ConfigurationFactory,
                            TokenReader, FeeService, LoanService, Logger);
                    case PaymentType.Fee:
                    case PaymentType.Extra:
                        return new ExtraPaymentStrategy(
                            Repository, AchProxyFactory, EventHubClientFactory,
                            TenantTime, Calendar, ConfigurationFactory,
                            TokenReader, FeeService, LoanService, Logger);
                    case PaymentType.Payoff:
                        return new PayoffPaymentStrategy(
                            Repository, AchProxyFactory, EventHubClientFactory,
                            TenantTime, Calendar, ConfigurationFactory,
                            TokenReader, LoanService, Logger);
                    default:
                        throw new ArgumentException($"Invalid payment type '{type}'");
                }
            }
            catch (ArgumentException ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message, ex);
                throw;
            }
        }
    }
}