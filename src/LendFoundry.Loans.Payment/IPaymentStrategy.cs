﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentStrategy
    {
        IEnumerable<IPayment> AddPayment(IPaymentRequest request);
        IEnumerable<IPayment> RetryPayment(IPayment payment, DateTimeOffset dueDate, BankAccount bankAccount);

        void CancelPayment(IPayment payment, string notes);
        void FailPayment(IPayment payment, string failureCode, string failureDescription);
    }
}