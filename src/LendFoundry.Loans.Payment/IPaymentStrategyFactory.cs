namespace LendFoundry.Loans.Payment
{
    public interface IPaymentStrategyFactory
    {
        IPaymentStrategy Create(PaymentMethod method, PaymentType type);
    }
}