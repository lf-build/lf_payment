﻿using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Payment.Api.Models;
using Microsoft.AspNet.Mvc;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.Payment.Api.Controllers
{
    [Route("/")]
    public class PaymentController : ExtendedController
    {
        public PaymentController(IPaymentService service, ITenantTime tenantTime, ILogger logger) : base (logger)
        {
            Service = service;
            TenantTime = tenantTime;
        }

        private IPaymentService Service { get; }
        private ITenantTime TenantTime { get; }
        private NoContentResult NoContentResult { get; } = new NoContentResult();

        [HttpGet("/{loanReferenceNumber}")]
        public async Task<IActionResult> GetLoanPayments(string loanReferenceNumber)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetAllPayments(loanReferenceNumber)));
        }

        [HttpGet("/status/{status}")]
        public async Task<IActionResult> GetPaymentsWithStatus(PaymentStatus status)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetPaymentsWithStatus(status, TenantTime.Today)));
        }

        [HttpGet("/status/{status}/{statusDate}")]
        public async Task<IActionResult> GetPaymentsWithStatus(PaymentStatus status, DateTime statusDate)
        {
            if (statusDate == default(DateTime))
                return ErrorResult.BadRequest($"Argument for parameter `{nameof(statusDate)}` is either empty or invalid");
            
            return await ExecuteAsync(async () => Ok(await Service.GetPaymentsWithStatus(status, TenantTime.FromDate(statusDate))));
        }

        [HttpGet("/status/{status}/{statusDate}/{method}")]
        public async Task<IActionResult> GetPaymentsWithStatus(PaymentStatus status, DateTime statusDate, PaymentMethod method)
        {
            if (statusDate == default(DateTime))
                return ErrorResult.BadRequest($"Argument for parameter `{nameof(statusDate)}` is either empty or invalid");
            
            return await ExecuteAsync(async () => Ok(await Service.GetPaymentsWithStatus(status, TenantTime.FromDate(statusDate), method)));
        }

        [HttpGet("/due-on/{date}")]
        public async Task<IActionResult> GetPaymentsDueOn(DateTime date)
        {
            if (date == default(DateTime))
                return ErrorResult.BadRequest($"Argument for parameter `{nameof(date)}` is either empty or invalid");
            
            return await ExecuteAsync(async () => Ok(await Service.GetPaymentsDueOn(TenantTime.FromDate(date))));
        }

        [HttpGet("/due-on/{date}/method/{method}")]
        public async Task<IActionResult> GetPaymentsDueOn(DateTime date, PaymentMethod method)
        {
            if (date == default(DateTime))
                return ErrorResult.BadRequest($"Argument for parameter `{nameof(date)}` is either empty or invalid");
            
            if (method == PaymentMethod.Undefined)
                return ErrorResult.BadRequest($"Argument for parameter `{nameof(method)}` is either empty or invalid");
            
            return await ExecuteAsync(async () => Ok(await Service.GetPaymentsDueOn(TenantTime.FromDate(date), method)));
        }

        [HttpGet("/due-on/{date}/status/{status}")]
        public async Task<IActionResult> GetPaymentsDueOn(DateTime date, PaymentStatus status)
        {
            if (date == default(DateTime))
                return ErrorResult.BadRequest($"Argument for parameter `{nameof(date)}` is either empty or invalid");
            
            return await ExecuteAsync(async () => Ok(await Service.GetPaymentsDueOn(TenantTime.FromDate(date), status)));
        }

        [HttpGet("/due-on/{date}/method/{method}/status/{status}")]
        public async Task<IActionResult> GetPaymentsDueOn(DateTime date, PaymentMethod method, PaymentStatus status)
        {
            if (date == default(DateTime))
                return ErrorResult.BadRequest($"Argument for parameter `{nameof(date)}` is either empty or invalid");
            
            if (method == PaymentMethod.Undefined)
                return ErrorResult.BadRequest($"Argument for parameter `{nameof(method)}` is either empty or invalid");
            
            return await ExecuteAsync(async () => Ok(await Service.GetPaymentsDueOn(TenantTime.FromDate(date), method, status)));
        }

        [HttpPut("/ach")]
        public IActionResult Add([FromBody] AchPaymentRequest request)
        {
            return AddPayment(request);
        }

        [HttpPut("/{paymentId}/retry/{dueDate}")]
        public async Task<IActionResult> Retry(string paymentId, DateTime dueDate, [FromBody] BankAccount bankAccount)
        {
            return await ExecuteAsync(async () =>
            {
                var result = await Service.RetryPayment(paymentId, TenantTime.FromDate(dueDate), bankAccount);
                return new HttpOkObjectResult(result);
            });
        }

        [HttpPost("/ach/{loanReferenceNumber}/pause")]
        public async Task<IActionResult> Pause(string loanReferenceNumber, [FromBody] PaymentPauseRequest paymentPauseRequest)
        {
            return await ExecuteAsync(async () =>
            {
                try
                {
                    return Ok(await Service.Pause(loanReferenceNumber, paymentPauseRequest));
                }
                catch (ActivePauseAlreadyExistException exception)
                {
                    return new ErrorResult((int)HttpStatusCode.Conflict, exception.Message);
                }
            });
        }

        [HttpDelete("/ach/{loanReferenceNumber}/pause")]
        public async Task<IActionResult> Resume(string loanReferenceNumber)
        {
            return await ExecuteAsync(async () =>
            {
                await Service.Resume(loanReferenceNumber);
                return NoContentResult;
            });
        }

        [HttpGet("/ach/pauses")]
        public async Task<IActionResult> GetPauses()
        {
            return await ExecuteAsync(async () => Ok(await Service.GetPauses()));
        }

        [HttpGet("/ach/{loanReferenceNumber}/pause")]
        public async Task<IActionResult> GetPauseByLoanReferenceNumber(string loanReferenceNumber)
        {
            return await ExecuteAsync(async () => Ok(await Service.GetPauseByLoanReferenceNumber(loanReferenceNumber)));
        }


        [HttpPut("/check")]
        public IActionResult Add([FromBody] CheckPaymentRequest request)
        {
            return AddPayment(request);
        }

        [HttpPut("/{paymentId}/fail")]
        public IActionResult Add(string paymentId, [FromBody] PaymentFailure failure)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(paymentId))
                    throw new ArgumentNullException(nameof(paymentId));

                Service.FailPayment(paymentId, failure?.Code, failure?.Description);

                return new HttpStatusCodeResult(204);
            }
            catch (PaymentAlreadyExistException exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpDelete("/")]
        public IActionResult Cancel([FromBody] PaymentCancelationRequest request)
        {
            try
            {
                if (request == null)
                    throw new ArgumentNullException(nameof(request));

                Service.CancelPayment(request.Id, request.Notes);
                return new HttpStatusCodeResult(204);
            }
            catch (PaymentAlreadyExistException exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpPost("/reconcile/{method}/{dueDate}")]
        public IActionResult ReconcilePendingPayments([FromRoute] PaymentMethod method, [FromRoute] DateTime dueDate)
        {
            return Execute(() =>
            {
                if (!new[] { PaymentMethod.ACH, PaymentMethod.Check }.Any(p => p == method))
                    return ErrorResult.BadRequest("Payment method cannot be undefined.");

                Task.Run(() =>
                {
                    try
                    {
                        Service.ReconcilePendingPayments(method, dueDate);
                    }
                    catch(Exception e)
                    {
                        Logger.Error("Error during payment reconciliation", e);
                    }
                });
                
                return new HttpStatusCodeResult(204);
            });
        }

        private IActionResult AddPayment(IPaymentRequest request)
        {
            if (request == null)
                return ErrorResult.BadRequest("Payment request cannot be empty.");

            if (!new[] { PaymentMethod.ACH, PaymentMethod.Check }.Any(p => p == request.Method))
                return ErrorResult.BadRequest("Payment method cannot be undefined.");

            if (string.IsNullOrEmpty(request.LoanReferenceNumber))
                return ErrorResult.BadRequest("Loan reference number cannot be empty.");

            try
            {
                return new HttpOkObjectResult(Service.AddPayment(request));
            }
            catch (PaymentAlreadyExistException exception)
            {
                return new ErrorResult(409, exception.Message);
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }

        [HttpGet("/paymentstatus/{loanReferenceNumber}")]
        public IActionResult PaymentStatus(string loanReferenceNumber)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                return ErrorResult.BadRequest("Loan reference number cannot be empty.");

            try
            {
                return new HttpOkObjectResult(Service.GetPaymentStatus(loanReferenceNumber));
            }
            catch (ArgumentException exception)
            {
                return ErrorResult.BadRequest(exception.Message);
            }
        }
    }
}