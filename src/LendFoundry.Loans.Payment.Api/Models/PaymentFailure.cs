﻿namespace LendFoundry.Loans.Payment.Api.Models
{
    public class PaymentFailure
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
