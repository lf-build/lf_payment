namespace LendFoundry.Loans.Payment.Api.Models
{
    public class PaymentCancelationRequest
    {
        public string Id { get; set; }

        public string Notes { get; set; }
    }
}