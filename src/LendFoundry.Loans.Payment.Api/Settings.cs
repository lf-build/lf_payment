using System;
using LendFoundry.Foundation.Services.Settings;

namespace LendFoundry.Loans.Payment.Api
{
    public static class Settings
    {
        private const string Prefix = "LOAN_PAYMENT";

        public static string ServiceName { get; } = "loan-payment";

        public static string Nats => Environment.GetEnvironmentVariable($"{Prefix}_NATS_URL") ?? "nats://nats:4222";

        public static ServiceSettings Ach { get; } = new ServiceSettings($"{Prefix}_ACH", "ach");

        public static ServiceSettings Calendar { get; } = new ServiceSettings($"{Prefix}_CALENDAR", "calendar");

        public static ServiceSettings Configuration { get; } = new ServiceSettings($"{Prefix}_CONFIGURATION", "configuration");

        public static ServiceSettings EventHub { get; } = new ServiceSettings($"{Prefix}_EVENTHUB", "eventhub");

        public static ServiceSettings Loans { get; } = new ServiceSettings($"{Prefix}_LOANS", "loans");

        public static ServiceSettings LoanFees { get; } = new ServiceSettings($"{Prefix}_LOAN_FEES", "loan-fees");

        public static ServiceSettings Tenant { get; } = new ServiceSettings($"{Prefix}_TENANT", "tenant");

        public static DatabaseSettings Mongo { get; } = new DatabaseSettings($"{Prefix}_MONGO_CONNECTION", "mongodb://mongo", $"{Prefix}_MONGO_DATABASE", "payments");
    }
}