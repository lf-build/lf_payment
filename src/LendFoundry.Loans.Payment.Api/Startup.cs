﻿using LendFoundry.Calendar.Client;
using LendFoundry.Configuration.Client;
using LendFoundry.EventHub.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Logging;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Foundation.Services;
using LendFoundry.Loans.Client;
using LendFoundry.Loans.Fees.Client;
using LendFoundry.Loans.Payment.Ach.Events;
using LendFoundry.Loans.Payment.Ach;
using LendFoundry.Loans.Payment.Persistence;
using LendFoundry.MoneyMovement.Ach.Client;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Payment.Api
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAdaptiveTokenHandler();
            services.AddTenantTime();
            services.AddHttpServiceLogging(Settings.ServiceName);
            services.AddCalendarService(Settings.Calendar.Host, Settings.Calendar.Port);
            services.AddLoanService(Settings.Loans.Host, Settings.Loans.Port);
            services.AddTenantService(Settings.Tenant.Host, Settings.Tenant.Port);
            services.AddEventHub(Settings.EventHub.Host, Settings.EventHub.Port, Settings.Nats);
            services.AddConfigurationService<PaymentConfiguration>(Settings.Configuration.Host, Settings.Configuration.Port, Settings.ServiceName);
            services.AddFeeQueue(Settings.LoanFees.Host, Settings.LoanFees.Port);

            //TODO remove once ACH is fixed.
            //services.AddTransient<IAchServiceFactory>(p => new AchServiceFactoryAdapter(p, Settings.Ach.Host, Settings.Ach.Port));
            //services.AddTransient(p => p.GetService<IAchServiceFactory>().Create(p.GetService<ITokenReader>()));
            services.AddAchService(Settings.Ach.Host, Settings.Ach.Port);

            services.AddTransient<IPaymentServiceFactory, PaymentServiceFactory>();
            services.AddTransient<IPaymentRepository, MongoPaymentRepository>();
            services.AddTransient<IPaymentPauseRepository, MongoPaymentPauseRepository>();
            services.AddTransient<IPaymentPauseRepositoryFactory, MongoPaymentPauseRepositoryFactory>();

            services.AddTransient<IPaymentService, PaymentService>();
            services.AddTransient<IPaymentStrategyFactory, PaymentStrategyFactory>();
            services.AddTransient<IReconciliationStrategyFactory, ReconciliationStrategyFactory>();
            services.AddTransient<IAchProxy, AchProxy>();
            services.AddTransient<IAchProxyFactory, AchProxyFactory>();
            services.AddSingleton<IMongoConfiguration>(p => new MongoConfiguration(Settings.Mongo.ConnectionString, Settings.Mongo.Database));
            services.AddTransient<IPaymentRepositoryFactory, MongoPaymentRepositoryFactory>();
            services.AddTransient<IPaymentConfiguration>(p => p.GetService<IConfigurationService<PaymentConfiguration>>().Get());
            
            services.AddTransient(p =>
            {
                var currentTokenReader = p.GetService<ITokenReader>();
                var staticTokenReader = new StaticTokenReader(currentTokenReader.Read());
                return p.GetService<IEventHubClientFactory>().Create(staticTokenReader);
            }); 
            
            services.AddMvc().AddLendFoundryJsonOptions();
            services.AddCors();

            services.AddEventSubscription<AchInstructionRejected, EventSubscriptions>();
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseCors(env);
            app.UseErrorHandling();
            app.UseRequestLogging();
            app.UseHealthCheck();
            app.UseMvc();
            app.UseEventHub();
        }
    }
}
