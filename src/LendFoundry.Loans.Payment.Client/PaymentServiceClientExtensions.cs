﻿using LendFoundry.Security.Tokens;
using Microsoft.Framework.DependencyInjection;

namespace LendFoundry.Loans.Payment.Client
{
    public static class PaymentServiceClientExtensions
    {
        public static IServiceCollection AddPaymentService(this IServiceCollection services, string endpoint, int port)
        {
            services.AddTransient<IPaymentServiceClientFactory>(p=> new PaymentServiceClientFactory(p, endpoint, port));
            services.AddTransient(p => p.GetService<IPaymentServiceClientFactory>().Create(p.GetService<ITokenReader>()));
            return services;
        }
    }
}