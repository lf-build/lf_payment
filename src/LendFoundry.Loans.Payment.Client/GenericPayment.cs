﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.Payment.Client
{
    internal class GenericPayment : Aggregate, IPayment
    {
        public int Attempts { get; set; }
        public PaymentStatus Status { get; set; }
        public string LoanReferenceNumber { get; set; }
        public PaymentMethod Method { get; set; }
        public PaymentType Type { get; set; }
        public string Code { get; set; }
        public DateTimeOffset DueDate { get; set; }
        public double Amount { get; set; }
        public TimeBucket StatusUpdated { get; set; }
        public string FailureCode { get; set; }
        public string FailureDescription { get; set; }
    }
}
