﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Services;
using RestSharp;
using System.Linq;

namespace LendFoundry.Loans.Payment.Client
{
    public class PaymentServiceClient : IPaymentService
    {
        public PaymentServiceClient(IServiceClient client)
        {
            Client = client;
        }

        private IServiceClient Client { get; }

        public async Task<IEnumerable<IPayment>> GetAllPayments(string loanReferenceNumber)
        {
            if (string.IsNullOrWhiteSpace(loanReferenceNumber))
                throw new ArgumentException($"{loanReferenceNumber} is null or empty");

            var request = new RestRequest("/{loanReferenceNumber}", Method.GET);
            request.AddUrlSegment("loanReferenceNumber", loanReferenceNumber);
            /*
            IMPORTANT:
                Do not use the Payment class as a type parameter for the response because
                that class is abstract which means it cannot be used to deserialize the response.
            */
            return await Client.ExecuteAsync<GenericPayment[]>(request);
        }
        
        public async Task<IEnumerable<IPayment>> GetPaymentsWithStatus(PaymentStatus status, DateTimeOffset statusUpdated)
        {
            if (statusUpdated == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(statusUpdated)}` is either empty or invalid");

            var request = new RestRequest("/status/{status}/{statusDate}", Method.GET);

            request.AddUrlSegment("status", status.ToString());
            request.AddUrlSegment("statusDate", statusUpdated.ToString("yyyy-MM-dd"));

            /*
            IMPORTANT:
                Do not use the Payment class as a type parameter for the response because
                that class is abstract which means it cannot be used to deserialize the response.
            */
            return await Client.ExecuteAsync<GenericPayment[]>(request);
        }
        
        public async Task<IEnumerable<IPayment>> GetPaymentsWithStatus(PaymentStatus status, DateTimeOffset statusUpdated, PaymentMethod method)
        {
            if (statusUpdated == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(statusUpdated)}` is either empty or invalid");

            var request = new RestRequest("/status/{status}/{statusDate}/{method}", Method.GET);

            request.AddUrlSegment("status", status.ToString());
            request.AddUrlSegment("statusDate", statusUpdated.ToString("yyyy-MM-dd"));
            request.AddUrlSegment("method", method.ToString());

            /*
            IMPORTANT:
                Do not use the Payment class as a type parameter for the response because
                that class is abstract which means it cannot be used to deserialize the response.
            */
            return await Client.ExecuteAsync<GenericPayment[]>(request);
        }

        public async Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date)
        {
            if (date == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(date)}` is either empty or invalid");

            var request = new RestRequest("/due-on/{date}", Method.GET);

            request.AddUrlSegment("date", date.ToString("yyyy-MM-dd"));

            /*
            IMPORTANT:
                Do not use the Payment class as a type parameter for the response because
                that class is abstract which means it cannot be used to deserialize the response.
            */
            return await Client.ExecuteAsync<GenericPayment[]>(request);
        }

        public async Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentMethod method)
        {
            if (date == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(date)}` is either empty or invalid");

            if (method == PaymentMethod.Undefined)
                throw new ArgumentException($"Argument for parameter `{nameof(method)}` cannot be {method}");

            var request = new RestRequest("/due-on/{date}/method/{method}", Method.GET);

            request.AddUrlSegment("date", date.ToString("yyyy-MM-dd"));
            request.AddUrlSegment("method", method.ToString());

            /*
            IMPORTANT:
                Do not use the Payment class as a type parameter for the response because
                that class is abstract which means it cannot be used to deserialize the response.
            */
            return await Client.ExecuteAsync<GenericPayment[]>(request);
        }

        public async Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentStatus status)
        {
            if (date == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(date)}` is either empty or invalid");

            var request = new RestRequest("/due-on/{date}/status/{status}", Method.GET);

            request.AddUrlSegment("date", date.ToString("yyyy-MM-dd"));
            request.AddUrlSegment("status", status.ToString());

            /*
            IMPORTANT:
                Do not use the Payment class as a type parameter for the response because
                that class is abstract which means it cannot be used to deserialize the response.
            */
            return await Client.ExecuteAsync<GenericPayment[]>(request);
        }

        public async Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentMethod method, PaymentStatus status)
        {
            if (date == default(DateTimeOffset))
                throw new ArgumentException($"Argument for parameter `{nameof(date)}` is either empty or invalid");

            if (method == PaymentMethod.Undefined)
                throw new ArgumentException($"Argument for parameter `{nameof(method)}` cannot be {method}");

            var request = new RestRequest("/due-on/{date}/method/{method}/status/{status}", Method.GET);

            request.AddUrlSegment("date", date.ToString("yyyy-MM-dd"));
            request.AddUrlSegment("method", method.ToString());
            request.AddUrlSegment("status", status.ToString());

            /*
            IMPORTANT:
                Do not use the Payment class as a type parameter for the response because
                that class is abstract which means it cannot be used to deserialize the response.
            */
            return await Client.ExecuteAsync<GenericPayment[]>(request);
        }

        public IEnumerable<IPayment> AddPayment(IPaymentRequest paymentRequest)
        {
            if (paymentRequest == null)
                throw new ArgumentNullException(nameof(paymentRequest));

            return HandleResponse(() =>
            {
                var request = new RestRequest("/{method}", Method.PUT);
                request.AddUrlSegment("method", paymentRequest.Method.ToString().ToLower());
                request.AddJsonBody(paymentRequest);

                /*
                IMPORTANT:
                    Do not use the Payment class as a type parameter for the response because
                    that class is abstract which means it cannot be used to deserialize the response.
                */
                return Client.Execute<GenericPayment[]>(request);
            });
        }

        public void CancelPayment(string id, string notes)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException(nameof(id));

            HandleResponse(() =>
            {
                var request = new RestRequest("/", Method.DELETE);
                
                request.AddUrlSegment("id", id);
                Client.Execute(request);
            });
        }

        public void FailPayment(string id, string failureCode, string failureDescription)
        {
            if (string.IsNullOrWhiteSpace(id))
                throw new ArgumentNullException(nameof(id));

            HandleResponse(() =>
            {
                var request = new RestRequest("/{id}/fail", Method.PUT);
                
                request.AddUrlSegment("id", id);
                request.AddJsonBody(new
                {
                    Code = failureCode,
                    Description = failureDescription
                });

                Client.Execute(request);
            });
        }
        
        public Task<IEnumerable<IPayment>> RetryPayment(string paymentId, DateTimeOffset dueDate, BankAccount bankAccount)
        {
            if (string.IsNullOrWhiteSpace(paymentId))
                throw new ArgumentNullException(nameof(paymentId));

            if (dueDate == default(DateTimeOffset))
                throw new ArgumentNullException(nameof(paymentId));

            try
            {
                var request = new RestRequest("/{paymentId}/retry/{dueDate}", Method.PUT);
                
                request.AddUrlSegment("paymentId", paymentId);
                request.AddUrlSegment("dueDate", dueDate.ToString("yyyy-MM-dd"));
                
                if (bankAccount != null)
                {
                    request.AddJsonBody(bankAccount);
                }

                return Task.Run(() => Client.Execute<GenericPayment[]>(request).Select(p => p as IPayment));
            }
            catch (ClientException exception)
            {
                throw HandleError(exception);
            }
        }

        public void ReconcilePendingPayments(PaymentMethod method, DateTime dueDate)
        {
            HandleResponse(() =>
            {
                var request = new RestRequest("/reconcile/{method}/{dueDate}", Method.POST);
                request.AddUrlSegment(nameof(method), method.ToString());
                request.AddUrlSegment(nameof(dueDate), dueDate.ToString("yyyy-MM-dd"));
                Client.Execute(request);
            });
        }

        public async Task Resume(string loanReferenceNumber)
        {
            var request = new RestRequest("ach/{loanReferenceNumber}/pause", Method.DELETE);
            request.AddUrlSegment("loanReferenceNumber", loanReferenceNumber);
            await Client.ExecuteAsync(request);
        }

        public async Task<IEnumerable<IPaymentPause>> GetPauses()
        {
            var request = new RestRequest("ach/pauses", Method.GET);
            return new List<IPaymentPause>(await Client.ExecuteAsync<List<PaymentPause>>(request));
        }

        public async Task<IPaymentPause> GetPauseByLoanReferenceNumber(string loanReferenceNumber)
        {
            var request = new RestRequest("ach/{loanReferenceNumber}/pause", Method.GET);
            request.AddUrlSegment("loanReferenceNumber", loanReferenceNumber);
            return await Client.ExecuteAsync<PaymentPause>(request);
        }

        public async Task<IPaymentPause> Pause(string loanReferenceNumber, IPaymentPauseRequest paymentPauseRequest)
        {
            var request = new RestRequest("ach/{loanReferenceNumber}/pause", Method.POST);
            request.AddUrlSegment("loanReferenceNumber", loanReferenceNumber);
            request.AddJsonBody(paymentPauseRequest);
            return await Client.ExecuteAsync<PaymentPause>(request);
        }

        public void HandleAchRejection(string instructionId, string rejectionReason, string description)
        {
            throw new NotSupportedException();
        }

        private static void HandleResponse(Action expression)
        {
            try
            {
                expression();
            }
            catch (ClientException exception)
            {
                throw HandleError(exception);
            }
        }

        private static T HandleResponse<T>(Func<T> expression)
        {
            try
            {
                return expression();
            }
            catch (ClientException exception)
            {
                throw HandleError(exception);
            }
        }

        private static Exception HandleError(ClientException exception)
        {
            var error = exception.Error;
            if (error == null)
                return exception;

            switch (error.Code)
            {
                case 404:
                    return new NotFoundException(error.Message);
                case 409:
                    return new PaymentAlreadyExistException(error.Message);
                case 400:
                    return new ArgumentException(error.Message);
                default:
                    return exception;
            }
        }

        public PaymentStatus GetPaymentStatus(string loanReferenceNumber)
        {
            throw new NotImplementedException();
        }
    }
}
