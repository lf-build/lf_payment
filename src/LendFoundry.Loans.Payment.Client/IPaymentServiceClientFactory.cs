using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Payment.Client
{
    public interface IPaymentServiceClientFactory
    {
        IPaymentService  Create(ITokenReader reader);
    }
}