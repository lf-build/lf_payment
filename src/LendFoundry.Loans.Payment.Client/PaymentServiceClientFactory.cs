﻿using System;
using LendFoundry.Foundation.Services;
using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Payment.Client
{
    public class PaymentServiceClientFactory : IPaymentServiceClientFactory
    {
        public PaymentServiceClientFactory(IServiceProvider provider, string endpoint, int port)
        {
            Provider = provider;
            Endpoint = endpoint;
            Port = port;
        }

        private IServiceProvider Provider { get; }

        private string Endpoint { get; }

        private int Port { get; }

        public IPaymentService Create(ITokenReader reader)
        {
            return new PaymentServiceClient(Provider.GetServiceClient(reader, Endpoint, Port));
        }
    }
}