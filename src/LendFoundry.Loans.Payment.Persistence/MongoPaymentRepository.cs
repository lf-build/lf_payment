using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using MongoDB.Driver.Linq;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Linq;
using System.Threading.Tasks;
using System;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.Payment.Persistence
{
    public class MongoPaymentRepository : MongoRepository<IPayment, Payment>, IPaymentRepository
    {
        static MongoPaymentRepository()
        {
            BsonClassMap.RegisterClassMap<Payment>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.Method).SetSerializer(new EnumSerializer<PaymentMethod>(BsonType.String));
                map.MapMember(p => p.Status).SetSerializer(new EnumSerializer<PaymentStatus>(BsonType.String));
                map.MapMember(p => p.Type).SetSerializer(new EnumSerializer<PaymentType>(BsonType.String));
                map.MapMember(p => p.DueDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(Payment);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

            BsonClassMap.RegisterClassMap<AchPayment>(map =>
            {
                map.AutoMap();

                var type = typeof(AchPayment);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<CheckPayment>(map =>
            {
                map.AutoMap();

                var type = typeof(CheckPayment);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });

            BsonClassMap.RegisterClassMap<TimeBucket>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.Time).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(TimeBucket);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
            });
        }

        public MongoPaymentRepository
            (
                ITenantService tenantService, 
                IMongoConfiguration configuration,
                ITenantTime tenantTime
            ) :base(tenantService, configuration, "payments")
        {
            TenantTime = tenantTime;
        }

        private ITenantTime TenantTime { get; }

        public IEnumerable<T> All<T>(Expression<Func<T, bool>> query) where T : IPayment
        {
            return Collection.AsQueryable().OfType<T>().Where(query);
        }

        public void Add(IEnumerable<IPayment> payments)
        {
            if (payments == null)
                throw new ArgumentNullException(nameof(payments));

            var entries = new List<IPayment>(payments);
            entries.ForEach(p =>
            {
                p.Id = ObjectId.GenerateNewId().ToString();
                p.TenantId = TenantService.Current.Id;
            });

            Collection.InsertMany(entries);
        }

        public void Update(IEnumerable<IPayment> payments)
        {
            var bulk = new List<WriteModel<IPayment>>();
            foreach (var payment in payments)
            {
                var filter = new ExpressionFilterDefinition<IPayment>(p =>
                    p.TenantId == TenantService.Current.Id &&
                    p.TenantId == payment.TenantId &&
                    p.Id == payment.Id);
                bulk.Add(new ReplaceOneModel<IPayment>(filter, payment));
            }
            Collection.BulkWrite(bulk);
        }

        public async Task<IEnumerable<IPayment>> MarkAsReceived(Expression<Func<IPayment, bool>> query)
        {
            var payments = await base.All(query);
            var filter = new ExpressionFilterDefinition<IPayment>(query);
            var now = new TimeBucket(TenantTime.Now);
            var update = new UpdateDefinitionBuilder<IPayment>()
                .Set(p => p.Status, PaymentStatus.Received)
                .Set(p => p.StatusUpdated, now);
            var result = await Collection.UpdateManyAsync(filter, update);
            payments.ToList().ForEach(p =>
            {
                p.Status = PaymentStatus.Received;
                p.StatusUpdated = now;
            });
            return payments;
        }

        public PaymentStatus GetStatus(string loanReferenceNumber)
        {
            if (string.IsNullOrEmpty(loanReferenceNumber))
                throw new ArgumentException(nameof(loanReferenceNumber));

            return Query.OrderByDescending(a => a.DueDate).FirstOrDefault(q => q.LoanReferenceNumber == loanReferenceNumber).Status;
        }
    }
}