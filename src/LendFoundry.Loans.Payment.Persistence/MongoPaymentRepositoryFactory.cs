using System;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using LendFoundry.Foundation.Date;
using LendFoundry.Configuration.Client;

namespace LendFoundry.Loans.Payment.Persistence
{
    public class MongoPaymentRepositoryFactory : IPaymentRepositoryFactory
    {
        public MongoPaymentRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; set; }

        public IPaymentRepository Create(ITokenReader reader)
        {
            var configuration = Provider.GetService<IMongoConfiguration>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var configFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var tenantTime = tenantTimeFactory.Create(configFactory, reader);
            return new MongoPaymentRepository(tenantService, configuration, tenantTime);
        }
    }
}