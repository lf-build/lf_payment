﻿using LendFoundry.Configuration.Client;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Security.Tokens;
using LendFoundry.Tenant.Client;
using Microsoft.Framework.DependencyInjection;
using System;

namespace LendFoundry.Loans.Payment.Persistence
{
    public class MongoPaymentPauseRepositoryFactory : IPaymentPauseRepositoryFactory
    {
        public MongoPaymentPauseRepositoryFactory(IServiceProvider provider)
        {
            Provider = provider;
        }

        private IServiceProvider Provider { get; set; }

        public IPaymentPauseRepository Create(ITokenReader reader)
        {
            var configuration = Provider.GetService<IMongoConfiguration>();
            var configurationFactory = Provider.GetService<IConfigurationServiceFactory>();
            var tenantServiceFactory = Provider.GetService<ITenantServiceFactory>();
            var tenantService = tenantServiceFactory.Create(reader);
            var tenantTimeFactory = Provider.GetService<ITenantTimeFactory>();
            var tenantTime = tenantTimeFactory.Create(configurationFactory, reader);
            return new MongoPaymentPauseRepository(tenantService, configuration, tenantTime);
        }
    }
}
