using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence.Mongo;
using LendFoundry.Tenant.Client;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;

namespace LendFoundry.Loans.Payment.Persistence
{
    public class MongoPaymentPauseRepository : MongoRepository<IPaymentPause, PaymentPause>, IPaymentPauseRepository
    {
        static MongoPaymentPauseRepository()
        {
            BsonClassMap.RegisterClassMap<PaymentPause>(map =>
            {
                map.AutoMap();
                map.MapMember(p => p.EndDate).SetIgnoreIfNull(true);
                map.MapMember(p => p.EndDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));
                map.MapMember(p => p.StartDate).SetSerializer(new DateTimeOffsetSerializer(BsonType.Document));

                var type = typeof(PaymentPause);
                map.SetDiscriminator($"{type.FullName}, {type.Assembly.GetName().Name}");
                map.SetIsRootClass(true);
            });

        }

        public MongoPaymentPauseRepository(ITenantService tenantService, IMongoConfiguration configuration, ITenantTime tenantTime) : base(tenantService, configuration, "payment-pauses")
        {
            TenantTime = tenantTime;

            CreateIndexIfNotExists("payment_pause_referenceNumber", Builders<IPaymentPause>.IndexKeys.Ascending(i => i.LoanReferenceNumber));
            CreateIndexIfNotExists("payment_pause_startDate", Builders<IPaymentPause>.IndexKeys.Ascending(i => i.StartDate));
            CreateIndexIfNotExists("payment_pause_endDate", Builders<IPaymentPause>.IndexKeys.Ascending(i => i.EndDate));
        }

        private ITenantTime TenantTime { get; }

        public async Task<IPaymentPause> AddOrReplace(IPaymentPause paymentPause)
        {
            paymentPause.Id = ObjectId.GenerateNewId().ToString();
            paymentPause.TenantId = TenantService.Current.Id;
            await Collection.ReplaceOneAsync(p =>
                    p.TenantId == TenantService.Current.Id &&
                    p.LoanReferenceNumber == paymentPause.LoanReferenceNumber,
                paymentPause, new UpdateOptions { IsUpsert = true });
            return paymentPause;
        }

        public async Task Remove(string referenceNumber)
        {
            await Collection.DeleteManyAsync(p =>
                p.TenantId == TenantService.Current.Id &&
                p.LoanReferenceNumber == referenceNumber);
        }

        public async Task<List<IPaymentPause>> Get()
        {
            return await Collection.Find(p =>
                p.TenantId == TenantService.Current.Id &&
                p.StartDate <= TenantTime.Today &&
                (p.EndDate == default(DateTimeOffset) || p.EndDate >= TenantTime.Today)).ToListAsync();
        }

        public async Task<IPaymentPause> GetByLoanReferenceNumber(string loanReferenceNumber)
        {
            return await Collection.Find(p =>
                p.TenantId == TenantService.Current.Id &&
                p.LoanReferenceNumber == loanReferenceNumber &&
                p.StartDate <= TenantTime.Today &&
                (p.EndDate == default(DateTimeOffset) || p.EndDate >= TenantTime.Today)).FirstOrDefaultAsync();
        }
    }
}