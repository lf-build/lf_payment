using System;

namespace LendFoundry.Loans.Payment
{
    public class AchPaymentRequest : IAchPaymentRequest
    {
        public string LoanReferenceNumber { get; set; }

        public PaymentMethod Method { get; set; }

        public PaymentType Type { get; set; }

        public string Code { get; set; }

        public DateTimeOffset DueDate { get; set; }

        public double Amount { get; set; }

        public string AccountHolder { get; set; }

        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public BankAccountType AccountType { get; set; }
    }
}