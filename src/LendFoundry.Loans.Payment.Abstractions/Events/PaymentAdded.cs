using System;

namespace LendFoundry.Loans.Payment.Events
{
    public class PaymentAdded
    {
        public PaymentAdded()
        {
        }

        public PaymentAdded(IPayment payment)
        {
            PaymentId = payment.Id;
            LoanReferenceNumber = payment.LoanReferenceNumber;
            Method = payment.Method;
            Type = payment.Type;
            DueDate = payment.DueDate;
            Code = payment.Code;
            Amount = payment.Amount;
            Status = payment.Status;
            Attempts = payment.Attempts;
        }

        public string PaymentId { get; set; }

        public string Code { get; set; }

        public string LoanReferenceNumber { get; set; }

        public PaymentMethod Method { get; set; }

        public PaymentType Type { get; set; }

        public DateTimeOffset DueDate { get; set; }

        public double Amount { get; set; }

        public int Attempts { get; set; }

        public PaymentStatus Status { get; set; }
    }
}