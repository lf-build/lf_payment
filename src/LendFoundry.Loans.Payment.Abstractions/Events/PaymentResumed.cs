namespace LendFoundry.Loans.Payment.Events
{
    public class PaymentResumed
    {
        public PaymentResumed()
        {

        }

        public PaymentResumed(string referenceNumber)
        {
            ReferenceNumber = referenceNumber;
        }

        public string ReferenceNumber { get; set; }
    }
}