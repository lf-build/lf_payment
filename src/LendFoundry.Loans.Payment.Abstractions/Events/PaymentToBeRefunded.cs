using System;

namespace LendFoundry.Loans.Payment.Events
{
    public class PaymentToBeRefunded
    {
        public double RefundAmount { get; set; }
        public PaymentMethod Method { get; set; }
        public double CheckAmount { get; set; }
        public PaymentType Type { get; set; }
        public double PayoffAmount { get; set; }
        public DateTimeOffset PaymentDate { get; set; }
        public string LoanReferenceNumber { get; set; }
    }
}