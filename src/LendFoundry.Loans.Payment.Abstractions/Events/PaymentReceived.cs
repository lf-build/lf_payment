namespace LendFoundry.Loans.Payment.Events
{
    public class PaymentReceived : PaymentAdded
    {
        public PaymentReceived()
        {
        }

        public PaymentReceived(IPayment payment) : base(payment)
        {
        }
    }
}