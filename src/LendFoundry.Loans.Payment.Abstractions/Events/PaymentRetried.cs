namespace LendFoundry.Loans.Payment.Events
{
    public class PaymentRetried : PaymentAdded
    {
        public PaymentRetried()
        {
        }

        public PaymentRetried(IPayment payment) : base(payment)
        {
        }
    }
}