namespace LendFoundry.Loans.Payment.Events
{
    public class PaymentCancelled : PaymentAdded
    {
        public PaymentCancelled()
        {
        }

        public PaymentCancelled(IPayment payment, string notes) : base(payment)
        {
            Notes = notes;
        }

        public string Notes { get; set; }
    }
}