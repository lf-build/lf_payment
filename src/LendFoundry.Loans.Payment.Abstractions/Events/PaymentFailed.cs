namespace LendFoundry.Loans.Payment.Events
{
    public class PaymentFailed : PaymentAdded
    {
        public PaymentFailed()
        {
        }

        public PaymentFailed(IPayment payment) : base(payment)
        {
        }
    }
}