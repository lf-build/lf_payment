using System;

namespace LendFoundry.Loans.Payment.Events
{
    public class PaymentPaused
    {
        public PaymentPaused()
        {

        }

        public PaymentPaused(IPaymentPause paymentPause)
        {
            ReferenceNumber = paymentPause.LoanReferenceNumber;
            StartDate = paymentPause.StartDate;
            EndDate = paymentPause.EndDate;
            Notes = paymentPause.Notes;
        }

        public string ReferenceNumber { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public string Notes { get; set; }
    }
}