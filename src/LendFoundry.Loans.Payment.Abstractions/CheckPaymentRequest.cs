﻿using System;

namespace LendFoundry.Loans.Payment
{
    public class CheckPaymentRequest : ICheckPaymentRequest
    {
        public string LoanReferenceNumber { get; set; }

        public PaymentMethod Method { get; set; }

        public PaymentType Type { get; set; }

        public string Code { get; set; }

        public DateTimeOffset DueDate { get; set; }

        public double Amount { get; set; }

        public string RoutingNumber { get; set; }

        public string CheckNumber { get; set; }

        public double CheckAmount { get; set; }

        public double FeeAmount { get; set; }

        public double BalanceAmount { get; set; }

        public string Notes { get; set; }
    }
}