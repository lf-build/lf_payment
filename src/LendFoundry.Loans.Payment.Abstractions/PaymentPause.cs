using System;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Loans.Payment
{
    public class PaymentPause : Aggregate, IPaymentPause
    {
        public string LoanReferenceNumber { get; set; }
        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset EndDate { get; set; }
        public string Notes { get; set; }
    }
}