﻿using System;

namespace LendFoundry.Loans.Payment
{
    public class CheckPayment : Payment, ICheckPayment
    {
        public CheckPayment(IPaymentRequest request) : base(request)
        {
            var checkRequest = request as ICheckPaymentRequest;
            if (checkRequest == null)
                throw new ArgumentNullException(nameof(request));

            RoutingNumber = checkRequest.RoutingNumber;

            CheckNumber = checkRequest.CheckNumber;
            CheckAmount = checkRequest.CheckAmount;
            FeeAmount = checkRequest.FeeAmount;

            Code = checkRequest.Code;

            Notes = checkRequest.Notes;
            Amount = checkRequest.Amount;
            Type = checkRequest.Type;
        }

        public CheckPayment(IPaymentRequest request, double amount) : this(request)
        {
            Amount = amount;
        }

        public CheckPayment(IPaymentRequest request, double amount, PaymentType type) : this(request, amount)
        {
            Type = type;
        }

        public CheckPayment(IPaymentRequest request, double amount, PaymentType type, string code) : this(request, amount, type)
        {
            Code = code;
        }

        public string RoutingNumber { get; set; }

        public string CheckNumber { get; set; }

        public double CheckAmount { get; set; }

        public double FeeAmount { get; set; }

        public string Notes { get; set; }
    }
}