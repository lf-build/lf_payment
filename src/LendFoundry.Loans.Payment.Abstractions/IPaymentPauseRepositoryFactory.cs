﻿using LendFoundry.Security.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentPauseRepositoryFactory
    {
        IPaymentPauseRepository Create(ITokenReader reader);
    }
}
