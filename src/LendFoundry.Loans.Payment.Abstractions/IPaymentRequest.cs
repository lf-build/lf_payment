using System;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentRequest
    {
        string LoanReferenceNumber { get; set; }

        PaymentMethod Method { get; set; }

        PaymentType Type { get; set;  }

        string Code { get; set; }

        DateTimeOffset DueDate { get; set; }

        double Amount { get; set; }
    }
}