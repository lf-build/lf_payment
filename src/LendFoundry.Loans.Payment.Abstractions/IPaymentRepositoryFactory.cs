using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentRepositoryFactory
    {
        IPaymentRepository Create(ITokenReader reader);
    }
}