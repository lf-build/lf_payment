using System;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentPause : IAggregate
    {
        string LoanReferenceNumber { get; set; }
        DateTimeOffset StartDate { get; set; }
        DateTimeOffset EndDate { get; set; }
        string Notes { get; set; }
    }
}