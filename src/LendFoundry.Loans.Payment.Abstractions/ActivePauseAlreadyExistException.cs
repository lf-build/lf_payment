using System;

namespace LendFoundry.Loans.Payment
{
    public class ActivePauseAlreadyExistException : Exception
    {
        public ActivePauseAlreadyExistException()
        {
        }

        public ActivePauseAlreadyExistException(string message) : base(message)
        {
        }

    }
}