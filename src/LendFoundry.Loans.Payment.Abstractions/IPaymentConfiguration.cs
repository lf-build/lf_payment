namespace LendFoundry.Loans.Payment
{
    public interface IPaymentConfiguration
    {
        int MaxNumberOfAttempts { get; set; }

        int AttemptIntervalInDays { get; set; }

        string CheckProcessingFeeCode { get; set; }

        double CheckProcessingFeeAmount { get; set; }

        string RetryEntryDescription { get; set; }

        string LateFeeCode { get; set; }

        string ReturnFeeCode { get; set; }

        string LateFeeEntryDescription { get; set; }

        string ReturnFeeEntryDescription { get; set; }

        string LateFeeInvestorId { get; set; }

        string ReturnFeeInvestorId { get; set; }

        string AcceptedAchRejectionCodeForPaymentFailures { get; set; }
    }
}