namespace LendFoundry.Loans.Payment
{
    public abstract class PaymentRejection : IPaymentRejection
    {
        public IPayment Payment { get; set; }
    }
}