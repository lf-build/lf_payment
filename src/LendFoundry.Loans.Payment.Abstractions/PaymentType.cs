namespace LendFoundry.Loans.Payment
{
    public enum PaymentType
    {
        Fee,
        Scheduled,
        Extra,
        Payoff
    }
}