﻿namespace LendFoundry.Loans.Payment
{
    public enum PaymentStatus
    {
        Pending,
        Received,
        Failed,
        Cancelled
    }
}