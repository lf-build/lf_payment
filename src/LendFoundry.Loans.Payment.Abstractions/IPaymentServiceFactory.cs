using LendFoundry.Security.Tokens;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentServiceFactory
    {
        IPaymentService Create(ITokenReader reader);
    }
}