using System;

namespace LendFoundry.Loans.Payment
{
    public class PaymentAlreadyExistException : ArgumentException
    {
        public PaymentAlreadyExistException(string message) : base(message)
        {
            
        }
    }
}