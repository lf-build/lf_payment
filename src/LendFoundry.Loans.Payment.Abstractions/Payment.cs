using System;
using LendFoundry.Foundation.Persistence;
using LendFoundry.Foundation.Date;

namespace LendFoundry.Loans.Payment
{
    public abstract class Payment : Aggregate, IPayment
    {
        protected Payment(IPaymentRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));

            LoanReferenceNumber = request.LoanReferenceNumber;
            Method = request.Method;
            Type = request.Type;
            DueDate = request.DueDate;
            Amount = request.Amount;
            Status = PaymentStatus.Pending;
            Attempts = 1;
        }

        public string LoanReferenceNumber { get; set; }

        public PaymentMethod Method { get; set; }

        public PaymentType Type { get; set; }

        public string Code { get; set; }

        public DateTimeOffset DueDate { get; set; }

        public double Amount { get; set; }

        public int Attempts { get; set; }

        public PaymentStatus Status { get; set; }

        public TimeBucket StatusUpdated { get; set; }

        public string FailureCode { get; set; }

        public string FailureDescription { get; set; }
    }
}