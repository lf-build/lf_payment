﻿namespace LendFoundry.Loans.Payment
{
    public class BankAccount
    {
        public BankAccountType AccountType { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
    }
}
