﻿namespace LendFoundry.Loans.Payment
{
    public interface ICheckPayment : IPayment, ICheckPaymentRequest
    {
        
    }
}