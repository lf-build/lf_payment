using System;

namespace LendFoundry.Loans.Payment
{
    public class PaymentPauseRequest : IPaymentPauseRequest
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Notes { get; set; }
    }
}