using System;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentPauseRequest
    {
        DateTime? EndDate { get; set; }
        string Notes { get; set; }
        DateTime StartDate { get; set; }
    }
}