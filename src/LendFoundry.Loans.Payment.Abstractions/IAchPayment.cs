namespace LendFoundry.Loans.Payment
{
    public interface IAchPayment : IPayment, IAchPaymentRequest
    {
        string InstructionId { get; set; }

        string InvestorId { get; set; }
    }
}