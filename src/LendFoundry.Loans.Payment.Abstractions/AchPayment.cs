using System;

namespace LendFoundry.Loans.Payment
{
    public class AchPayment : Payment, IAchPayment
    {
        public AchPayment(IPaymentRequest request) : base(request)
        {
            var achRequest = request as IAchPaymentRequest;
            if (achRequest == null)
                throw new ArgumentNullException(nameof(request));

            AccountHolder = achRequest.AccountHolder;
            RoutingNumber = achRequest.RoutingNumber;
            AccountNumber = achRequest.AccountNumber;
            AccountType = achRequest.AccountType;
        }

        public AchPayment(IPaymentRequest request, double amount) : this(request)
        {
            Amount = amount;
        }

        public AchPayment(IPaymentRequest request, double amount, PaymentType type) : this(request, amount)
        {
            Type = type;
        }

        public AchPayment(IPaymentRequest request, double amount, PaymentType type, string code) : this(request, amount, type)
        {
            Code = code;
        }

        public AchPayment(IPaymentRequest request, string investorId) : this(request)
        {
            InvestorId = investorId;
        }

        public AchPayment(IAchPayment payment, string investorId) : this(payment)
        {
            InvestorId = investorId;
            Attempts = payment.Attempts;
        }

        public AchPayment(IPaymentRequest request, double amount, PaymentType type, string code, string investorId) : this(request, amount, type, code)
        {
            InvestorId = investorId;
        }

        public AchPayment(IPaymentRequest request, double amount, string investorId) : this(request, amount)
        {
            InvestorId = investorId;
        }

        public string AccountHolder { get; set; }

        public string RoutingNumber { get; set; }

        public string AccountNumber { get; set; }

        public BankAccountType AccountType { get; set; }

        public string InstructionId { get; set; }

        public string InvestorId { get; set; }
    }
}