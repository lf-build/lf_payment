﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentService
    {
        IEnumerable<IPayment> AddPayment(IPaymentRequest request);
        Task<IEnumerable<IPayment>> RetryPayment(string paymentId, DateTimeOffset dueDate, BankAccount bankAccount);
        void CancelPayment(string id, string notes);
        void ReconcilePendingPayments(PaymentMethod method, DateTime dueDate);
        Task Resume(string loanReferenceNumber);
        Task<IEnumerable<IPaymentPause>> GetPauses();
        Task<IPaymentPause> GetPauseByLoanReferenceNumber(string loanReferenceNumber);
        Task<IPaymentPause> Pause(string loanReferenceNumber, IPaymentPauseRequest paymentPauseRequest);
        Task<IEnumerable<IPayment>> GetAllPayments(string loanReferenceNumber);
        void FailPayment(string paymentId, string failureCode, string failureDescription);
        void HandleAchRejection(string instructionId, string rejectionCode, string description);
        Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date);
        Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentMethod method);
        Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentStatus status);
        Task<IEnumerable<IPayment>> GetPaymentsDueOn(DateTimeOffset date, PaymentMethod method, PaymentStatus status);
        Task<IEnumerable<IPayment>> GetPaymentsWithStatus(PaymentStatus status, DateTimeOffset statusUpdated);
        Task<IEnumerable<IPayment>> GetPaymentsWithStatus(PaymentStatus status, DateTimeOffset statusUpdated, PaymentMethod method);
        PaymentStatus GetPaymentStatus(string loanReferenceNumber);
    }
}