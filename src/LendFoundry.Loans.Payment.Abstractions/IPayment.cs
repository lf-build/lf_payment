﻿using LendFoundry.Foundation.Date;
using LendFoundry.Foundation.Persistence;
using System;

namespace LendFoundry.Loans.Payment
{
    public interface IPayment : IPaymentRequest, IAggregate
    {
        int Attempts { get; set; }
        
        PaymentStatus Status { get; set; }

        TimeBucket StatusUpdated { get; set; }

        string FailureCode { get; set; }

        string FailureDescription { get; set; }
    }
}