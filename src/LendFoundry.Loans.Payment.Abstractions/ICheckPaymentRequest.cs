namespace LendFoundry.Loans.Payment
{
    public interface ICheckPaymentRequest : IPaymentRequest
    {
        string RoutingNumber { get; set; }

        string CheckNumber { get; set; }

        double CheckAmount { get; set; }

        double FeeAmount { get; set; }

        string Notes { get; set; }
    }
}