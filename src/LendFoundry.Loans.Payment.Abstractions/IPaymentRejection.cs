namespace LendFoundry.Loans.Payment
{
    public interface IPaymentRejection
    {
        IPayment Payment { get; set; }
    }
}