namespace LendFoundry.Loans.Payment
{
    public interface IAchPaymentRequest : IPaymentRequest
    {
        string AccountHolder { get; set; }

        string RoutingNumber { get; set; }

        string AccountNumber { get; set; }
        
        BankAccountType AccountType { get; set; }
    }
}