﻿using System;
using System.Collections.Generic;

namespace LendFoundry.Loans.Payment
{
    public class PredicatedValue<SUBJECT, VALUE>
    {
        public PredicatedValue(Func<SUBJECT, VALUE> valueFn, Func<SUBJECT, bool> predicate)
        {
            ValueFn = valueFn;
            Predicates = predicate;
        }

        public PredicatedValue(VALUE value, Func<SUBJECT, bool> predicate)
            : this(s => value, predicate)
        {
        }

        public PredicatedValue(Func<SUBJECT, VALUE> defaultValueFn)
            : this(defaultValueFn, s => true)
        {
        }

        public Func<SUBJECT, bool> Predicates { get; }
        public Func<SUBJECT, VALUE> ValueFn { get; }
        
        public static V ValueFor<S, V>(S subject, IEnumerable<PredicatedValue<S, V>> predicativeValues)
        {
            foreach (var pv in predicativeValues)
                if (pv.Predicates(subject))
                    return pv.ValueFn(subject);

            return default(V);
        }

        public static PredicatedValue<S, V> Default<S, V>(Func<S, V> defaultValueFn)
        {
            return new PredicatedValue<S, V>(defaultValueFn);
        }
    }

    public class PredicatedValues<SUBJECT, VALUE>
    {
        private IEnumerable<PredicatedValue<SUBJECT, VALUE>> Values { get; }

        public PredicatedValues(params PredicatedValue<SUBJECT, VALUE>[] values)
        {
            Values = values;
        }

        public VALUE ValueFor(SUBJECT subject)
        {
            return PredicatedValue<SUBJECT, VALUE>.ValueFor(subject, Values);
        }
    }
}
