using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentRepository : IRepository<IPayment>
    {
        void Add(IEnumerable<IPayment> payments);

        Task<IEnumerable<IPayment>> MarkAsReceived(Expression<Func<IPayment, bool>> query);

        IEnumerable<T> All<T>(Expression<Func<T, bool>> query) where T : IPayment;

        void Update(IEnumerable<IPayment> payments);

        PaymentStatus GetStatus(string loanReferenceNumber);
    }
}