using System.Collections.Generic;
using System.Threading.Tasks;
using LendFoundry.Foundation.Persistence;

namespace LendFoundry.Loans.Payment
{
    public interface IPaymentPauseRepository : IRepository<IPaymentPause>
    {
        Task<IPaymentPause> AddOrReplace(IPaymentPause paymentPause);
        Task Remove(string loanReferenceNumber);
        Task<List<IPaymentPause>> Get();
        Task<IPaymentPause> GetByLoanReferenceNumber(string loanReferenceNumber);
    }
}