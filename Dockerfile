FROM registry.lendfoundry.com/base:beta8

ADD ./src/LendFoundry.Loans.Payment.Abstractions /app/LendFoundry.Loans.Payment.Abstractions
WORKDIR /app/LendFoundry.Loans.Payment.Abstractions
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Payment /app/LendFoundry.Loans.Payment
WORKDIR /app/LendFoundry.Loans.Payment
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Payment.Persistence /app/LendFoundry.Loans.Payment.Persistence
WORKDIR /app/LendFoundry.Loans.Payment.Persistence
RUN eval "$CMD_RESTORE"
RUN dnu build

ADD ./src/LendFoundry.Loans.Payment.Api /app/LendFoundry.Loans.Payment.Api
WORKDIR /app/LendFoundry.Loans.Payment.Api
RUN eval "$CMD_RESTORE"
RUN dnu build

EXPOSE 5000
ENTRYPOINT dnx kestrel
